import json;
import nltk;
import sys;
import traceback;
import re;
from nltk.corpus import words;
from nltk.corpus import stopwords;
from nltk.stem.porter import PorterStemmer;
from nltk.stem import WordNetLemmatizer;

PREFIX_MIN_LEN = 3;
SUFFIX_MIN_LEN = 3;
STEMMER = PorterStemmer()
LEMMATIZER = WordNetLemmatizer()
ENG_WORDS = set(words.words());
ENG_STOPWORDS = set(stopwords.words("english"));
JAVA_STOPWORDS = set([
    'abstract',   'continue',   'for',          'new',         'switch',
    'assert,'     'default',    'if',           'package',     'synchronized',
    'boolean',    'do',         'goto',         'private',     'this',
    'break',      'double',     'implements',   'protected',   'throw',
    'byte',       'else',       'import',       'public',      'throws',
    'case',       'enum',       'instanceof',   'return',      'transient',
    'catch',      'extends',    'int',          'short',       'try',
    'char',       'final',      'interface',    'static',      'void',
    'class',      'finally',    'long',         'strictfp',    'volatile',
    'const',      'float',      'native',       'super',       'while',
    'fixme', 'todo', 'and', 'bitand', 'bitor', 'bool', 'decltype',
    'template', 'typename', 'xor', 'import']);

def longest_prefix(term):
    if len(term) <= PREFIX_MIN_LEN:
        return term;

    i = len(term);
    while i >= PREFIX_MIN_LEN:
        if term[0:i] in ENG_WORDS:
            return term[0:i];
        i -= 1;

def longest_suffix(term):
    if len(term) <= SUFFIX_MIN_LEN:
        return term;

    i = 0;
    while i <= len(term) - SUFFIX_MIN_LEN:
        if term[i : len(term)] in ENG_WORDS:
            return term[i : len(term)];
        i += 1;

def normal_split(term, result):
    prefix = longest_prefix(term);
    suffix = longest_suffix(term);

    if prefix is None and suffix is None:
        result.append(term);
    elif prefix is not None and suffix is None:
        result.append(prefix);
        normal_split(term[len(prefix):], result);
    elif prefix is None and suffix is not None:
        result.append(suffix);
        normal_split(term[:len(term) - len(suffix)], result);
    else:
        if len(prefix) >= len(suffix):
            result.append(prefix);
            term = term[len(prefix):];
        else:
            result.append(suffix);
            term = term[:(len(term) - len(suffix))];

        if len(term) > 0:
            normal_split(term, result);

    return result;

def process_terms(term_list):
    """
    Preprocess all the terms. This step includes case splitting,
    stemming, lemmatization, etc.. This should return a list of terms
    that can be directly used for calculating TFIDF.
    """

    result_list = term_list

    # replace all non alphabetical char into underscore
    result_list = [re.sub("[^a-zA-Z]", '_', w) for w in result_list];

    # break the terms using underscores
    tmp_list = [];
    for t in result_list:
        s = re.split("_+", t);
        tmp_list.extend(s);

    result_list = [x for x in tmp_list if len(x) > 1];

    # if there are capital chars in a term, do camelcase splitting
    # otherwise, do greedy splitting
    tmp_list = [];
    for t in result_list:
        if re.search("[A-Z]", t) is None:
            tmp_list.extend(normal_split(t, []));
        else:
            s = re.sub("(.)([A-Z][a-z]+)", r"\1 \2", t);
            s = re.sub("([a-z])([A-Z])", r"\1 \2", s).lower().split();
            tmp_list.extend(s);
    
    result_list = [x for x in tmp_list if len(x) > 1];

    # remove stop words and java keywords 
    tmp_list = [];
    for t in result_list:
        if t not in ENG_STOPWORDS and t not in JAVA_STOPWORDS:
            s = STEMMER.stem(t);
            s = LEMMATIZER.lemmatize(s);
            tmp_list.append(s);
    
    result_list = [x for x in tmp_list if len(x) > 1];

    # TODO: weight more on the function names
    
    return result_list;

def is_valid_word(word):
    return len(word) > 0 and \
        word.isalnum() and \
        word not in ENG_STOPWORDS and \
        word not in JAVA_STOPWORDS;

def get_comments(pe):
    words = [];

    try:
        contents = pe["Features"]["Comments"]["Feature"]["Content"];
        for c in contents:
            text = c["Textual Representation"];
            tokens = nltk.word_tokenize(text);
            processed_tokens = process_terms(tokens);
            words.extend([t for t in processed_tokens if is_valid_word(t)]);
    except:
        traceback.print_exc();

    return words;

def get_tokens(pe):
    """
    Return the token representation of the program
    """
    try:
        token_list = pe["Features"]["Token"]["Feature"]["Content"];
        return token_list;
    except:
        traceback.print_ext();

    return [];

def get_varnum(pe):
    """
    Return the token representation of the program
    """
    try:
        varnum = pe["Features"]["VarNum"]["Feature"]["Content"];
        return varnum;
    except:
        traceback.print_ext();

    return None;

def get_data(filename):
    """
    This function takes a pliny proram element JSON and split out the
    token that can be fed into RNNs
    """
    all_data = [];

    lines = open(filename).read().split("\n");
    for l in lines:
        try:
            jdata = json.loads(l);
        except:
            continue;
        # comments = get_comments(jdata);

        # tokens = get_tokens(jdata);

        # read description and preprocessing
        words = [];
        description = jdata["description"];

        tokens = nltk.word_tokenize(description);
        processed_tokens = process_terms(tokens);
        words.extend([t for t in processed_tokens if is_valid_word(t)]);

        type_tokens = jdata["type"];
        # varnum = get_varnum(jdata);

        # if len(comments) > 0:
            # all_data.append((comments, varnum));
        if len(description) > 0 and len(type_tokens) > 0:
            all_data.append((words, type_tokens));

    return all_data;

def main(filename):
    desc_filename = filename + "_desc.txt";
    type_token_filename = filename + "_types.txt";

    desc_outfile = open(desc_filename, "w");
    type_token_outfile = open(type_token_filename, "w");

    all_data = get_data(filename);
    all_words = set();
    for data in all_data:
        description = " ".join(data[0]).encode("ascii", "ignore") + " <eos> ";
        type_tokens = " ".join(data[1]);
        for w in data[0]:
            all_words.add(w);

        desc_outfile.write(description + "\n");
        type_token_outfile.write(type_tokens + "\n");

    desc_outfile.close();
    type_token_outfile.close();
    print "Wrote description to %s." % desc_filename;
    print "Wrote type tokens to %s." % type_token_filename;

    return list(all_words);

def get_vocab(filename):
    """
    return the vocabulary as a list
    """
    all_data = get_data(filename);

    all_words = set();
    for data in all_data:
        for w in data[0]:
            all_words.add(w);

    return list(all_words);

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit("Usage: %s [pliny PE JSON filename]" % sys.argv[0]);

    main(sys.argv[1])
    # all_words = get_vocab(sys.argv[1]);
    # print " ".join(all_words);
