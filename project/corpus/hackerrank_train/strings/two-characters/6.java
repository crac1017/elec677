import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String s = in.next();
        Set<Character> set = new HashSet<>();
        for (int k=0; k<n; k++) {
            set.add(s.charAt(k));
        }
        int l = set.size();
        if (l < 2) {
            System.out.println(0);
        } else {
            int max = 0;
            char[] c = new char[l];
            int ind = 0;
            for (char x : set) {
                c[ind] = x;
                ind++;
            }
            for (int i=0; i<l-1; i++) {
                for (int j=1; j<l; j++) {
                    String o = "";
                    char last = '0';
                    boolean b = true;
                    for (int k=0; k<n; k++) {
                        if(s.charAt(k) == c[i]) {
                            if (last == c[i]) {
                                b = false;
                                break;
                            } else {
                                o += c[i];
                                last = c[i];
                            }
                        } else if(s.charAt(k) == c[j]) {
                            if (last == c[j]) {
                                b = false;
                                break;
                            } else {
                                o += c[j];
                                last = c[j];
                            }
                        } else {
                            
                        }
                    }
                    if(b && o.length() > max) {
                        max = o.length();
                    }
                }
            }
            System.out.println(max);
        }
    }
}
