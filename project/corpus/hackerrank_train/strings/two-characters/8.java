import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String n_str = br.readLine();
        String actual_str = br.readLine();
        int result = process(actual_str);
        System.out.println(result);
        br.close();
    }
    
    public static int process(String input) {
        ArrayList<Character> chars = new ArrayList<Character>();
        for (int i = 0; i < input.length(); i++) {
            char cur_char = input.charAt(i);
            if (chars.contains(cur_char)) continue;
            else chars.add(cur_char);
        }
        int longest = 0;
        for (int i = 0; i < chars.size() - 1; i++) {
            for (int j = i + 1; j < chars.size(); j++) {
                longest = Math.max(longest, extractAndValidate(input, chars.get(i), chars.get(j)));
            }
        }
        return longest;
    }
    
    public static int extractAndValidate(String input, char a, char b) {
        String curstr = new String(input);
        curstr = curstr.replaceAll("[^" + a + b +"]", "");
        if (curstr.length() == 1) return 1;
        for (int i = 0; i < curstr.length() - 1; i++) {
            if (curstr.charAt(i) == curstr.charAt(i + 1)) return -1;
        }
        return curstr.length();
    }
}
