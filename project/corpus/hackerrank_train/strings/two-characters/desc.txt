String t always consists of two distinct alternating characters. For
example, if string 's two distinct characters are x and y, then t
could be xyxyx or yxyxy but not xxyy or xyyx.

You can convert some string  to string  by deleting characters from
. When you delete a character from , you must delete all occurrences
of it in . For example, if  abaacdabd and you delete the character a,
then the string becomes bcdbd.

Given , convert it to the longest possible string . Then print the
length of string  on a new line; if no string  can be formed from ,
print  instead.

