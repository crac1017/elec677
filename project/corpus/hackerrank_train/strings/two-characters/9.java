import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int slength = Integer.valueOf(in.nextLine());
        char[] s = in.nextLine().toCharArray();
        int bestcount = 0;
        
        
        for(char i='a'; i<='z'; i++){
            for(char j='a'; j<='z'; j++){
                if(i != j){
                    char last = '#';
                    int count = 0;
                    for(int x=0; x<slength; x++){
                        if(s[x] == i){
                            if(last == i){
                                count = 0;
                                break;
                            }
                            last = i;
                            count++;
                        }
                        
                        if(s[x] == j){
                            if(last == j){
                                count = 0;
                                break;
                            }
                            last = j;
                            count++;
                        }
                        
                    }
                    
                    if(bestcount < count) bestcount = count;
                }
            }
            
        }
        if(bestcount == 1) bestcount = 0;
        
        System.out.println(bestcount);
    }
}
