import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = sc.next();
        String az = "abcdefghijklmnopqrstuvwxyz";
        
        int maxLength = 0;
        
        for(int i = 0; i < az.length() - 1; i++)
        {            
            for(int j = i + 1; j < az.length(); j++)
            {
                StringBuilder sb = new StringBuilder();

                for(int g = 0; g < s.length(); g++)
                {
                    if(s.charAt(g) == az.charAt(i) || s.charAt(g) == az.charAt(j))
                    {                    
                        sb.append(s.charAt(g));
                    }
                }

                if(verify(sb.toString()) && sb.length() > maxLength)
                {
                    maxLength = sb.length();
                }
            }
        }
        
        System.out.println(maxLength);
    }
    
    public static boolean verify(String s)
    {        
        HashSet<Character> set = new HashSet<Character>();
 
        for(int i = 0; i < s.length(); i++)
        {
            if(!set.contains(s.charAt(i)))
            {
                set.add(s.charAt(i));
            }
        }
        
        if(set.size() != 2)
        {
            return false;
        }
        
        for(int i = 0; i < s.length() - 1; i++)
        {
            if(s.charAt(i) == s.charAt(i + 1))
            {
                return false;
            }
        }
        
        return true;
    }
}
