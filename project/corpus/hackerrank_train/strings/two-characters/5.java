/*
 * Code Author: Akshay Miterani
 * DA-IICT
 */
import java.io.*;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
 
 
 
public class MainA {
 
	static double eps=(double)1e-6;
	static int mod=(int)1e9+7;
	public static void main(String args[]){
		InputReader in = new InputReader(System.in);
		OutputStream outputStream = System.out;
		PrintWriter out = new PrintWriter(outputStream);
		//----------My Code----------
		int n=in.nextInt();
		if(n==1){
			System.out.println("0");
			return;
		}
		String s=in.nextLine();
		int ans=0;
		for(char c1='a';c1<='z';c1++){
			for(char c2='a';c2<='z';c2++){
				if(c1!=c2){
					ans=Math.max(ans,chk(s, c1, c2));
				}
			}
		}
		System.out.println(ans);
		out.close();
		//---------------The End------------------
 
	}
	static int chk(String s,char c1,char c2){
		int len=0;
		int xor=0;
		for(int i=0;i<s.length();i++){
			if(xor==0){
				if(s.charAt(i)==c1){
					len++;
					xor=1;
				}
				else if(s.charAt(i)==c2){
					return 0;
				}
			}
			else if(xor==1){
				if(s.charAt(i)==c2){
					len++;
					xor=0;
				}
				else if(s.charAt(i)==c1){
					return 0;
				}
			}
		}
		return len;
	}
	static double area(double a,double b,double c){
		double p=(a+b+c)/2;
		return Math.sqrt(p*(p-a)*(p-b)*(p-c));
	}
	public static void debug(Object... o) {
		System.out.println(Arrays.deepToString(o));
	}
	static class InputReader {
		public BufferedReader reader;
		public StringTokenizer tokenizer;
 
		public InputReader(InputStream inputstream) {
			reader = new BufferedReader(new InputStreamReader(inputstream));
			tokenizer = null;
		}
 
		public String nextLine(){
			String fullLine=null;
			while (tokenizer == null || !tokenizer.hasMoreTokens()) {
				try {
					fullLine=reader.readLine();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				return fullLine;
			}
			return fullLine;
		}
		public String next() {
			while (tokenizer == null || !tokenizer.hasMoreTokens()) {
				try {
					tokenizer = new StringTokenizer(reader.readLine());
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			return tokenizer.nextToken();
		}
		public long nextLong() {
			return Long.parseLong(next());
		}
		public int nextInt() {
			return Integer.parseInt(next());
		}
	}
}     
