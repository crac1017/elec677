import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner stdin = new Scanner (System.in);
        
        String S = stdin.next();
        
        while (true) {
            
            int i = 0;
            boolean found = false;
            
            while (i < S.length()-1) {
                if (S.charAt(i) == S.charAt(i+1)) { 
                    found = true;
                    break;
                }
                i = i+1;
            
            }
            
            if (found) {
                
                S = S.substring(0,i) + S.substring(i+2,S.length()); 
                
            }
            else {
                break;
            }
            
            
        }
        
        
        if (S.isEmpty()) 
            System.out.println ("Empty String");
        else
            System.out.println (S);
    }
}
