import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.next();
        String ans = "";
        boolean flag = false;
        while (!flag) {
            flag = true;
            for (int i = 1 ; i < str.length() ; i++) {
                if (str.charAt(i-1) == str.charAt(i)) {
                    str = str.replaceAll(str.substring(i-1, i+1), "");
                    flag = false;
                }
            }
        }
        System.out.println(str.length() == 0 ? "Empty String" : str);
    }
}
