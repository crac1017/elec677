import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        boolean changed = true;
        while (changed) {
            changed = false;
            for (int i = 1; i < s.length(); i++) {
                if (s.charAt(i-1)==s.charAt(i)) {
                    s = s.substring(0,i-1)+s.substring(i+1);
                    changed = true;
                    break;
                }
            }
        }
        if (s.isEmpty()) {
            System.out.println("Empty String");
        } else {
            System.out.println(s);
        }
    }
}
