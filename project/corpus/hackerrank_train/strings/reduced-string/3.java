import java.io.*;
import java.util.*;
public final class hack_ap_1
{
    static FastScanner sc=new FastScanner(new BufferedReader(new InputStreamReader(System.in)));
    static PrintWriter out=new PrintWriter(System.out);
	
	public static void main(String args[]) throws Exception
	{
		String s1=sc.next();
		for(int i=0;i<s1.length();i++)
		{
			if(i<=0)
			{
				continue;
			}
			if(s1.charAt(i)==s1.charAt(i-1))
			{
				String sub1=s1.substring(0,i-1),sub2=s1.substring(i+1,s1.length());
				s1=sub1+sub2;
				i=0;
				continue;
			}
		}
		out.println(s1.length()>0?s1:"Empty String");
		out.close();
	}
}
class FastScanner
{
    BufferedReader in;
    StringTokenizer st;

    public FastScanner(BufferedReader in) {
        this.in = in;
    }
	
    public String nextToken() throws Exception {
        while (st == null || !st.hasMoreTokens()) {
            st = new StringTokenizer(in.readLine());
        }
        return st.nextToken();
    }
	
	public String next() throws Exception {
		return nextToken().toString();
	}
	
    public int nextInt() throws Exception {
        return Integer.parseInt(nextToken());
    }

    public long nextLong() throws Exception {
        return Long.parseLong(nextToken());
    }

    public double nextDouble() throws Exception {
        return Double.parseDouble(nextToken());
    }
}
