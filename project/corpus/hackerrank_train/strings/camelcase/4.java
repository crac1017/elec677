import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        char[] c=s.toCharArray();
        int count=1;
        for(int i=0;i<c.length;i++){
            if(c[i]>=65&&c[i]<=90){
                count=count+1;
            }
        }
        System.out.println(count);
    }
}
