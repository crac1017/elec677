import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        int capitalCount = 0;
        for (char c : s.toCharArray()) {
            if (c >= 'A' && c <= 'Z') {
                capitalCount++;
            }
        }
        System.out.println(capitalCount + 1);
    }
}
