import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        int ans = 0;
        for(char c : s.toCharArray())
            if('A' <= c && c <= 'Z')
            ans++;
        System.out.println(ans+1);
    }
}
