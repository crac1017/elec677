import java.io.*;
import java.util.*;
public final class hr8_a
{
    static FastScanner sc=new FastScanner(new BufferedReader(new InputStreamReader(System.in)));
    static PrintWriter out=new PrintWriter(System.out);
	
	public static void main(String args[]) throws Exception
	{
		int n=sc.nextInt(),c=0;
		char[] a=sc.next().toCharArray(),b=new char[]{'0','1','0'};
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<3 && i+j<n;j++)
			{
				if(a[i+j]!=b[j])
				{
					break;
				}
				if(j==2)
				{
					c++;
					a[i+j]=1;
				}
			}
		}
		out.println(c);
		out.close();
	}
}
class FastScanner
{
    BufferedReader in;
    StringTokenizer st;

    public FastScanner(BufferedReader in) {
        this.in = in;
    }
	
    public String nextToken() throws Exception {
        while (st == null || !st.hasMoreTokens()) {
            st = new StringTokenizer(in.readLine());
        }
        return st.nextToken();
    }
	
	public String next() throws Exception {
		return nextToken().toString();
	}
	
    public int nextInt() throws Exception {
        return Integer.parseInt(nextToken());
    }

    public long nextLong() throws Exception {
        return Long.parseLong(nextToken());
    }

    public double nextDouble() throws Exception {
        return Double.parseDouble(nextToken());
    }
}
