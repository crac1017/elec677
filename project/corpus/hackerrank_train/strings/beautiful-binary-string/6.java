import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.util.regex.*;


public class A
{
    String line;
    StringTokenizer inputParser;
    BufferedReader is;
    FileInputStream fstream;
    DataInputStream in;
    String FInput="";
    
    void openInput(String file)
    {
        if(file==null)is = new BufferedReader(new InputStreamReader(System.in));//stdin
        else
        {
                try{
        
                        
                fstream = new FileInputStream(file);
                in = new DataInputStream(fstream);
                is = new BufferedReader(new InputStreamReader(in));
                }catch(Exception e)
                {
                        System.err.println(e);
                }
        }
    }
    
    void readNextLine()
	{
		try {
			line = is.readLine();
			inputParser = new StringTokenizer(line, " ,\t");
			//System.err.println("Input: " + line);
		} catch (IOException e) {
			System.err.println("Unexpected IO ERROR: " + e);
		}	
		catch (NullPointerException e)
		{
			line=null;
			
		}
		
	}
    
    long NextLong()
    {
            String n = inputParser.nextToken();
            
            long val = Long.parseLong(n);
            
             return val;
    }
    
    int NextInt()
    {
            String n = inputParser.nextToken();
            int val = Integer.parseInt(n);
            
            //System.out.println("I read this number: " + val);
            return val;
    }
    
    double NextDouble()
    {
            String n = inputParser.nextToken();
            double val = Double.parseDouble(n);
            
            //System.out.println("I read this number: " + val);
            return val;
    }
    
    String NextString()
    {
            String n = inputParser.nextToken();
            return n;
    }
    
    void closeInput()
    {
            try {
                    is.close();
            } catch (IOException e) {
                    System.err.println("Unexpected IO ERROR: " + e);
            }
                    
    }
    
    
    public static void main(String [] argv)
    {
            //String filePath="circles.in";
            String filePath=null;
            if(argv.length>0)filePath=argv[0];
            new A(filePath);
            
    }
    
    public A(String inputFile)
    {
    	openInput(inputFile);
		//readNextLine();
		int T=1;//NextInt();
		StringBuilder sb = new StringBuilder();
		Random r = new Random();
		for(int t=1; t<=T; t++)
		{
			readNextLine();
			int N= NextInt();
			readNextLine();
			String s = NextString();
			int p=0;
			int ret = 0;
			while(p<N-1)
			{
				p = s.indexOf("010", p);
				if(p==-1)break;
				ret++;
				p +=3;
			}
			sb.append(ret);
		}
		System.out.print(sb);
		
		closeInput();		
	}
    
}
