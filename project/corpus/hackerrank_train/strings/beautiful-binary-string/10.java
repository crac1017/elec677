import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String B = in.next();
        
        char a[]=B.toCharArray();
        int soBuoc=0;
        for(int i=2;i<a.length;i++){
        	if(a[i]=='0' && a[i-1]=='1' && a[i-2]=='0'){
        		a[i]='1';
        		soBuoc++;
        	}
        }
        System.out.println(soBuoc);
    }
}
