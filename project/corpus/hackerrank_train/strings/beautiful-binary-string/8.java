import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String B = in.next();
        int result = 0;
        for (int i = 0; i < n - 2; i++) {
            if (B.substring(i, i+3).compareTo("010") == 0) {
                result++;
                i += 2;
            }
        }
        System.out.println(result);
    }
}
