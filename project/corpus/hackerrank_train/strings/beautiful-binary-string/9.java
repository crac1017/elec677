import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String b = in.next();
        
        StringBuilder s = new StringBuilder(b);
        
        int counter = 0;
        
        for(int i = 0; i < s.length() - 2; ++i) {
            
            if(s.charAt(i) == '0' && s.charAt(i+1) == '1' && s.charAt(i+2) == '0') {
                
                s.setCharAt(i+2, '1');
                ++counter;
            }
                
        }
        
        System.out.println(counter);
    }
}
