import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String B = in.next();
        int i = 0;
        int count = 0;
        while( i < n - 2  )
        {
            if( B.charAt( i ) == '0' )
            {
                if( B.charAt( i + 1 ) == '1' && B.charAt( i + 2 ) == '0' )
                {
                    i = i + 3;
                    count++;
                    continue;
                }
            }
            i++;
        }
        System.out.println( count );
    }
}
