import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String B = in.next();
        
        String pattern = "010";

      // Create a Pattern object
      Pattern r = Pattern.compile(pattern);

      // Now create matcher object.
      Matcher m = r.matcher(B);
      int count=0;
      while(m.find( )) {         
          count++;
        }
        if(count==0)
         System.out.println("0");
        else
            System.out.println(count);
    }       
}
