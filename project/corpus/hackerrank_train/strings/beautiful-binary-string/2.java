import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String B = in.next();
        
        if (B.length() < 3) {
            System.out.println(0);
            return;
        }
        int a = 0;
        String comp = B.substring(0,2);
        for (int i = 2; i < n; i++) {
            comp += B.charAt(i);
            if (comp.equals("010")) {
                a++;
                comp = "11";
            } else {
                comp = comp.substring(1);
            }
        }
        System.out.println(a);
    }
}
