import java.util.HashMap;
import java.util.Scanner;
public class Solution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);
		String str = s.nextLine().toLowerCase();
		
		HashMap<Character,Boolean> ht = new HashMap<Character, Boolean>();
		char [] temp = str.toCharArray();
		int n = str.length();
		
		for(int i=0;i<n;i++)
		{
			ht.put(temp[i], true);
		}
		for(char i='a';i<='z';i++)
		{
			if(!ht.containsKey(i))
			{
				System.out.println("not pangram");
				return;
			}
		}
		System.out.println("pangram");
	}

}
