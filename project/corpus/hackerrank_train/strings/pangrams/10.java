import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
 
class Solution {
 
	private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	private static final PrintWriter pw = new PrintWriter(System.out);
 
	public static void main(String[] args) throws NumberFormatException, IOException {
		try {
			compute();
		} finally {
			pw.close();
		}
	}

	private static void compute() throws IOException {
        String s= br.readLine();
        s=s.toLowerCase();
        boolean[]alp=new boolean[26];
        for(int i=0;i<s.length();i++)
        {
            char c=s.charAt(i);
           if(c-'a'<26 && c-'a'>=0)alp[c-'a']=true;   
        }
        boolean flag=false;
        for(int i=0;i<26;i++){
            flag=alp[i];
            if(flag==false)break;
        }
        pw.println(flag?"pangram":"not pangram");
    }
}
