import java.io.*;
import java.util.*;
import java.math.BigInteger;
import java.util.Map.Entry;
import static java.lang.Math.*;

public class Solution {

	void run() {
		boolean[] has = new boolean[256];
		for (char c : nextLine().toCharArray()) {
			if ('a' <= c && c <= 'z') {
				has[c - 'a' + 'A'] = true;
			}
			if ('A' <= c && c <= 'Z') {
				has[c] = true;
			}
		}

		boolean ok = true;
		for (int c = 'A'; ok && c <= 'Z'; c++) {
			ok &= has[c];
		}
		out.println(ok ? "pangram" : "not pangram");
	}

	int[][] nextMatrix(int n, int m) {
		int[][] matrix = new int[n][m];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				matrix[i][j] = nextInt();
		return matrix;
	}

	String next() {
		while (!st.hasMoreTokens())
			st = new StringTokenizer(nextLine());
		return st.nextToken();
	}

	boolean hasNext() {
		while (!st.hasMoreTokens()) {
			String line = nextLine();
			if (line == null) {
				return false;
			}
			st = new StringTokenizer(line);
		}
		return true;
	}

	int[] nextArray(int n) {
		int[] array = new int[n];
		for (int i = 0; i < n; i++) {
			array[i] = nextInt();
		}
		return array;
	}

	int nextInt() {
		return Integer.parseInt(next());
	}

	long nextLong() {
		return Long.parseLong(next());
	}

	double nextDouble() {
		return Double.parseDouble(next());
	}

	String nextLine() {
		try {
			return in.readLine();
		} catch (IOException err) {
			return null;
		}
	}

	static PrintWriter out;
	static BufferedReader in;
	static StringTokenizer st = new StringTokenizer("");
	static Random rnd = new Random();

	public static void main(String[] args) throws IOException {
		out = new PrintWriter(System.out);
		// out = new PrintWriter(new File("hc.txt"));
		in = new BufferedReader(new InputStreamReader(System.in));
		new Solution().run();
		out.close();
		in.close();
	}
}
