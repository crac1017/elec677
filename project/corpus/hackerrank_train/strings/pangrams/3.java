import java.io.*;
import java.math.BigInteger;
import java.util.*;


public class Solution {

    void solve() throws IOException {
        String s=reader.readLine();
        boolean[] a=new boolean[26];
        s=s.toLowerCase();
        for(int i=0;i<s.length();i++)
            if(s.charAt(i)>='a'&&s.charAt(i)<='z')a[s.charAt(i)-'a']=true;
        boolean good=true;
        for(int i=0;i<26;i++)
            if(!a[i]){
                good=false;
                break;
            }
        if(good)
            out.println("pangram");
        else
            out.println("not pangram");
    }

    public static void main(String[] args) throws IOException {
        new Solution().run();
    }

    void run() throws IOException {
        reader = new BufferedReader(new InputStreamReader(System.in));
//		reader = new BufferedReader(new FileReader("input.txt"));
        tokenizer = null;
        out = new PrintWriter(new OutputStreamWriter(System.out));
//		out = new PrintWriter(new FileWriter("output.txt"));
        solve();
        reader.close();
        out.flush();

    }

    BufferedReader reader;
    StringTokenizer tokenizer;
    PrintWriter out;

    int nextInt() throws IOException {
        return Integer.parseInt(nextToken());
    }

    long nextLong() throws IOException {
        return Long.parseLong(nextToken());
    }

    double nextDouble() throws IOException {
        return Double.parseDouble(nextToken());
    }

    String nextToken() throws IOException {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            tokenizer = new StringTokenizer(reader.readLine());
        }
        return tokenizer.nextToken();
    }
}
