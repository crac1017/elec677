/* package whatever; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Solution
{
	public static void main(String[] args) throws java.lang.Exception
	{
		// your code goes here
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String s = in.readLine();
		s = s.toLowerCase();
		for (char c = 'a'; c <= 'z'; c++)
		{
		    if (s.indexOf(c) == -1)
		    {
		        System.out.println("not pangram");
		        return;
		    }
		}
		System.out.println("pangram");
	}
}
