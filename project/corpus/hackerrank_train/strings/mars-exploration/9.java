import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String S = in.next();
        int len=S.length();
        int i=0,count=0;
        while(i<len)
        {
            if((i%3==0)&&(S.charAt(i)!='S'))
                count++;
            if((i%3==1)&&(S.charAt(i)!='O'))
                count++;
            if((i%3==2)&&(S.charAt(i)!='S'))
                count++;
            i++;            
        }
        System.out.println(count);
        
    }
}
