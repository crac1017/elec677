import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

   	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		int e = 0;
		String sos = "SOS";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != sos.charAt(i % 3)) {
				e++;
			}
		}
		System.out.println(e);
		in.close();
	}
}
