import java.util.Scanner;


public class Solution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in=new Scanner(System.in);
		int n=in.nextInt();
		
		int arr[][]=new int[n][26];
		
		for(int i=0;i<n;i++)
		{
			String inp=in.next();
			for(int j=0;j<inp.length();j++)
			{
				arr[i][inp.charAt(j)-'a']++;
			}
		}
		
		int count=0;
		
		for(int i=0;i<26;i++)
		{
			int flag=0;
			for(int j=0;j<n;j++)
			{
				if(arr[j][i]==0)
				{
					flag=1;
					break;
				}
				
			}
			if(flag==0)
				count++;
		}
		
		
		System.out.println(count);
	}

}
