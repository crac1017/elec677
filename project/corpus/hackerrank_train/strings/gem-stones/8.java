import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Solution {

  public static void main(String[] args) {
    List<String> input;
    if (args.length > 0) {
      File inputFile = new File(args[0]);
      try (FileInputStream fileInputStream = new FileInputStream(inputFile)) {
        input = Solution.readInputStreamContents(fileInputStream);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      input = Solution.readInputStreamContents(System.in);
    }
    Solution solution = new Solution();
    try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out))) {
      int count = solution.solve(input);
      writer.write(String.valueOf(count));
      writer.newLine();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // Couldn't figure this one out, had to use stackoverflow:
  // http://stackoverflow.com/questions/9469898/1-x-1-y-1-nfactorial
  private int solve(List<String> input) throws IOException {
    int n = Integer.parseInt(input.get(0));
    Set<Character> gems = null;
    for (int i = 1; i <= n; i++) {
      String line = input.get(i);
      Set<Character> currentGems = new HashSet<>();
      for (char c : line.toCharArray()) {
        currentGems.add(new Character(c));
      }
      if (gems == null) {
        gems = currentGems;
      } else {
        List<Character> gemsToRemove = new ArrayList<>();
        for (Character c : gems) {
          if (!currentGems.contains(c)) {
            gemsToRemove.add(c);
          }
        }
        gems.removeAll(gemsToRemove);
      }
    }
    return gems.size();
  }

  private static List<String> readInputStreamContents(InputStream inputStream) {
    List<String> results = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
      String line;
      while ((line = reader.readLine()) != null) {
        results.add(line);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return results;
  }
}
