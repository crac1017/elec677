import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;


public class Solution {	
	BufferedReader reader;
    StringTokenizer tokenizer;
    PrintWriter out;
    
	public void solve() throws IOException {				
		int N = nextInt();
		boolean[] existed = new boolean[26];
		Arrays.fill(existed, true);
		for (int i = 0; i < N; i++) {
			boolean[] existed2 = new boolean[26];
			char[] s = reader.readLine().toCharArray();
			for (int j = 0; j < s.length; j++) {
				existed2[s[j] - 'a'] = true;
			}
			for (int j = 0; j < 26; j++) {
				existed[j] &= existed2[j];
			}
		}
		int cnt = 0;
		for (int j = 0; j < 26; j++) {
			if (existed[j]) cnt++;
		}
		out.println(cnt);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Solution().run();
	}
	
	public void run() {
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            tokenizer = null;
            out = new PrintWriter(System.out);
            solve();
            reader.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    int nextInt() throws IOException {
        return Integer.parseInt(nextToken());
    }

    long nextLong() throws IOException {
        return Long.parseLong(nextToken());
    }

    double nextDouble() throws IOException {
        return Double.parseDouble(nextToken());
    }

    String nextToken() throws IOException {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            tokenizer = new StringTokenizer(reader.readLine());
        }
        return tokenizer.nextToken();
    }

}
