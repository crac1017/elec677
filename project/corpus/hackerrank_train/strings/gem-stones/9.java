import java.io.*;
import java.util.StringTokenizer;
class Solution {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int test = Integer.parseInt(br.readLine());
		int n = test;
		String[] array = new String[test];
		for(int i=0;i<n;i++) {
			array[i] = br.readLine();	
		}
		int[] arr = new int[26];
		char a;
		int b=0;
		for(int i=0;i<n;i++) {
			int[] arra = new int[26];
			for(int j=0;j<array[i].length();j++) {
				a = array[i].charAt(j);
				b = (int)a-97;
				arra[b]++;
				if(arra[b]==1) {
					arr[b]++;
				}
			}
		}
		int ans = 0;
		for(int i=0;i<26;i++) {
			if(arr[i]==n) {
				ans++;
			}
		}
		System.out.println(ans);
	}
}	
