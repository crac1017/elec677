import java.io.*;
import java.util.*;
import java.math.*;

public class Solution
{
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	StringTokenizer tokenizer=null;
	
	public static void main(String[] args) throws IOException
	{
		new Solution().execute();
	}
	
	void debug(Object...os)
	{
		System.out.println(Arrays.deepToString(os));
	}
	
	int ni() throws IOException
	{
		return Integer.parseInt(ns());
	}
	
	long nl() throws IOException 
	{
		return Long.parseLong(ns());
	}
	
	double nd() throws IOException 
	{
		return Double.parseDouble(ns());
	}
		
	String ns() throws IOException 
	{
		while (tokenizer == null || !tokenizer.hasMoreTokens()) 
			tokenizer = new StringTokenizer(br.readLine());
		return tokenizer.nextToken();
	}
	
	String nline() throws IOException
	{
		tokenizer=null;
		return br.readLine();
	}
		
	
	//Main Code starts Here
	int totalCases, testNum;	
	int m,n;
	int mod = 1000000007;
	void execute() throws IOException
	{
		totalCases = ni();
		for(testNum = 1; testNum <= totalCases; testNum++)
		{
			input();
			solve();
		}
	}

	void solve()
	{
		int[] distance = new int[110];
        for(int i = 2 ; i <110 ; i++){
            distance[i] = 100000000;
        }
        
        for(int i = 0 ;i < 102 ; i++) {
            for(int j = 0 ; j < edges.size() ; j++) {
                Edge edge = edges.get(j);                    
                int u = edge.u;
                int v = edge.v;
                int w = edge.w;
                distance[v] = Math.min(distance[u] + w,distance[v]);
            }
        }
        
        System.out.println(distance[100]);
	}
	
    int[][] snake;
    int[][] ladder;
    
    List<Edge> edges = new ArrayList<Edge>();
    
    class Edge {
        public int u, v, w;
        public Edge(int u, int v, int w) {
            this.u = u;
            this.v = v;
            this.w = w;
        }
        @Override
        public String toString(){
            return "(" + u + "," + v + "," + w + ")";
        }
    }
    

	boolean input() throws IOException
	{
		String count = nline();
        
        String[] token = count.split(",");
        int ladders = Integer.parseInt(token[0].trim());
        int snakes = Integer.parseInt(token[1].trim());
        
        snake = new int[snakes][2];
        ladder = new int[ladders][2];
        
        for(int i = 0 ; i < ladders ; i++)
        {
            String pairs = ns();
            token = pairs.split(",");
            ladder[i][0] = Integer.parseInt(token[0].trim());
            ladder[i][1] = Integer.parseInt(token[1].trim());
        }
        
        for(int i = 0 ; i < snakes ; i++)
        {
            String pairs = ns();
            token = pairs.split(",");
            snake[i][0] = Integer.parseInt(token[0].trim());
            snake[i][1] = Integer.parseInt(token[1].trim());
        }
        
        //debug(ladder);
        //debug(snake);
        edges = new ArrayList<Edge>();
        for (int i = 1; i < 100; i++){
            edges.add(new Edge(i, i+1, 1));
            edges.add(new Edge(i, i+2, 1));
            edges.add(new Edge(i, i+3, 1));
            edges.add(new Edge(i, i+4, 1));
            edges.add(new Edge(i, i+5, 1));
            edges.add(new Edge(i, i+6, 1));
        }
                      
        for (int i = 0 ; i< snakes ; i++ ){
            edges.add(new Edge(snake[i][0], snake[i][1], 0));
        }
        for (int i = 0 ; i< ladders ; i++ ){
            edges.add(new Edge(ladder[i][0], ladder[i][1], 0));
        }
        //debug(edges);
		return true;
	}
}
