import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Solution s = new Solution();
        s.Snakes_Ladders();
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    }
    private void Snakes_Ladders() {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(System.in));
            String line = in.readLine();
            int size = Integer.parseInt(line);
            if (size<1 || size>10) {
                System.out.println("Please input a number 1 to 10");
                return;
            }

            for (int i=0; i<size; i++) {
                line = in.readLine();
                String[] ss = line.split(",");
                int ladders = Integer.parseInt(ss[0]);
                int snakes = Integer.parseInt(ss[1]);
                if (ladders<1 ||ladders > 15 || snakes<1 ||snakes > 15) {
                    System.out.println("Please input a number 1 to 15");
                    return; 
                }
                HashMap<String, String> jumps = 
                    new HashMap<String, String>();
                line = in.readLine();
                ss = line.split(" ");
                for (int j=0; j<ss.length; j++) {
                    String[] str = ss[j].split(",");
                    jumps.put(str[0], str[1]);
                }
                line = in.readLine();
                ss = line.split(" ");
                for (int j=0; j<ss.length; j++) {
                    String[] str = ss[j].split(",");
                    jumps.put(str[0], str[1]);
                }
                ArrayList<Step> searched = new ArrayList<Step>();
                ArrayList<Step> available = new ArrayList<Step>();
                Step node = new Step(null, 0, 1);
                available.add(node);
                do {
                    node = available.remove(0);
                    node = next(searched, jumps, node, available);
                    if (node != null) {
                        System.out.println(node.steps);
                        break;
                    }
                } while (available.size()>0);
            }
        } catch (Exception e) {
            System.out.println("input error");
        } finally {
            if (in != null) {
                try {
                    in.close();
                }catch (Exception ee){}
                in = null;
            }
        }
    }
    public Step next(ArrayList<Step> searched, Map<String, String> jump, 
                Step parent, ArrayList<Step> nextgroup) {
        int from = parent.to;
        for (int i=6; i>0; i--) {
            int to = from + i;
            String str = jump.get(""+to);
            if (str != null) {
                to = Integer.parseInt(str);
            }
            if (to > 93 ) {
                Step child = new Step(parent, from, to);
                child = new Step(child, to, 100);
                return child;
            } else if (!isSearched(to, searched)) {
                Step child = new Step(parent, from, to);
                nextgroup.add(child);
            }
        }
        searched.add(parent);
        return null;
    }
    private boolean isSearched(int to, ArrayList<Step> searched) {
        int len = searched.size();
        for (int i=0; i<len; i++) {
            if (to == searched.get(i).to) {
                return true;
            }
        }
        return false;
    }
    private class Step {
        private int steps;
        private int from;
        private int to;
        private Step parent;
        public Step(Step parent, int from, int to) {
            this.from = from;
            this.to = to;
            this.parent = parent;
            if (parent != null) {
                steps = parent.steps + 1;
            }
        }
        public int steps () {
            return steps;
        }
        public int from() {
            return from;
        }
        public int to() {
            return to;
        }
        public Step parent() {
            return parent;
        }
    }
}
