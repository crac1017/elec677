import java.util.*;
import java.io.*;

class graph
{
    private int v;
    public ArrayList<LinkedList<Integer>> alist = new ArrayList<LinkedList<Integer>>();
    
    public graph(int a)
    {
        v = a;
        
        int i;
        
        for(i=0;i<v;i++)
        {
            alist.add(new LinkedList<Integer>());
        }
    }
    
    public void addedge(int a, int b)
    {
        alist.get(a).add(b);
    }
    
    public void print()
    {
        int i;
        
        for(i=0;i<v;i++)
        {
            System.out.print(i+" : ");
            for(int k:alist.get(i))
                System.out.print(k+ " -- ");
            System.out.println();
        }
    }
    
    public int bfs()
    {
        boolean visited[] = new boolean[v];
        
        int dist[] = new int[v];
        
        visited[0] = true;
        
        dist[0] = 0;
        
        LinkedList<Integer> q = new LinkedList<Integer>();
        q.add(0);
        
        while(q.size()>0)
        {
            int temp = q.removeFirst();
         
            
            for(int j:alist.get(temp))
            {
                if(!visited[j])
                {
                visited[j] = true;
                    dist[j] = dist[temp]+1;
                    q.addLast(j);
                }
                
                if(j==99)
                {
                    return dist[99]-1;
                }
            }
        }
        
        return dist[99]-1;
        
    }
}

class Solution
{
    public static void main(String args[]) throws Exception
    {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        int t = Integer.parseInt(br.readLine());

        int i;
        
        for(i=0;i<t;i++)
        {
            String s = br.readLine();
            
            graph g = new graph(100);
            
            int a = Integer.parseInt(s.split(",")[0]);
            
           // int b = Integer.parseInt(s.split(",")[1]);
            
            s = br.readLine();
            String b = br.readLine();
            
            StringTokenizer st = new StringTokenizer(s);
            
            while(st.hasMoreTokens())
            {
                String temp = st.nextToken();
                    int tempa = Integer.parseInt(temp.split(",")[0])-1;
                int tempb = Integer.parseInt(temp.split(",")[1])-1;
                   g.addedge(tempa,tempb);
            }
            
             st = new StringTokenizer(b);
            
            while(st.hasMoreTokens())
            {
                String temp = st.nextToken();
                    int tempa = Integer.parseInt(temp.split(",")[0])-1;
                int tempb = Integer.parseInt(temp.split(",")[1])-1;
                   g.addedge(tempa,tempb);
            }
              int j;
            for(j=0;j<100;j++)
            {
                if(g.alist.get(j).size()==0)
                {
                    int k;
                    for(k=1;k<=6 && k+j<=99;k++)
                        g.addedge(j,j+k);
                        
                }
                
                
            }
            
            //g.print();
            System.out.println(g.bfs());
        }

    }
}
