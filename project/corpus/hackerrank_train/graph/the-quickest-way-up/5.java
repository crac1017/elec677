import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class Solution {
	
	public static class Key{
		int position, depth;
		
		public Key(int position, int depth){
			this.position = position;
			this.depth = depth;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine());
        for(int i=0; i<T; i++){
        	String line = br.readLine();
        	int noOfLadders = Integer.parseInt(line.substring(0, line.indexOf(',')));
        	int noOfSnakes = Integer.parseInt(line.substring(line.indexOf(",")+1, line.length()));
        	HashMap<Integer, Integer> ladders = new HashMap<Integer, Integer>();
        	String[] laddersSplit = br.readLine().split(" ");
        	if(laddersSplit.length != noOfLadders){
        		System.err.println("The number of ladders did'nt match");
        		return;
        	} else {
        		for(int j=0; j<laddersSplit.length; j++){
        			int initialPosition = Integer.parseInt(
        					laddersSplit[j].substring(0, laddersSplit[j].indexOf(',')));
        			int finalPosition = Integer.parseInt(
        					laddersSplit[j].substring(laddersSplit[j].indexOf(',')+1, laddersSplit[j].length()));
        			ladders.put(initialPosition, finalPosition);
        		}
        	}
        	HashMap<Integer, Integer> snakes = new HashMap<Integer, Integer>();
        	String[] snakesSplit = br.readLine().split(" ");
        	if(snakesSplit.length != noOfSnakes){
        		System.err.println("The number of snakes did'nt match");
        		return;
        	} else {
        		for(int j=0; j<snakesSplit.length; j++){
        			int initialPosition = Integer.parseInt(
        					snakesSplit[j].substring(0, snakesSplit[j].indexOf(',')));
        			int finalPosition = Integer.parseInt(
        					snakesSplit[j].substring(snakesSplit[j].indexOf(',')+1, snakesSplit[j].length()));
        			snakes.put(initialPosition, finalPosition);
        		}
        	}
        	
        	HashSet<Integer> visited = new HashSet<Integer>();
        	visited.add(1);
        	ArrayList<Key> queue = new ArrayList<Key>();
        	queue.add(new Key(1, 0));
        	boolean isComplete = false;
        	while(!queue.isEmpty()){
        		Key currentValue = queue.remove(0);
        		for(int moveLength=1; moveLength<=6; moveLength++){
    				int newPosition = currentValue.position+moveLength;
    				if(ladders.containsKey(newPosition)){
    					newPosition = ladders.get(newPosition);
    				} else if(snakes.containsKey(newPosition)){
    					newPosition = snakes.get(newPosition);
    				} else {
    					newPosition = currentValue.position+moveLength;
    				}
    				
    				if(newPosition == 100){
						System.out.println(currentValue.depth+1);
						isComplete = true;
						break;
					}
    				
    				if(!visited.contains(newPosition)){
    					queue.add(new Key(newPosition, currentValue.depth+1));
    					visited.add(newPosition);
    				}
        		}

            	if(isComplete){
            		break;
            	}
        	}
        }
        
        br.close();
	}

}
