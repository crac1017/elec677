import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;

public class Solution {
	static InputStream is;
	static PrintWriter out;
	static String INPUT = "";
	
	static void solve()
	{
		for(int T = ni();T >= 1;T--){
			int L = ni(), s = ni();
			int[][] ls = new int[L][];
			for(int i = 0;i < L;i++)ls[i] = na(2);
			int[][] ss = new int[s][];
			for(int i = 0;i < s;i++)ss[i] = na(2);
			int[][] g = new int[100][100];
			for(int i = 0;i < 100;i++){
				Arrays.fill(g[i], 99999);
				g[i][i] = 0;
			}
			for(int i = 0;i < 99;i++){
				outer:
				for(int j = 1;j <= 6;j++){
					if(i+j >= 100)continue;
					int h = i+j;
					for(int k = 0;k < s;k++){
						if(ss[k][0]-1 == h){
							g[i][ss[k][1]-1] = 1;
							continue outer;
						}
					}
					for(int k = 0;k < L;k++){
						if(ls[k][0]-1 == h){
							g[i][ls[k][1]-1] = 1;
							continue outer;
						}
					}
					g[i][h] = 1;
				}
			}
			for(int k = 0;k < 100;k++){
				for(int i = 0;i < 100;i++){
					for(int j = 0;j < 100;j++){
						if(g[i][j] > g[i][k] + g[k][j]){
							g[i][j] = g[i][k] + g[k][j];
						}
					}
				}
			}
			out.println(g[0][99]);
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		long S = System.currentTimeMillis();
		is = INPUT.isEmpty() ? System.in : new ByteArrayInputStream(INPUT.getBytes());
		out = new PrintWriter(System.out);
		
		solve();
		out.flush();
		long G = System.currentTimeMillis();
		tr(G-S+"ms");
	}
	
	private static byte[] inbuf = new byte[1024];
	static int lenbuf = 0, ptrbuf = 0;
	
	private static int readByte()
	{
		if(lenbuf == -1)throw new InputMismatchException();
		if(ptrbuf >= lenbuf){
			ptrbuf = 0;
			try { lenbuf = is.read(inbuf); } catch (IOException e) { throw new InputMismatchException(); }
			if(lenbuf <= 0)return -1;
		}
		return inbuf[ptrbuf++];
	}
	
	private static int[] na(int n)
	{
		int[] a = new int[n];
		for(int i = 0;i < n;i++)a[i] = ni();
		return a;
	}
	
	private static int ni()
	{
		int num = 0, b;
		boolean minus = false;
		while((b = readByte()) != -1 && !((b >= '0' && b <= '9') || b == '-'));
		if(b == '-'){
			minus = true;
			b = readByte();
		}
		
		while(true){
			if(b >= '0' && b <= '9'){
				num = num * 10 + (b - '0');
			}else{
				return minus ? -num : num;
			}
			b = readByte();
		}
	}
	
	private static void tr(Object... o) { if(INPUT.length() != 0)System.out.println(Arrays.deepToString(o)); }
}
