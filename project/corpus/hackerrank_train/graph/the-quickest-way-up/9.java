import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        int h, t;
        sc.nextLine();
        //System.out.println(str);
        for (int k = 0; k < T; k++) {
            int[] l = new int[101];
            int[] s = new int[101];
            String[] str = sc.next().split(",");
            int ladders = Integer.parseInt(str[0]);
            int snakes = Integer.parseInt(str[1]);

            for (int i = 0; i < ladders; i++) {
                str = sc.next().split(",");
                h = Integer.parseInt(str[0]);
                t = Integer.parseInt(str[1]);
                l[h] = t;
            }

            for (int i = 0; i < snakes; i++) {
                str = sc.next().split(",");
                h = Integer.parseInt(str[0]);
                t = Integer.parseInt(str[1]);
                s[h] = t;
            }

            Queue<Node> q = new LinkedList<>();
            boolean[] visited = new boolean[101];

            int start = 1;
            while (l[start] != 0 || s[start] != 0) {
                if (l[start] != 0) {
                    start = l[start];
                }

                if (s[start] != 0) {
                    start = s[start];
                }
            }

            q.add(new Node(start, 0));
            visited[start] = false;
            boolean finish = false;

            while (!q.isEmpty() && !finish) {
                Node top = q.poll();
                int steps = top.steps;
                for (int i = 1; i <= 6; i++) {
                    int next = top.n + i;
                    while (l[next] != 0 || s[next] != 0) {
                        if (l[next] != 0) {
                            next = l[next];
                        }

                        if (s[next] != 0) {
                            next = s[next];
                        }
                    }
                    if (!visited[next]) {
                        q.add(new Node(next, steps + 1));
                        visited[next] = true;
                    }

                    if (next == 100) {
                        finish = true;
                        System.out.println(steps + 1);
                        break;
                    }
                }
            }
        }
    }

    static class Node {

        int n;
        int steps;

        public Node(int n, int steps) {
            this.n = n;
            this.steps = steps;
        }
    }
}
