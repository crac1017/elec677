//package GraphTheory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;

class Solution {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //System.out.println(adj);
        int cases = Integer.parseInt(br.readLine());
        String snakes[], ssna[];
        String ladders[], slad[];
        while (cases > 0) {
            HashMap<Integer, LinkedList<Integer>> adj = new HashMap<Integer, LinkedList<Integer>>();
            HashMap<Integer, Integer> mindist = new HashMap<>();
            for (int i = 0; i <= 106; i++) {
                adj.put(i, new LinkedList<Integer>());
                mindist.put(i, Integer.MAX_VALUE);
            }
            for (int i = 0; i <= 100; i++) {
                for (int j = i + 1; j <= i + 6; j++) {
                    adj.get(i).add(j);
                }
            }
            HashMap<Integer, Integer> banned = new HashMap<Integer, Integer>();
            cases--;
            br.readLine();
            ladders = br.readLine().split(" ");
            snakes = br.readLine().split(" ");
            for (int i = 0; i < ladders.length; i++) {
                slad = ladders[i].split(",");
                adj.get(Integer.parseInt(slad[0])).add(Integer.parseInt(slad[1]));
            }
            for (int i = 0; i < snakes.length; i++) {
                ssna = snakes[i].split(",");
                banned.put(Integer.parseInt(ssna[0]), 1);
            }
            computePaths(0, adj, mindist, banned);
            //System.out.println(mindistcpy);
        }
    }

    public static void computePaths(int source, HashMap<Integer, LinkedList<Integer>> graph, HashMap<Integer, Integer> dist, HashMap<Integer, Integer> banned) {
        dist.remove(source);
        dist.put(source, 0);

        PriorityQueue<Integer> vertexQueue = new PriorityQueue<Integer>();
        vertexQueue.add(source);

        while (!vertexQueue.isEmpty()) {
            int u = vertexQueue.poll();
            if (u > 100 || banned.containsKey(u)) {
                continue;
            }
            // Visit each edge exiting u
            for (int e : graph.get(u)) {
                //System.out.println("For u="+u+" edge is:"+e);
                int v = e;
                int dtu = dist.get(u) + 1;
                if (dtu < dist.get(v)) {
                    vertexQueue.remove(v);
                    dist.remove(v);
                    dist.put(v, dtu);
                    vertexQueue.add(v);
                }
            }
        }
        System.out.println(dist.get(100)-1);
    }
}
