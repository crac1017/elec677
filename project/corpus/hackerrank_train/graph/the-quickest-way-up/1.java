import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Solution
{

 private static List<Movers> snakesAndLadders;

 public static void main(String[] args)
 {


  Scanner in = new Scanner(System.in);
 String N = in.nextLine(); 
int n = Integer.parseInt(N); 
for (int x=0;x<n;x++){

    snakesAndLadders = new ArrayList<Movers>();
    int total=0; 
    String xx=in.nextLine(); 
    String SL= in.nextLine(); 
    String AL=in.nextLine(); 
    Scanner s = new Scanner(SL).useDelimiter(",| "); 
    while (s.hasNextInt()){ 
    int start = s.nextInt(); 
    int end = s.nextInt(); 
    
    snakesAndLadders.add(new Movers(start, end));
     } 
    Scanner sx = new Scanner(AL).useDelimiter(",| "); 
    while (sx.hasNextInt()){ 
    int start = sx.nextInt(); 
    int end = sx.nextInt(); 
    
    snakesAndLadders.add(new Movers(start, end)); 
    }
    

  int[] moveMap = new int[101];

  for (Movers movers : snakesAndLadders)
   moveMap[movers.start] = movers.end;

  int[] minMove = new int[101];
  int[] dice = new int[101];
  int[]fromCell=new int[101];
  
  LinkedList<Integer> queue = new LinkedList<Integer>();
  queue.add(1);
  boolean finished=false;
  while (!queue.isEmpty()&&!finished)
  {
   int cell = queue.poll();
   for (int i = 1; i <= 6; ++i)
   {
    int newCell = moveMap[cell + i] == 0 ? cell + i : moveMap[cell + i];
    if (minMove[newCell] == 0)
    {
     minMove[newCell] = minMove[cell] + 1;
     dice[newCell]=i;
     fromCell[newCell]=cell;
     queue.add(newCell);
    }
    if(newCell==100)
    {
     finished=true;
     break;
    }

   }

  }
  int cell=100;
  Stack<String> stack=new Stack<String>();
  while(cell!=1)
  {
   stack.push("new cell "+cell);
   stack.push("dice throw "+dice[cell]);
   
   cell=fromCell[cell];
  }
    while(!stack.isEmpty()){
   stack.pop();
    total++;
    }
System.out.println(total/2);
 }

 }
}

/**
 * Snakes and Ladders basically do the same thing. They move the coin from one
 * place to another if they move in positive direction we call them ladders
 * otherwise snakes.
 * 
 */
class Movers
{

 public int start;

 public int end;

 public boolean goingUp;

 public Movers(int start, int end)
 {

  this.start = start;

  this.end = end;
 }
}        
