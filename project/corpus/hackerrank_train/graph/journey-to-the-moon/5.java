import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int n = in.nextInt();
        int l = in.nextInt();
        
        int[] p = new int[n];
        for (int i = 0; i < n; i++) {
            p[i] = i;
        }
        
        for (int i = 0; i < l; i++ ) {
            int a = in.nextInt();
            int b = in.nextInt();
            
            int c = p[a];
            while(p[c] != c) {
                c = p[c];
            }
            
            int d = p[b];
            while (p[d] != d) {
                d = p[d];
            }
            
            if(c < d) {
                p[d] = c;
            } else {
                p[c] = d;
            }
            
        }
        
        Map<Integer,Integer> count = new HashMap<Integer,Integer>();
        for (int i = 0; i < n; i++ ){
            int c = p[i];
            while(p[c] != c) {
                c = p[c];
            }
            if(!count.containsKey(c)) {
                count.put(c,0);
            }
            count.put(c,count.get(c)+1);
        }
        
        long single = 0L;
        Map<Integer, Integer> tmp = new HashMap<Integer,Integer>();
        for(int i : count.keySet()) {
            if(count.get(i) > 1) {
                tmp.put(i,count.get(i));
            } else {
                single ++;
            }
        }        
        count  = tmp;
        
        List<Integer> group = new ArrayList<Integer>(count.keySet());
        
        long total = 0;
        
        for(int i = 0; i < group.size(); i++) {
            for(int j = i + 1; j < group.size(); j++) {
                total += count.get(group.get(i)) * count.get(group.get(j));
            }
        }
        
        total += single*(single-1) / 2;
        
        for(int i = 0; i < group.size(); i++) {
            total += count.get(group.get(i)) * single;
        }
        
        System.out.println(total);
        
        
    }
}
