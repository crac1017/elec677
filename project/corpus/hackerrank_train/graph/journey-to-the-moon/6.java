import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int lines = in.nextInt();
        List<List<Integer>> graph = new ArrayList<List<Integer>>();
        boolean[] visited = new boolean[n];
        for(int i=0;i<n;++i) {
            visited[i] = false;
            graph.add(new LinkedList<Integer>());
        }
        while(lines-->0) {
            int a = in.nextInt();
            int b = in.nextInt();
            graph.get(b).add(a);
            graph.get(a).add(b);
        }
        int remaining = n;
        int next = 0;
        long answer = 0;
        while(remaining > 0) {
            while(visited[next]) ++next;
            visited[next] = true;
            int curGroup = 1;
            --remaining;
            ArrayDeque<Integer> q = new ArrayDeque<Integer>();
            q.offer(next);
            while(!q.isEmpty()) {
                int cur = q.poll();
                for(int adj : graph.get(cur)) {
                    if (!visited[adj]) {
                        ++curGroup;
                        --remaining;
                        visited[adj] = true;
                        q.offer(adj);
                    }
                }
            }
            answer += (long)curGroup*remaining;
        }
        System.out.println(answer);
        
    }
}
