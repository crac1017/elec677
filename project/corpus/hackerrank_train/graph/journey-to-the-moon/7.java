import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;


public class Solution {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		int I = scanner.nextInt();
		
		ArrayList<HashSet<Integer>> nationality = new ArrayList<HashSet<Integer>>();
		
		for(int i=0; i<I; i+=1){
			int value1 = scanner.nextInt();
			int value2 = scanner.nextInt();
			int list1Index = -1;
			int list2Index = -1;
			
			for(int j=0; j<nationality.size(); j+=1) {
				if(list1Index>=0 && list2Index>=0){
					break;
				}
				
				HashSet<Integer> currentNation = nationality.get(j);
				boolean index1 = currentNation.contains(value1);
				boolean index2 = currentNation.contains(value2);;
				if(index1 && index2){
					list1Index = j;
					list2Index = j;
					break;
				} else if(index1){
					list1Index = j;
					currentNation.add(value2);
				} else if(index2){
					list2Index = j;
					currentNation.add(value1);
				} else {
					continue;
				}
			}
			
			if(list1Index == -1 && list2Index == -1){
				HashSet<Integer> newNation = new HashSet<Integer>();
				if(value1 == value2){
					newNation.add(value1);
				}else if(value1 <= value2){
					newNation.add(value1);
					newNation.add(value2);
				} else {
					newNation.add(value2);
					newNation.add(value1);
				}
				nationality.add(newNation);
			} else if(list1Index != list2Index && list1Index != -1 && list2Index != -1){
				HashSet<Integer> nation1 = nationality.get(list1Index);
				HashSet<Integer> nation2 = nationality.get(list2Index);
				nation1.addAll(nation2);
				nationality.remove(list2Index);
			} else {
				
			}
		}//for
		
		if(N == 0 || N == 1){
			System.out.println(0);
			return;
		}
		
		int[] counts = new int[nationality.size()+1];
		Arrays.fill(counts, 0);
		int s = N;
		for(int i=0; i<nationality.size(); i++){
			counts[i] = nationality.get(i).size();
			s -= nationality.get(i).size();
		}
		
		if(s > 0){
			counts[nationality.size()] = s;
		}
		
		long result = 0L;
		for(int i=0; i<counts.length-1; i+=1){
			long sum = 0L;
			for(int j=i+1; j<counts.length; j+=1){
				sum += ((long)counts[j]);
			}
			result += (((long)counts[i])*sum);
		}
		
		if(s>1){
			result += (((long)s)*((long)(s-1)))/2;
		}
		System.out.println(result);
	}
}
