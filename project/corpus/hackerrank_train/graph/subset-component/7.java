import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class Solution {

    static class UnionFind {
        byte[] id;
        byte[] rank;
        int conn;
        UnionFind() {
            conn = 64;
            id = new byte[64];
            rank = new byte[64];
            for (byte i = 0; i < 64; i++) {
                id[i] = i;
            }
        }
        
        byte find(byte i) {
           while (i != id[i]) {
               id[i] = id[id[i]];    // path compression by halving
               i= id[i];
           }
           return i;
        }

        void union(byte p, byte q) {
            byte i = find(p);
            byte j = find(q);
            if (i == j)
                return;
            if (rank[i] > rank[j]) {
                id[j] = i;
            } else if (rank[i] < rank[j]) {
                id[i] = j;
            } else {
                id[i] = j;
                rank[i] += 1;
            }
            conn--;
        }

        int connCom() {
            return conn;
        }

        UnionFind copyIt() {
            UnionFind f = new UnionFind();
            for (int i = 0; i < 64; i++) {
                f.id[i] = id[i];
                f.rank[i] = rank[i];
                f.conn = conn;
            }
            return f;
        }
    }

    public static void combine(UnionFind u, BigInteger b) {
        List<Byte> f = new ArrayList<>();
        for (byte i = 0; i < 64; i++) {
            if (b.testBit(i)) 
                f.add(i);
        }
        if (f.size() < 2) {
            return;
        }
        byte num = f.get(0);
        for (int i = 1; i < f.size(); i++) {
            u.union(num, f.get(i));
        }
    }

    public static long f(BigInteger[] A, int i, UnionFind u) {
        if (i >= A.length) {
            //System.out.println(u.conn);
            return u.conn;
        }
        long tot = 0;
        tot += f(A, i + 1, u);
        UnionFind dd = u.copyIt();
        combine(dd, A[i]);
        tot += f(A, i + 1, dd);
        return tot;
    }

    public static void main(String[] argv) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer d = new StringTokenizer(in.readLine());
        int N = Integer.parseInt(d.nextToken());
        BigInteger[] A = new BigInteger[N];
        d = new StringTokenizer(in.readLine());
        for (int i = 0; i < N; i++) {
            A[i] = new BigInteger(d.nextToken());
        }
        long tot = 0;
        boolean [] bool = new boolean[N];
        /*for (int j = 0; j < Math.pow(2, N); j++) {

            BitSet bs = BitSet.valueOf(new long[] {j});
            UnionFind uf = new UnionFind();
            for (int i = bs.nextSetBit(0); i >= 0; i = bs.nextSetBit(i+1)) {
                combine(uf, A[i]);
            }
            tot = tot + uf.connCom();
        }*/
        System.out.println(f(A, 0, new UnionFind()));
        }
}

