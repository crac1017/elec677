/* HackerRank Template */

import java.io.*;
import java.math.*;
import java.util.*;

import static java.lang.Math.*;
import static java.util.Arrays.fill;
import static java.util.Arrays.binarySearch;
import static java.util.Arrays.sort;

public class Solution {
	
	static long initTime;
	static final Random rnd = new Random(7777L);
	static boolean writeLog = false;
	
	public static void main(String[] args) throws IOException {
//		initTime = System.currentTimeMillis();
//		try {
////			writeLog = "true".equals(System.getProperty("LOCAL_RUN_7777"));
//			writeLog = true;
//		} catch (SecurityException e) {}
//		new Thread(null, new Runnable() {
//			public void run() {
//				try {
//					try {
//						if (new File("input.txt").exists())
//							System.setIn(new FileInputStream("input.txt"));
//					} catch (SecurityException e) {}
//					long prevTime = System.currentTimeMillis();
//					new Solution().run();
//					writeLog("Total time: " + (System.currentTimeMillis() - prevTime) + " ms");
//					writeLog("Memory status: " + memoryStatus());
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}, "1", 1L << 24).start();
		new Solution().run();
	}

	void run() throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
//		out = new PrintWriter(System.out);
		solve();
//		out.close();
		in.close();
	}
	
	/*************************************************************** 
	 * Solution
	 **************************************************************/

	final int MAXN = 20;
	final int V = 64;
	
	int n;
	long[] a;
	DSF[] numDsfs;
	
	DSF dsf = new DSF(V);
	MDSF mdsf = new MDSF(V, MAXN);
	DSF[] mem;
	int answer = 0;
	
	void solve() throws IOException  {
		n = nextInt();
		a = new long [n];
		for (int i = 0; i < n; i++)
			a[i] = new BigInteger(nextToken()).longValue();
		numDsfs = new DSF [n];
		for (int i = 0; i < n; i++) {
			numDsfs[i] = new DSF(V);
			long x = a[i];
			for (int u = 0; u < V; u++)
				if (testBit(x, u))
					for (int v = u + 1; v < V; v++)
						if (testBit(x, v))
							numDsfs[i].union(u, v);
		}
		
		mem = new DSF [MAXN];
		for (int i = 0; i < mem.length; i++)
			mem[i] = new DSF(V);
		
		boolean neg = false;
		for (long x : a)
			if (x < 0L)
				neg = true;
		
		if (neg)
			rec(0);
		else
			rec1(0);
//		out.println(answer);
		System.out.println(answer);
	}
	
	void rec(int pos) {
		if (dsf.nComponents == 1 || pos == n) {
			answer += dsf.nComponents << (n - pos);
			return;
		}
		rec(pos + 1);
		mem[pos].set(dsf);
		union(dsf, numDsfs[pos]);
		rec(pos + 1);
		dsf.set(mem[pos]);
	}
	
	void rec1(int pos) {
		if (mdsf.nComponents == 1 || pos == n) {
			answer += mdsf.nComponents << (n - pos);
			return;
		}
		rec1(pos + 1);
		mdsf.pushState();
		long x = a[pos];
		for (int u = 0; u < V; u++) {
			if (!testBit(x, u)) continue;
			for (int v = u + 1; v < V; v++) {
				if (!testBit(x, v)) continue;
				mdsf.union(u, v);
			}
		}
		rec1(pos + 1);
		mdsf.restoreState();
	}
	
	DSF uDsf = new DSF(V + V);
	
	void union(DSF dsf1, DSF dsf2) {
		uDsf.clear(V + V);
		for (int i = 0; i < V; i++) {
			uDsf.set[i] = dsf1.setOf(i);
			uDsf.set[V + i] = V + dsf2.setOf(i);
		}
		for (int i = 0; i < V; i++)
			uDsf.union(i, V + i);
		for (int i = 0; i < V; i++) {
			int si = uDsf.setOf(i);
			if (si >= V) si -= V;
			dsf1.set[i] = si;
		}
		dsf1.recalcCompnents();
	}

	boolean testBit(long x, int bit) {
		return (x & (1L << bit)) != 0L;
	}
	
	int[] map = new int [V];
	
	class DSF {
		int sz;
		int[] set;
		int nComponents;
		
		DSF(int sz) {
			set = new int [sz];
			clear(sz);
		}
		
		void recalcCompnents() {
			nComponents = 0;
			fill(map, -1);
			for (int i = 0; i < sz; i++)
				if (map[set[i]] == -1)
					map[set[i]] = nComponents++;
		}

		void set(DSF dsf) {
			sz = dsf.sz;
			System.arraycopy(dsf.set, 0, set, 0, sz);
			nComponents = dsf.nComponents;
		}

		void clear(int sz) {
			for (int i = 0; i < sz; i++)
				set[i] = i;
			nComponents = sz;
			this.sz = sz;
		}
		
		int setOf(int x) {
			return x == set[x] ? x : (set[x] = setOf(set[x]));
		}
		
		boolean union(int i, int j) {
			if ((i = setOf(i)) == (j = setOf(j)))
				return false;
			if (rnd.nextBoolean()) {
				set[j] = i;
			} else {
				set[i] = j;
			}
			nComponents--;
			return true;
		}
	}
	
	class MDSF {
		int sz;
		int[] set;
		int[] rnk;
		int nComponents;
		
		int[][] storedSet;
		int[][] storedRnk;
		int[] storedNumComponents;
		int memPnt = 0;
		
		MDSF(int sz, int memoryDepth) {
			set = new int [sz];
			rnk = new int [sz];
			storedSet = new int [memoryDepth][sz];
			storedRnk = new int [memoryDepth][sz];
			storedNumComponents = new int [memoryDepth];
			clear(sz);
		}
		
		void pushState() {
			System.arraycopy(set, 0, storedSet[memPnt], 0, sz);
			System.arraycopy(rnk, 0, storedRnk[memPnt], 0, sz);
			storedNumComponents[memPnt] = nComponents;
			memPnt++;
		}
		
		void restoreState() {
			memPnt--;
			System.arraycopy(storedSet[memPnt], 0, set, 0, sz);
			System.arraycopy(storedRnk[memPnt], 0, rnk, 0, sz);
			nComponents = storedNumComponents[memPnt];
		}

		void clear(int sz) {
			fill(rnk, 0, sz, 0);
			for (int i = 0; i < sz; i++)
				set[i] = i;
			nComponents = sz;
			this.sz = sz;
		}
		
		int setOf(int x) {
			return x == set[x] ? x : (set[x] = setOf(set[x]));
		}
		
		boolean union(int i, int j) {
			if ((i = setOf(i)) == (j = setOf(j)))
				return false;
			if (rnk[i] > rnk[j]) {
				set[j] = i;
			} else {
				set[i] = j;
				if (rnk[i] == rnk[j])
					rnk[j]++;
			}
			nComponents--;
			return true;
		}
	}
	
	/*************************************************************** 
	 * Input 
	 **************************************************************/
	BufferedReader in;
	PrintWriter out;
	StringTokenizer st = new StringTokenizer("");
	
	String nextToken() throws IOException {
		while (!st.hasMoreTokens())
			st = new StringTokenizer(in.readLine());
		return st.nextToken();
	}
	
	int nextInt() throws IOException {
		return Integer.parseInt(nextToken());
	}
	
	long nextLong() throws IOException {
		return Long.parseLong(nextToken());
	}
	
	double nextDouble() throws IOException {
		return Double.parseDouble(nextToken());
	}
	
	int[] nextIntArray(int size) throws IOException {
		int[] ret = new int [size];
		for (int i = 0; i < size; i++)
			ret[i] = nextInt();
		return ret;
	}
	
	long[] nextLongArray(int size) throws IOException {
		long[] ret = new long [size];
		for (int i = 0; i < size; i++)
			ret[i] = nextLong();
		return ret;
	}
	
	double[] nextDoubleArray(int size) throws IOException {
		double[] ret = new double [size];
		for (int i = 0; i < size; i++)
			ret[i] = nextDouble();
		return ret;
	}
	
	String nextLine() throws IOException {
		st = new StringTokenizer("");
		return in.readLine();
	}
	
	boolean EOF() throws IOException {
		while (!st.hasMoreTokens()) {
			String s = in.readLine();
			if (s == null)
				return true;
			st = new StringTokenizer(s);
		}
		return false;
	}
	
	/*************************************************************** 
	 * Output 
	 **************************************************************/
	void printRepeat(String s, int count) {
		for (int i = 0; i < count; i++)
			out.print(s);
	}
	
	void printArray(int[] array) {
		if (array == null || array.length == 0)
			return;
		for (int i = 0; i < array.length; i++) {
			if (i > 0) out.print(' ');
			out.print(array[i]);
		}
		out.println();
	}
	
	void printArray(long[] array) {
		if (array == null || array.length == 0)
			return;
		for (int i = 0; i < array.length; i++) {
			if (i > 0) out.print(' ');
			out.print(array[i]);
		}
		out.println();
	}
	
	void printArray(double[] array) {
		if (array == null || array.length == 0)
			return;
		for (int i = 0; i < array.length; i++) {
			if (i > 0) out.print(' ');
			out.print(array[i]);
		}
		out.println();
	}
	
	void printArray(double[] array, String spec) {
		if (array == null || array.length == 0)
			return;
		for (int i = 0; i < array.length; i++) {
			if (i > 0) out.print(' ');
			out.printf(Locale.US, spec, array[i]);
		}
		out.println();
	}
	
	void printArray(Object[] array) {
		if (array == null || array.length == 0)
			return;
		boolean blank = false;
		for (Object x : array) {
			if (blank) out.print(' '); else blank = true;
			out.print(x);
		}
		out.println();
	}
	
	@SuppressWarnings("rawtypes")
	void printCollection(Collection collection) {
		if (collection == null || collection.isEmpty())
			return;
		boolean blank = false;
		for (Object x : collection) {
			if (blank) out.print(' '); else blank = true;
			out.print(x);
		}
		out.println();
	}
	
	/*************************************************************** 
	 * Utility
	 **************************************************************/
	static String memoryStatus() {
		return (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory() >> 20) + "/" + (Runtime.getRuntime().totalMemory() >> 20) + " MB";
	}
	
	static void checkMemory() {
		System.err.println(memoryStatus());
	}
	
	static long prevTimeStamp = Long.MIN_VALUE;
	
	static void updateTimer() {
		prevTimeStamp = System.currentTimeMillis();
	}
	
	static long elapsedTime() {
		return (System.currentTimeMillis() - prevTimeStamp);
	}
	
	static void checkTimer() {
		System.err.println(elapsedTime() + " ms");
	}
	
	static void chk(boolean f) {
		if (!f) throw new RuntimeException("Assert failed");
	}
	
	static void chk(boolean f, String format, Object ... args) {
		if (!f) throw new RuntimeException(String.format(format, args));
	}
	
	static void writeLog(String format, Object ... args) {
		if (writeLog) System.err.println(String.format(Locale.US, format, args));
	}
}
