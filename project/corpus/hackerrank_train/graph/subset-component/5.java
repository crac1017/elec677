import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;


public class Solution
{
    public static void main(String[] args) {
        final BigInteger TWO = BigInteger.valueOf(2);
        final BigInteger TWO_63 = TWO.pow(63);
        final BigInteger TWO_64 = TWO.pow(64);

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in), 64 * 1024);

            final int N = Integer.parseInt(br.readLine().trim(), 10);
            final String[] data = br.readLine().trim().split(" ");
            final BigInteger[] D = new BigInteger[N];

            final long[] d = new long[N];
            int n = 0;

            int bitCount = 0;
            for (int i = 0; i < N; i++) {
                D[i] = new BigInteger(data[i], 10);

                if (D[i].bitCount() <= 1) {
                    bitCount++;
                } else {
                    if (D[i].compareTo(TWO_63) >= 0) {
                        d[n] = D[i].subtract(TWO_64).longValue();
                    } else {
                        d[n] = D[i].longValue();
                    }

                    n++;
                }
            }

            long[][] sets = new long[1 << n][];
            sets[0] = new long[0];
            int len = 1;
            int idx = 1;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < len; j++) {
                    long v = d[i];
                    long[] s = sets[j];
                    long[] new_s = new long[s.length + 1];
                    int cnt = 0;
                    for (int k = 0; k < s.length; k++) {
                        if ((v & s[k]) == 0) {
                            new_s[cnt] = s[k];
                            cnt++;
                        } else {
                            v |= s[k];
                        }
                    }
                    new_s[cnt] = v;
                    cnt++;
                    sets[idx] = Arrays.copyOfRange(new_s, 0, cnt);
                    idx++;
                }

                len <<= 1;
            }

            long res = 0;
            for (int i = 0; i < sets.length; i++) {
                res += 64;
                for (int j = 0; j < sets[i].length; j++) {
                    res -= Math.max(0, Long.bitCount(sets[i][j]) - 1);
                }
            }
            System.out.println(res << bitCount);

            br.close();
            br = null;
        }
        catch (IOException ioe) {
            System.exit(1);
        }
    }
}
