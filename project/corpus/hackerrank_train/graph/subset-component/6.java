import java.io.*;
import java.util.*;
import java.math.*;

public class Solution implements Runnable {

	public void solve() throws IOException {
		int n = nextInt();
		long[] d = new long[n];
		for (int i = 0; i < n; ++i) {
			d[i] = new BigInteger(next()).longValue();
		}
		
		long[] old = new long[n];
		int old_size = 0;
		long[] next = new long[n];
		int next_size = 0;
		long result = 0;
		for (int msk = 0; msk < (1 << n); ++msk) {
			for (int i = 0; i < n; ++i) {
				if ((msk >> i) % 2 == 1) {
					long cur = d[i];
					for (int j = 0; j < old_size; ++j) {
						if ((cur & old[j]) == 0) {
							next[next_size++] = old[j];
						} else {
							cur |= old[j];
						}
					}
					if (cur != 0) {
						next[next_size++] = cur;
					}
					long[] tmp = next;
					next = old;
					old = tmp;
					old_size = next_size;
					next_size = 0;
				}
			}
			int single = 64;
			for (int i = 0; i < old_size; ++i) {
				single -= Long.bitCount(old[i]);
			}
			result += old_size + single;
			old_size = 0;
		}
		out.println(result);

	}

	BufferedReader br;
	StringTokenizer st;
	PrintWriter out;

  public static void main(String[] args) {
		(new Thread(new Solution())).start();
	}	

	@Override
	public void run() {
		try {
			br = new BufferedReader(new InputStreamReader(System.in));
			out = new PrintWriter(System.out);
			st = new StringTokenizer("");
		
			solve();

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
			out.close();
		}
	}

	String next() throws IOException {
		while (!st.hasMoreTokens()) {
			String temp = br.readLine();
			if (temp == null) {
				return null;
			}
			st = new StringTokenizer(temp);
		}
		return st.nextToken();
	}

	long nextLong() throws IOException {
		return Long.parseLong(next());
	}
	int nextInt() throws IOException {
		return Integer.parseInt(next());
	}

}
