import java.math.BigInteger;
import java.util.Scanner;

public class Solution {
    static int col = 0;
    static long[] a;
    private static long dfs(int i, int mask) {
        if ((col & (1 << i)) != 0) {
            return 0;
        }
        col |= 1 << i;
        long ret = a[i];
        for (int j = 0; j < a.length; ++j) {
            if ((mask & (1 << j)) != 0 && (a[i] & a[j]) != 0) {
                ret |= dfs(j, mask);
            }
        }
        return ret;
    }

    public static void main(String[] args)  {
    	Scanner sc  = new Scanner(System.in);
    	
    	 int n = sc.nextInt();
    	 
    	 a = new long[n];
         for (int i = 0; i < n; ++i) 
             a[i] = new BigInteger(sc.next()).longValue();
         
         int ans = 0;
         for (int mask = 0; mask < 1 << n; ++mask) {
             col = 0;
             int countBits = 64;
             int add = 0;
             for (int i = 0; i < n; ++i) {
                 if (((mask & ~col) & (1 << i)) != 0) {
                     long mask1 = dfs(i, mask);
                     if (mask1 != 0) {
                         add++;
                         countBits -= Long.bitCount(mask1);
                     }
                 }
             }
             add += countBits;
             ans += add;
         }
         System.out.println(ans);
        sc.close();
    }

    }
