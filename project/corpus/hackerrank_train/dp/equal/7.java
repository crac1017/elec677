
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Solution {

	private static void debug(Object... args) {
		System.out.println(Arrays.deepToString(args));
	}

	public static void main(String[] rags) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		while (t-- > 0) {
			int N=Integer.parseInt(br.readLine());
			int[]v=new int[N];
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int i=0;i<N;i++) v[i]=Integer.parseInt(st.nextToken());
			Arrays.sort(v);
			int sum=0;
			int sum1=1;
			int sum2=1;
			for(int i=1;i<N;i++) {
				int d=v[i]-v[0];
				sum+=d/5 + (d%5)/2 + (d%5)%2;
				d+=1;
				sum1+=d/5 + (d%5)/2 + (d%5)%2;
				d+=1;
				sum2+=d/5 + (d%5)/2 + (d%5)%2;
			}
			System.out.println(Math.min(Math.min(sum,sum1),sum2));
		}
	}
}
