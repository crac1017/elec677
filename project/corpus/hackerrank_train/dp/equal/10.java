import java.util.Arrays;
import java.util.Scanner;

//need to take care of threes
public class Solution {
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		int num = in.nextInt();
		int co_num=0;

		for(int i=0;i<num;i++)
		{
			co_num=in.nextInt();
			if(co_num==0){
				System.out.println("0");
			}
			else if(co_num==1){
				in.next();
				System.out.println("0");
			}
			else{
				int[] nums = new int[co_num];
				int min =10000;
				for(int j=0;j<co_num;j++){
					nums[j]=in.nextInt();
					min = Math.min(min, nums[j]);
				}
				System.out.println(getNum(nums,min));
			}
		}
	}
	
	public static int getNum(int[] nums, int min)
	{
		int size = nums.length;
		int moves1=0;
		int moves2=0;
		int moves3=0;
		
		int num =0;
		int num5,num2;
		
		int temp=0;
		for(int i=0;i<size;i++)
		{
			num=(nums[i]-min);
			num5=num/5;
			num=num-num5*5;
			
			if(num%3==0)
				temp=1;
			else
				temp=2;
			moves2+=temp+num5;
			
			if(num==2 || num==3)
				temp=2;
			else
				temp=1;
			moves3+=temp+num5;
			
			num2=num/2;
			num=num-num2*2;
			
			moves1+=num5+num2+num;
		}
		return Math.min(moves1,Math.min(moves3,moves2));
	}
}
