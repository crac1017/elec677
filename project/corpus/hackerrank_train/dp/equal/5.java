import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader (new InputStreamReader (System.in));
        PrintWriter out = new PrintWriter (System.out, true);
        int [] dp = new int [1020];
        dp [0] = 0;
        dp [1] = 1;
        dp [2] = 1;
        dp [3] = 2;
        dp [4] = 2;
        for (int i = 5; i <= 1019; i++) {
            dp [i] = Math.min (dp [i - 1], Math.min (dp [i - 2], dp [i - 5])) + 1;
        }
        
        int T = Integer.parseInt (in.readLine());
        StringTokenizer st;
        while (T-- > 0) {
            int N = Integer.parseInt (in.readLine());
            st = new StringTokenizer (in.readLine());
            int [] arr = new int [N];
            int min = 10000;
            for (int i = 0; i < N; i++) {
                arr [i] = Integer.parseInt (st.nextToken());
                if (arr [i] < min) min = arr [i];
            }
            
            for (int i = 0; i < N; i++)
                arr [i] -= min;
            
            int mincost = Integer.MAX_VALUE;
            for (int off = 0; off < 5; off++) {
                int cost = 0;
                for (int i = 0; i < N; i++) {
                    cost += dp [arr [i] + off];
                }
                if (cost < mincost)
                    mincost = cost;
            }
            
            out.println (mincost);
        }
        out.close();
        System.exit(0);
    }
}
