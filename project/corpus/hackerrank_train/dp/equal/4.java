import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
          InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        //BufferedReader x=new BufferedReader(new InputStreamReader(System.in));
       OutputWriter outt = new OutputWriter(outputStream);
       StringBuilder out=new StringBuilder();
       int t=in.readInt();
       int denom[]={1,2,5};
        while(t-->0){
            int ans=0;
            int n=in.readInt();
            int sum=0;
            HashMap<Integer,Boolean> z=new HashMap<Integer,Boolean>();
            ArrayList<Integer> a=new ArrayList<Integer>();
            int min=10000;
            for(int i=0;i<n;i++){int x=in.readInt();
               // if(!z.containsKey(x)){
                    z.put(x,true);
                    a.add(x);
                    min=Math.min(min,x);
               // }
            }
            int l=a.size();
            //int sum=0;
            for(int i=0;i<l;i++){
                int y=(a.get(i)-min);
                sum+=y/5;
                y=y%5;
                sum+=y/2;
                y=y%2;
                sum+=y;
            }
            int sum1=0;int min1=Math.max(0,min-1);
            for(int i=0;i<l;i++){
                int y=(a.get(i)-min1);
                sum1+=y/5;
                y=y%5;
                sum1+=y/2;
                y=y%2;
                sum1+=y;
            }
            int sum2=0;int min2=Math.max(0,min-2);
            for(int i=0;i<l;i++){
                int y=(a.get(i)-min2);
                sum2+=y/5;
                y=y%5;
                sum2+=y/2;
                y=y%2;
                sum2+=y;
            }
             //ans=count(denom,sum);
           ans=Math.min(Math.min(sum,sum1),sum2);
            //Arrays.sort();
            out.append(ans+"\n");
        }
         outt.print(out);
        outt.close();
    }
    static long gcd(long a,long b){
       // if(a.equals(BigInteger.ONE)||b.equals(BigInteger.ONE)) return BigInteger.ONE;
    while( b!=0)
      {long t = b;
       b = a%(t);
       a = t;}
    return a;}
    static int count(int v[],int s){
    
    int min[]=new int[s+1];
    Arrays.fill(min,Integer.MAX_VALUE);
    min[0]=0;
    int n=v.length;
for(int i=1;i<=s;i++)
    for(int j=0;j<n;j++)
   if(v[j]<=i && min[i-v[j]]+1<min[i])
        min[i]=min[i-v[j]]+1;

        
        
        
return min[s];}
}

class InputReader {
    
    private InputStream stream;
    private byte[] buf = new byte[1024];
    private int curChar;
    private int numChars;
    private SpaceCharFilter filter;
 
    public InputReader(InputStream stream) {
        this.stream = stream;
    }
 
    public int read() {
        if (numChars == -1)
            throw new InputMismatchException();
        if (curChar >= numChars) {
            curChar = 0;
            try {
                numChars = stream.read(buf);
            } catch (IOException e) {
                throw new InputMismatchException();
            }
            if (numChars <= 0)
                return -1;
        }
        return buf[curChar++];
    }
 
    public int readInt() {
        int c = read();
        while (isSpaceChar(c))
            c = read();
        int sgn = 1;
        if (c == '-') {
            sgn = -1;
            c = read();
        }
        int res = 0;
        do {
            if (c < '0' || c > '9')
                throw new InputMismatchException();
            res *= 10;
            res += c - '0';
            c = read();
        } while (!isSpaceChar(c));
        return res * sgn;
    }
    public long readLong() {
        int c = read();
        while (isSpaceChar(c))
            c = read();
        int sgn = 1;
        if (c == '-') {
            sgn = -1;
            c = read();
        }
        long res = 0;
        do {
            if (c < '0' || c > '9')
                throw new InputMismatchException();
            res *= 10;
            res += c - '0';
            c = read();
        } while (!isSpaceChar(c));
        return res * sgn;
    }
 
    public String readString() {
        int c = read();
        while (isSpaceChar(c))
            c = read();
        StringBuffer res = new StringBuffer();
        do {
            res.appendCodePoint(c);
            c = read();
        } while (!isSpaceChar(c));
        return res.toString();
    }
 
    public boolean isSpaceChar(int c) {
        if (filter != null)
            return filter.isSpaceChar(c);
        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
    }
 
    public String next() {
        return readString();
    }
 
    public interface SpaceCharFilter {
        public boolean isSpaceChar(int ch);
    }
}
 
class OutputWriter {
    private final PrintWriter writer;
 
    public OutputWriter(OutputStream outputStream) {
        writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream)));
    }
 
    public OutputWriter(Writer writer) {
        this.writer = new PrintWriter(writer);
    }
 
    public void print(Object...objects) {
        for (int i = 0; i < objects.length; i++) {
            if (i != 0)
                writer.print(' ');
            writer.print(objects[i]);
        }
    }
 
    public void printLine(Object...objects) {
        print(objects);
        writer.println();
    }
 
    public void close() {
        writer.close();
    }}      
