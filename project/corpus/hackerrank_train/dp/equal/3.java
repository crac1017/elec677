import java.util.Scanner;

public class Solution {
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int numTests = sc.nextInt();

		StringBuilder sb = new StringBuilder();

		for (int t = 0; t < numTests; ++t){
			TestCase test = new TestCase(sc);
			sb.append(test.solve()).append("\n");
		}

		System.out.print(sb.toString());
	}
}

class TestCase{
	int numValues;
	int[] values;

	public TestCase(Scanner sc){
		numValues = sc.nextInt();
		values = new int[numValues];
		int mini = Integer.MAX_VALUE;

		for (int idx = 0; idx < numValues; ++idx){
			values[idx] = sc.nextInt();
			mini = Math.min(mini, values[idx]);
		}

		for (int idx = 0; idx < numValues; ++idx){
			values[idx] -= mini;
		}
	}

	public int solve(){
		int result = 0;
		int[][] reminder = new int[][] 
				{{0,1,1,2,2},
				{1,1,2,2,1},
				{1,2,2,1,2}};

		int[] possibilities = new int[3];

		for (int idx = 0; idx < numValues; idx++){
			result += values[idx] / 5;

			for (int p = 0; p < 3; ++p){
				possibilities[p] += reminder[p][values[idx] % 5];
			}
		}

		int min = Math.min(possibilities[0], possibilities[1]);
		min = Math.min(min, possibilities[2]);

		return result + min;
	}
}
