import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int lines = Integer.parseInt(reader.readLine());

        for (int i = 0; i < lines; i++) {
            int amountOfEmployees = Integer.parseInt(reader.readLine());
            String line = reader.readLine();
            if (amountOfEmployees < 0) {
                throw new AssertionError();
            }
            String[] currLine = line.split(" ");
            if (amountOfEmployees != 0 && currLine.length != amountOfEmployees) {
                throw new AssertionError();
            }
            int[] currChocStatus = new int[amountOfEmployees];
            for (int j = 0; j < amountOfEmployees; j++) {
                if (!currLine[j].matches("[0-9]+")) {
                    throw new AssertionError();
                }
                currChocStatus[j] = Integer.parseInt(currLine[j]);
            }
            giveChocolate(currChocStatus);
        }
    }

    private static void giveChocolate(int[] currChocStatus) {
        if (currChocStatus.length == 0) {
            System.out.println(0);
        } else {
            int min = getMinimum(currChocStatus);
            int minSum = Integer.MAX_VALUE;
            for (int i = 0; i < 5; i++) {
                int sum = 0;
                for (int choc : currChocStatus) {
                    int tempChoc = choc;
                    tempChoc -= min;
                    sum += countOperations(tempChoc);
                }
                minSum = Math.min(minSum, sum);
                min--;
            }
            System.out.println(minSum);
        }
    }

    private static int countOperations(int choc) {
        int returner = choc / 5;
        choc %= 5;
        returner += choc / 2;
        returner += choc % 2;
        return returner;
    }

    private static int getMinimum(int[] ints) {
        int min = ints[0];
        for (int i : ints) {
            min = Math.min(min, i);
        }
        return min;
    }
}
