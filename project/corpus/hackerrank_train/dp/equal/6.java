import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

	public static void main(String [] args ) {
		try{
			String str;			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			BufferedOutputStream bos = new BufferedOutputStream(System.out);
			String eol = System.getProperty("line.separator");
			byte [] eolb = eol.getBytes();
			str  = br.readLine();
			int test = Integer.parseInt(str);
			for(int i = 0 ; i < test ; i++) {
				str  = br.readLine();
				int n = Integer.parseInt(str);
				int [] ar = new int[n];
				int j=0;
				int s=0;
				int k =0;
				str = br.readLine();
				int length = str.length();
				int min = Integer.MAX_VALUE;
				while(j<length) {
					while(j<length) {
						if(str.charAt(j) == ' ') {
							break;
						}else {
							j++;
						}
					}
					ar[k] = Integer.parseInt(str.substring(s,j)) ;
					if(ar[k]<min) {
						min = ar[k];
					}
					k++;
					j++;
					s=j;			
				}
				int ans = Integer.MAX_VALUE;
				for(int p = 0 ; p < 11 ; p++) {
					int temp = 0;
					for(int q = 0 ; q < n ; q++) {
						int e = ar[q] - ( min - p );
						int x = e % 5;
						int y = e/5;
						temp += y;
						y =  x / 2;
						x = x % 2;	
						temp += y;
						temp += x;
					}
					if(temp<ans) {
						ans = temp;
					}
				}
				bos.write(new Integer(ans).toString().getBytes());
				bos.write(eolb);
			}
			bos.flush();
		}  catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
