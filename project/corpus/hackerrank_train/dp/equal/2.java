import java.io.BufferedInputStream;
import java.util.Scanner;


public class Solution {
	static int offset = 100;
	static Integer f[][] = new Integer[1111][1111];
	static int a[] = new int[11111];
	
	// From i down to j
	static Integer F(int i, int j) {
		if (i < j) return Integer.MAX_VALUE / 2;
		if (f[i + offset][j + offset] != null) return f[i + offset][j + offset];
		if (i == j) return 0;
		int ans = Integer.MAX_VALUE / 2;
		ans = Math.min(ans, F(i - 1, j) + 1);
		ans = Math.min(ans, F(i - 2, j) + 1);
		ans = Math.min(ans, F(i - 5, j) + 1);
		return f[i + offset][j + offset] = ans;
	}
	public static void main(String[] args) {
		Scanner cin = new Scanner(new BufferedInputStream(System.in));
	
		for (int T = cin.nextInt(); T!=0; T--) {
			int n = cin.nextInt();
			int min = Integer.MAX_VALUE;
			for (int i=0; i<n; i++) {
				a[i] = cin.nextInt();
				min = Math.min(min, a[i]);
			}
			int ans = Integer.MAX_VALUE;
			for (int i=min; i>=min-30; i--) {
				int tmp = 0;
				for (int j=0; j<n; j++) {
					tmp += F(a[j], i);
				}
				ans = Math.min(ans, tmp);
			}
			System.out.println(ans);
		}
	}
}
