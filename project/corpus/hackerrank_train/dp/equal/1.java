
import java.awt.Point;
import java.io.*;
import java.math.BigInteger;
import java.util.*;
import static java.lang.Math.*;

public class Solution implements Runnable {

    BufferedReader in;
    PrintWriter out;
    StringTokenizer tok = new StringTokenizer("");

    public static void main(String[] args) {
        new Thread(null, new Solution(), "", 256 * (1L << 20)).start();
    }

    public void run() {
        try {
            long t1 = System.currentTimeMillis();
            out = new PrintWriter(System.out);

            in = new BufferedReader(new InputStreamReader(System.in));
            //in = new BufferedReader(new FileReader("src/input.txt"));
            //   out = new PrintWriter("output.txt");

            Locale.setDefault(Locale.US);
            solve();
            in.close();
            out.close();
            long t2 = System.currentTimeMillis();
            System.err.println("Time = " + (t2 - t1));
        } catch (Throwable t) {
            t.printStackTrace(System.err);
            System.exit(-1);
        }
    }

    String readString() throws IOException {
        while (!tok.hasMoreTokens()) {
            tok = new StringTokenizer(in.readLine());
        }
        return tok.nextToken();
    }

    int readInt() throws IOException {
        return Integer.parseInt(readString());
    }

    long readLong() throws IOException {
        return Long.parseLong(readString());
    }

    double readDouble() throws IOException {
        return Double.parseDouble(readString());
    }

    // solution
    void solve() throws IOException {
        int testcases = readInt();
        while (testcases-- > 0) {
            int n = readInt();
            int min = Integer.MAX_VALUE;
            int[] a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = readInt();
                min = Math.min(min, a[i]);
            }
            int bestResult = Integer.MAX_VALUE / 2;
            for (int borderline = min; borderline >= 0 && borderline >= min - 20; borderline--) {
                int result = 0;
                for (int i = 0; i < n; i++) {
                    result += (a[i] - borderline) / 5;
                    result += (a[i] - borderline) % 5 / 2;
                    result += (a[i] - borderline) % 5 % 2 / 1;
                }
                bestResult = Math.min(bestResult, result);
            }
            out.println(bestResult);
        }
    }
}
