import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        String split[] = s.split(", ");
        int c[] = new int[split.length + 1];
        int m = split.length;
        
        for (int i = 0 ; i < split.length; i++)
            c[i + 1] = Integer.parseInt(split[i].trim());
        int sum = scan.nextInt();
        scan.close();
        /*
            platesc sum i cu j banc
        */
        int p[][] = new int[sum + 1][m + 1];
       ;
        for (int i = 0; i <= sum; i++) {
            for (int j = 0; j <= m; j++) {
                if (i == 0)
                    p[i][j] = 1;
                else if (j == 0) {
                    p[i][j] = 0;
                } else
                if (c[j] > i) {
                    p[i][j] = p[i][j-1];
                } else {
                    p[i][j] = p[i][j-1] + p[i - c[j]][j];
                }
            }
        }
        System.out.println(p[sum][m]);
    }
}
