import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String line= in.nextLine();
        int amt = in.nextInt();
        String[] nums = line.split(",");
        int[] coins = new int[nums.length];
        for(int i = 0; i < nums.length; i++){
            coins[i] = Integer.parseInt(nums[i].trim());
        }
        int[] ways = new int[amt+1];
        for(int i = 0; i <= amt; i++)
            ways[i] = 0;
        ways[0] = 1;
        for(int i = 0; i < coins.length; i++)
            for(int j = coins[i]; j <= amt; j++)
                ways[j] += ways[j-coins[i]];
         System.out.println(ways[amt]);
    }
}
