import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        StringTokenizer st = new StringTokenizer(input,", ");
        int tokenCount = st.countTokens();
        int[] coins = new int[tokenCount];
        int i = 0;
        for(; i < tokenCount; i++) {
            coins[i] = Integer.parseInt(st.nextToken());
        }
        int n = sc.nextInt();
        
        n = findCoinChange(n, coins);
        System.out.println(n);
    }
    
    private static int findCoinChange(int n, int[] coins) {
        int[] table = new int[n+1];
        table[0] = 1;
        for(int i = 0; i < coins.length; i++) {
            for(int j = coins[i]; j <= n; j++) {
                table[j] += table[j-coins[i]];
            }
        }
        return table[n];
    }
}
