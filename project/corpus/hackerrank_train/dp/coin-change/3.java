import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    static int[] coins = null;
    //static int[] coins = null;
    // OPT (n, c): number of coins for amount n if max coin used is c.
    // OPT (n, c): sum of OPT(n-ci, ci) for ci <= c
    // return OPT(n, ci) for all ci where 0 <= i<= m
    public static int makeChange2(int n) {
        int[][] cache = new int[n+1][coins.length];
        
        for (int c=0; c<coins.length; c++)
            makeChange2(n, c, cache);

        int sum = 0;
        for (int c=0; c<coins.length; c++) {
            sum += cache[n][c];
        }
        
        //for (int i=0; i<n+1; i++) System.out.println(Arrays.toString(cache[i]));
        return sum;
    }
    
    public static int makeChange2(int n, int c, int[][] cache) {
        if (n<0 || c < 0 || n-coins[c] < 0)  return 0;
        if (cache[n][c] > 0)    return cache[n][c];
        if (n-coins[c]==0) {
            cache[n][c] = 1;
            return 1;
        }
        
        int sum = 0;
        for (int i=0; i<=c; i++) {
            sum += makeChange2(n-coins[c], i, cache);
        }
        cache[n][c] = sum;
        return sum;
    }
    
    public static void main(String[] args) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        String line = in.nextLine();
        String[] sa = line.split(", ");
        
        coins = new int[sa.length];
        for(int i=0; i < sa.length; i++) {
            int a = Integer.parseInt(sa[i].trim());
            coins[i] = a;
        }
        int N = in.nextInt();
        
        System.out.println(makeChange2(N));
    }
}
