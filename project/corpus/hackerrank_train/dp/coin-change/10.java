import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    public static int ways(int[] a, int n){
        int len = a.length;
        int[] dp = new int[n+1];
        dp[0] = 1;
        
        for(int i=0;i<len;i++)
            for(int j=a[i];j<=n;j++)
                dp[j]+=dp[j-a[i]];
        
        return dp[n];
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        int n = Integer.parseInt(sc.nextLine().replace(" ",""));
        line = line.replace(" ","");
        String[] temp = line.split(",");
        int[] coins = new int[temp.length];
        for(int i=0;i<temp.length;i++)
            coins[i] = Integer.parseInt(temp[i]);
        
        System.out.println(ways(coins, n));
    }
}
