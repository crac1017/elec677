import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String[] tokens = s.nextLine().replace(" ", "").split(",");
        int[] S = new int[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            S[i] = Integer.parseInt(tokens[i]);
        }
        int n = s.nextInt();
        System.out.println(change(S, S.length, n));
	}

	private static int change(int[] S, int m, int n) {
		int[] table = new int[n + 1];
		table[0] = 1;
		for (int i = 0; i < m; i++) {
			for (int j = S[i]; j <= n; j++) {
				table[j] += table[j - S[i]];
			}
		}
		return table[n];
	}
}
