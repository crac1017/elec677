import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] sarr = reader.readLine().split(",");
        int[] arr = new int[sarr.length];
        for(int i=0;i<sarr.length;++i){
            arr[i]=Integer.valueOf(sarr[i].trim());
        }
        int amount = Integer.valueOf(reader.readLine().trim());
        System.out.println(totalChanges(arr,amount));
    }
    
    public static int totalChanges(int[] arr,int amount){
        int[][] table = new int[amount+1][arr.length+1];
        for(int i=0;i<arr.length;++i){
            table[0][i]=1;
        }
        for(int i=1;i<=amount;++i){
            for(int j =0;j<arr.length;++j){
                if(arr[j] <= i){
                    table[i][j]+=table[i-arr[j]][j];
                }
                if(j >= 1){
                    table[i][j]+=table[i][j-1];
                }
            }
        }
        return table[amount][arr.length-1];
    }
    
}
