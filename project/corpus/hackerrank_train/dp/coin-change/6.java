import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.TreeMap;


public class Solution {
	static LinkedList<Integer> coins = new LinkedList<Integer>();
	
	static HashMap<Integer, HashMap<Integer, Integer> > memo = new HashMap<Integer, HashMap<Integer,Integer> >();
	
	public static void main(String[] args) throws IOException {
		Reader.init(System.in);
		
		boolean done = false;
		while (!done) {
			String s = Reader.next();
			if (s.charAt(s.length()-1) == ',')
				coins.add(Integer.valueOf(s.substring(0,s.length()-1)));
			else {
				coins.add(Integer.valueOf(s));
				done = true;
			}
		}
		int N = Reader.nextInt();
	
		System.out.println(calcChange(N, Integer.MAX_VALUE));
	}
	
	static int calcChange(int n, int max) {
		if (n < 0) return 0;
		if (n == 0) return 1;
		if (!memo.containsKey(n)) {
			memo.put(n, new HashMap<Integer,Integer>());
		} else if (memo.get(n).containsKey(max)) {
			return memo.get(n).get(max);
		}
		
		int retVal = 0;
		for (int c: coins) {
			if (c < max) {
				int i = 1;
				while (n - i*c >= 0) {
					retVal += calcChange(n-i*c, c);
					i++;
				}
			}
		}
		memo.get(n).put(max, retVal);
		return retVal;
	}
}


/** Class for buffered reading int and double values */
class Reader {
    static BufferedReader reader;
    static StringTokenizer tokenizer;

    /** call this method to initialize reader for InputStream */
    static void init(InputStream input) {
        reader = new BufferedReader(
                     new InputStreamReader(input) );
        tokenizer = new StringTokenizer("");
    }

    /** get next word */
    static String next() throws IOException {
        while ( ! tokenizer.hasMoreTokens() ) {
            //TODO add check for eof if necessary
            tokenizer = new StringTokenizer(
                   reader.readLine() );
        }
        return tokenizer.nextToken();
    }

    static int nextInt() throws IOException {
        return Integer.parseInt( next() );
    }
	
    static double nextDouble() throws IOException {
        return Double.parseDouble( next() );
    }
}
