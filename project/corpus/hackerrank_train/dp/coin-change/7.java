import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        ArrayList<Integer> dm = new ArrayList<Integer>();
		int sum;
		int dmLen = 0;
		int A[] []; 
		int B[][] ; 
	    int cnt = 0;

		
		Scanner scanner = new Scanner(System.in);
		
		String input = scanner.nextLine();
		/*while(scanner.hasNextInt()){
			dm.add( scanner.nextInt());
		}*/
		
		String[] in = input.split(","); 
		int i =0;
		
		while (i <=(in.length - 1)) {
			in[i].trim();
			dm.add(Integer.parseInt(in[i].trim()));
			i++;
		}
		
		
		dm.add(Integer.parseInt(scanner.nextLine().trim()));
		
		
		sum = dm.remove(dm.size() - 1);
		dmLen = dm.size();
		A = new int[sum+1][dmLen];
		for (i=0; i<dmLen; i++)
	        A[0][i] = 1;
		
		Collections.sort(dm);
	    
	 
	    // Fill rest of the table enteries in bottom up manner 
	    for ( i = 1; i < sum+1; i++)
	    {
	        for (int j = 0; j < dmLen; j++)
	        {
	            // Count of solutions including S[j]
	            int x = (i-dm.get(j) >= 0)? A[i - dm.get(j)][j]: 0;
	 
	            // Count of solutions excluding S[j]
	            int y = (j >= 1)? A[i][j-1]: 0;
	 
	            // total count
	            A[i][j] = x + y;
	        }
	    }
		
		
		System.out.print(A[sum][dmLen-1]);
		
    }
}
