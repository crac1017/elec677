import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        BigInteger a = BigInteger.valueOf(in.nextInt());
		BigInteger b = BigInteger.valueOf(in.nextInt());

		int n = in.nextInt();
		n -= 2;
		while (n-- > 0) {
			BigInteger c = b.multiply(b).add(a);
			a = b;
			b = c;
		}

		System.out.println(b.toString());
    }
}
