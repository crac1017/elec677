import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;
public class Solution {

	public static void main(String[] args) throws IOException {
		int numT = 1;
		Scanner in = new Scanner(System.in);
		for (int t = 0; t < numT; t++) {
			BigInteger a = BigInteger.valueOf(in.nextInt());
			BigInteger b = BigInteger.valueOf(in.nextInt());
			int n = in.nextInt();
			
			for(int i = 3; i <= n; i ++)
			{
				BigInteger x = b;
				b = b.multiply(b).add(a);
				a = x;
			}
			System.out.println(b);
		}
	}
}
