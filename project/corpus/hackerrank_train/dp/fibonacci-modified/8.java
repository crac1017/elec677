import java.util.*;
import java.lang.*;
import java.math.BigInteger;

public class template1 {
	
	private static void solveProblem(){
		Scanner scan = new Scanner(System.in);
		
		/*
		long a = scan.nextInt();
		long b = scan.nextInt();
		int n = scan.nextInt();
		long answer = 0;
		long temp = 0;*/
		
		BigInteger a = BigInteger.valueOf(scan.nextInt());
		BigInteger b = BigInteger.valueOf(scan.nextInt());
		int n = scan.nextInt();
		
		for(int i = 3; i <= n; i++){
			BigInteger temp = b;
			b = (b.multiply(b)).add(a);
			a = temp;
		}
		
		System.out.println(b);
		return ;
	}

	
	public static void main(String[] args) {
		solveProblem();	
	}
}
