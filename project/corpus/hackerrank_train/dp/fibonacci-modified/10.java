import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
	int a = in.nextInt();
	int b = in.nextInt();
	int n = in.nextInt();
        BigInteger[] seq = new BigInteger[n];
	seq[0] = new BigInteger(Integer.toString(a));
	seq[1] = new BigInteger(Integer.toString(b));
	for (int counter = 2; counter < n; counter++)
	    seq[counter] = seq[counter - 1].pow(2).add(seq[counter - 2]);
	System.out.println(seq[n - 1]);
    }
}
