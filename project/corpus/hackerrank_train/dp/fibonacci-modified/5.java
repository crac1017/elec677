import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
      public static void main(String[] args) {
    Scanner cin = new Scanner(System.in);
    while(cin.hasNextInt()) {
      int A = cin.nextInt();
      int B = cin.nextInt();
      int N = cin.nextInt();
      BigInteger[] vals = new BigInteger[N];
      vals[0] = BigInteger.valueOf(A);
      vals[1] = BigInteger.valueOf(B);
      for (int i = 2; i < N; i++) {
        vals[i] = (vals[i-1].pow(2)).add(vals[i-2]);
      }
      System.out.println(vals[N-1]);
    }
  }
}
