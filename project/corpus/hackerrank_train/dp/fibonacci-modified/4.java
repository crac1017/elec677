import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int A = sc.nextInt(), B = sc.nextInt(), N = sc.nextInt();
		sc.close();
		BigInteger[] T = new BigInteger[N];
		T[0] = BigInteger.valueOf(A);
		T[1] = BigInteger.valueOf(B);
		for (int i = 2; i < N; i++) {
			T[i] = T[i - 1].multiply(T[i - 1]).add(T[i - 2]);
		}
		System.out.println(T[N - 1]);
	}
}
