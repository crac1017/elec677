import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
		BigInteger a = BigInteger.valueOf(in.nextInt());
		BigInteger b = BigInteger.valueOf(in.nextInt());
		BigInteger[] series = new BigInteger[in.nextInt()];
		series[0] = a;
		series[1] = b;
		for (int i = 2; i < series.length; i++){
			series[i] = series[i - 1].multiply(series[i - 1]).add(series[i - 2]);
		}
		System.out.println(series[series.length - 1]);
    }
}
