import java.io.*;
import java.util.*;
import java.math.*;

public class Solution{
	public static void main(String[] args){
		int a, b, n;
		Scanner in = new Scanner(System.in);
		a = in.nextInt();
		b = in.nextInt();
		n = in.nextInt();

		BigInteger lol[] = new BigInteger[25];
		lol[1] = BigInteger.valueOf(a);
		lol[2] = BigInteger.valueOf(b);

		for(int i = 3; i <= n; i++){
			lol[i] = lol[i-1].multiply(lol[i-1]).add(lol[i-2]);
		}
		System.out.println(lol[n]);
	}
}
