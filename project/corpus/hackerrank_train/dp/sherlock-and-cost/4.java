import java.io.BufferedReader;
import java.util.Comparator;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.math.BigInteger;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 */
public class Solution {
	public static void main(String[] args) {
		InputStream inputStream = System.in;
		OutputStream outputStream = System.out;
		InputReader in = new InputReader(inputStream);
		PrintWriter out = new PrintWriter(outputStream);
		TaskC solver = new TaskC();
		int testCount = Integer.parseInt(in.next());
		for (int i = 1; i <= testCount; i++)
			solver.solve(i, in, out);
		out.close();
	}
}

class TaskC {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt();
        int[] b = new int[n];
        for (int i = 0; i < n; ++i) b[i] = in.nextInt();
        int[][] f = new int[2][2];
        ArrayUtils.fill(f, -1);
        f[0][0] = 0;
        f[0][1] = 0;
        for (int i = 0; i < n-1; ++i) {
            for (int j = 0; j < 2; ++j) {
                int cur = f[0][j];
                int prevB = (j == 0 ? 1 : b[i]);

                f[1][0] = Math.max(f[1][0], cur + Math.abs(prevB - 1));
                f[1][1] = Math.max(f[1][1], cur + Math.abs(prevB - b[i+1]));
            }
            for (int j = 0; j < 2; ++j) {
                f[0][j] = f[1][j];
                f[1][j] = -1;
            }
        }
        out.println(Math.max(f[0][0], f[0][1]));
    }
}

class InputReader {
    private BufferedReader reader;
    private StringTokenizer stt;

    public InputReader(InputStream stream) {
        reader = new BufferedReader(new InputStreamReader(stream));
    }

    public String nextLine() {
        try {
            return reader.readLine().trim();
        } catch (IOException e) {
            return null;
        }
    }

    public String nextString() {
        while (stt == null || !stt.hasMoreTokens()) {
            stt = new StringTokenizer(nextLine());
        }
        return stt.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt(nextString());
    }

    public String next() {
        return nextString();
    }
}

class ArrayUtils {

    public static void fill(int[][] f, int value) {
        for (int i = 0; i < f.length; ++i) {
            Arrays.fill(f[i], value);
        }
    }

}

