import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.Arrays;
import java.util.Random;

public class Solution {

	public static void main(String[] args) throws Exception {
		new Solution().run();
	}

	StreamTokenizer st;

	int n, a[], r2, cur[];

	private void run() throws Exception {
		st = new StreamTokenizer(new BufferedReader(new InputStreamReader(
				System.in)));
		int t = nextInt();
		a = new int[100000];
		int[][] b = new int[2][100000];
		while (t-- > 0) {
			n = nextInt();
			//n = 10;
			for (int i = 0; i < n; i++) {
				a[i] = nextInt();
				//a[i] = rr.nextInt(5) + 1;
			}
			for (int i = 1; i < n; i++) {
				b[0][i] = Math.max(b[1][i - 1] + a[i - 1] - 1, b[0][i - 1]);
				b[1][i] = Math.max(b[0][i - 1] + a[i] - 1, b[1][i - 1] + Math.abs(a[i] - a[i - 1]));
			}
			int r = Math.max(b[0][n - 1], b[1][n - 1]);
//			System.out.println(Arrays.toString(b[0]));
//			System.out.println(Arrays.toString(b[1]));
//			r2 = 0;
//			dumb(0, 0, -1);
			System.out.println(r);
//			System.out.println(r2);
//			System.out.println(Arrays.toString(a));
		}
	}

	private void dumb(int i, int sum, int prev) {
		if (i == n) {
			if (r2 < sum) {
				r2 = sum;
				System.out.println(r2);
				System.out.println(Arrays.toString(cur));
			}
			return;
		}
		for (int j = 1; j <= a[i]; j++) {
			int add = 0;
			if (prev != -1)
				add += Math.abs(j - prev);
			cur[i] = j;
			dumb(i + 1, sum + add, j);
		}
	}

	private int nextInt() throws Exception {
		st.nextToken();
		return (int) st.nval;
	}

}
