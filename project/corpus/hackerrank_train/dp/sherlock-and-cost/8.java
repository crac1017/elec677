import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;


public class Solution {	
	BufferedReader reader;
    StringTokenizer tokenizer;
    PrintWriter out;

    int INF = Integer.MAX_VALUE / 2;
	public void solve() throws IOException {
        int T = nextInt();
        for (int t = 0; t < T; t++) {

            int N = nextInt();
            int[] B = new int[N];
            for (int i = 0; i < N; i++) {
                B[i] = nextInt();
            }

            int[] dp = new int[101];
            Arrays.fill(dp, -INF);
            for (int i = 1; i <= B[0]; i++) {
                dp[i] = 0;
            }

            for (int i = 1; i < N; i++) {
//                out.println("loop: " + i);

                int[] old = dp;
                dp = new int[101];
                Arrays.fill(dp, -INF);

                int max_value = -INF;
                for (int k = 1; k <= B[i]; k++) {
                    if (old[k] - k > max_value) {
                        max_value = old[k] - k;
                    }
                    dp[k] = max_value + k;
                }

                max_value = -INF;
                for (int k = 100; k >= 1; k--) {
                    if (old[k] + k > max_value) {
                        max_value = old[k] + k;
                    }
                    if (k <= B[i]) {
                        dp[k] = Math.max(dp[k], max_value - k);
                    }
                }

//                int[] values1 = new int[101];
//                int[] values2 = new int[101];
//                for (int j = 1; j < 101; j++) {
//                    values1[j] = old[j] - j;
//                    values2[j] = old[j] + j;
//                }
//                RMQSparseTable rmq1 = new RMQSparseTable(values1);
//                RMQSparseTable rmq2 = new RMQSparseTable(values2);
//
////                out.println(rmq2.maxPos(0, 100) + ": " + values2[rmq1.maxPos(0, 100)]);
//
//                for (int k = 1; k <= B[i]; k++) {
//                    int best1 = values1[rmq1.maxPos(1, k)] + k;
//                    int best2 = values2[rmq2.maxPos(k, 100)] - k;
//
//
//
//                    dp[k] = Math.max(best1, best2);
//                }
            }


            int best = -INF;
            for (int i = 1; i < 101; i++) {
                best = Math.max(best, dp[i]);
            }
            out.println(best);
        }
	}

    public class RMQSparseTable {

        int[] logTable;
        int[][] rmq;
        int[] a;

        public RMQSparseTable(int[] a) {
            this.a = a;
            int n = a.length;

            logTable = new int[n + 1];
            for (int i = 2; i <= n; i++)
                logTable[i] = logTable[i >> 1] + 1;

            rmq = new int[logTable[n] + 1][n];

            for (int i = 0; i < n; ++i)
                rmq[0][i] = i;

            for (int k = 1; (1 << k) < n; ++k) {
                for (int i = 0; i + (1 << k) <= n; i++) {
                    int x = rmq[k - 1][i];
                    int y = rmq[k - 1][i + (1 << k - 1)];
                    rmq[k][i] = a[x] >= a[y] ? x : y;
                }
            }
        }

        public int maxPos(int i, int j) {
            int k = logTable[j - i];
            int x = rmq[k][i];
            int y = rmq[k][j - (1 << k) + 1];
            return a[x] >= a[y] ? x : y;
        }

    }
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Solution().run();
	}
	
	public void run() {
        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            tokenizer = null;
            out = new PrintWriter(System.out);
            solve();
            reader.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    int nextInt() throws IOException {
        return Integer.parseInt(nextToken());
    }

    long nextLong() throws IOException {
        return Long.parseLong(nextToken());
    }

    double nextDouble() throws IOException {
        return Double.parseDouble(nextToken());
    }

    String nextToken() throws IOException {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            tokenizer = new StringTokenizer(reader.readLine());
        }
        return tokenizer.nextToken();
    }

}
