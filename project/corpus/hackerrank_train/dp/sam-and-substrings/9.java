
import java.util.*;
public class Solution
    {
    public static void main(String args[])
        {
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        long last = 0;
        long ans = 0;
        for(int i = 0; i < input.length(); i++)
            {
            last = ((input.charAt(i)-'0')*(1+i)+10*last)% 1000000007;
            
            ans = (ans+last)% 1000000007;
           
            
        }
        System.out.println(ans);
    }
}
