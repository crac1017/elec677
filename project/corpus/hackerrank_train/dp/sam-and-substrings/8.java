import java.util.*;

public class Solution {
    static final long P = 1000000007;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String line = in.nextLine();
        long t = 0;
        long p = 0;

        for(int i = 0; i < line.length(); i++) {
            long ch = (long) (line.charAt(i) - '0');
            p = (p * 10 + ch * (i+1)) % P;
            t = (t + p) % P;
        }
        System.out.println(t);
    }
}
