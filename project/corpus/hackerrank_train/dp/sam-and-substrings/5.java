import java.util.Scanner;


public class Solution {

	static long mod = 1000000007;
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		char s[] = cin.next().toCharArray();
		long ten[] = new long[s.length + 2];
		
		ten[0] = 1;
		for (int i=1; i<ten.length; i++) {
			ten[i] = ten[i - 1] * 10 + 1;
			ten[i] %= mod;
		}
		
		long ans = 0;
		for (int i=0; i<s.length; i++) {
			ans += ((s[i] - '0') * ten[s.length - i - 1]) % mod * (i + 1) % mod;
			ans %= mod;
		}
		
		System.out.println(ans);
		cin.close();
	}

}
