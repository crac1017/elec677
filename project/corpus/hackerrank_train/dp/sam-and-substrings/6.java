import java.util.*;
import java.io.*;

class Solution{
    public static void main(String args[]) throws IOException{
        int mod = 1000000007;
        
        BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
        String s = f.readLine();
        long ans = 0;
        long k = 1;
        for(int i = s.length() - 1; i >= 0; --i){
            long digit = Long.parseLong(s.substring(i, i + 1));
            ans += (digit % mod) * ((i + 1) % mod) * (k % mod);
            ans %= mod;
            k *= 10;
            k++;
            k %= mod;
        }
        System.out.println(ans);
    }
}
