import java.io.*;
import java.util.*;

public class Solution {
	
public static void main(String[] args) {
		   Scanner in = new Scanner(System.in);
		   String number = in.next();
		   int l = number.length();
		   char[] nArray = number.toCharArray();
		   long ones = 1;
		   long result = 0;
		   for(int i=l-1;i>=0;i--)
		   {
				int digit = nArray[i] - '0';
				long tmp = (ones * digit * (i + 1))%1000000007;
				result = (result + tmp )%1000000007;
				ones = (ones * 10 + 1 )%1000000007;
		   }
		   
			System.out.print(result);
	   }    
   }
   
