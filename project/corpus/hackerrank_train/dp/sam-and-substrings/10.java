import java.util.*;
public class Solution {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        double mod = 1000000007;
        String x = s.next();
        double sum = 0;
        double tmp = 0;
        for (int i = 0; i < x.length(); i++) {
            sum = (sum * 10) % mod;
            sum = (tmp + sum) % mod;
            int k = x.charAt(i) - '0';
            tmp = (tmp + (k * (i + 1))) % mod;
            sum = (sum + (k * (i + 1))) % mod;
        }
        System.out.println((int) sum);
    }
}
