import java.io.PrintStream;
import java.util.Scanner;

/**
 * Date: 12/19/11
 * Time: 10:48 PM
 */
public class Solution
{
    public static void main(String[] args)
    {
        solve(new Scanner(System.in), System.out);
    }

    private static void solve(Scanner sc, final PrintStream output)
    {
        int c = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < c; i++)
        {
            solveCase(sc, output);
        }
    }

    private static void solveCase(Scanner sc, PrintStream output)
    {
        int n = Integer.parseInt(sc.nextLine());
        Scanner lineScanner = new Scanner(sc.nextLine());
        int[] input = new int[n];
        for(int i = 0; i < n; i++){
            input[i] = lineScanner.nextInt();
        }
        output.printf("%.2f\n",getExpectedValue(input));
    }

    private static double getExpectedValue(int[] input)
    {
        double sum = 0;

        for(int i = 0; i < input.length; i++){
            sum += getEvElement(input, i);
        }

        return sum;
    }

    private static double getEvElement(int[] input, int index)
    {
        int eq = 0;
        int l = 0;
        final int n = input[index];
        for(int i = 0; i < input.length; i++){
            if (i == index) continue;

            if (input[i] < n){
                l++;
            }
            else {
                eq++;
            }
        }

        double sum = 0;
        double last = 0;
        for (int x = 1; x <= input.length; x++){
            if (x > l + 1){
                sum += last;
            }
            else {
                last = calculateEvAtX(x, eq, l);
                sum += last;
            }
        }
        return sum/input.length;
    }

    private static double calculateEvAtX(int location, int eq, int l)
    {
        int maxLength = (location > (l + 1)) ? l + 1 : location;

        double sum = 0;
        for (int length = 1; length <= maxLength; length++){
            int slot = length - 1;
            double weight = calculateWeight(slot, eq, l);
            if (length < maxLength) {
                weight = (weight * eq) / (eq + l - slot);
            }
            sum += length * weight;
        }

        return sum;
    }

    private static double calculateWeight(int slot, int eq, int l)
    {
        double num = 1D;
        double den = 1D;
        int total = eq + l;

        for (int i = 0; i < slot; i++){
            num *= l - i;
            den *= total - i;
        }

        return num / den ;
    }
}
