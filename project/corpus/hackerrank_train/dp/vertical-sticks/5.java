
import java.io.* ;
import java.text.DecimalFormat;
import java.util.*;
import static java.lang.Math.* ;
import static java.util.Arrays.* ;

public class Solution {
	
	public static void main(String[] args) {
		
		int n = in.nextInt() ; in.nextLine() ;
		
		while( n-- > 0)
			new Solution().solveProblem();
		
		out.close();
	}

	static Scanner in = new Scanner(new InputStreamReader(System.in));
	static PrintStream out = new PrintStream(new BufferedOutputStream(System.out));
	//static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	
	int n ;
	int[] get ;
	double[][] cache ;
	public void solveProblem() {		

		n = in.nextInt() ;
		
		get = new int[n] ;
		for( int i = 0 ; i < n ; i++ )
			get[i] = in.nextInt() ;
		
		sort(get) ;
		
		cache = new double[n+1][n+1] ;
		for( int i = 0 ; i <= n ; i++)
			fill(cache[i],-1.0) ;
		
		double gem = 0.0 ;
		
		for( int i = 0 ; i < n ; i++ ){
			int j = i ;
			while( j + 1 < n && get[i] == get[j+1])
				j++ ;
			
			gem += (double) (j-i+1) * geef(i,n-i-1) ;
			
			i = j ;
		}
	
		DecimalFormat formatter = new DecimalFormat("0.00");
		out.println(formatter.format(gem).replace(',', '.')) ;
	}
	
	private double geef(int lager, int hoger) {
		
		if( hoger == 0 )
			return (double) lager / 2.0 + 1.0 ;
		
		if( cache[lager][hoger] >= -0.5)
			return cache[lager][hoger] ;

		double nu = 0.0 ;
		if( lager > 0 )
			nu = (double) lager / (double) (lager+hoger) * (1.0 + geef(lager-1,hoger)) ;
	
		nu += (double) hoger / (double) (lager+hoger) * 1.0 ;
		nu *= (double) (lager+hoger)/(double) (lager+hoger+1) ;

		nu += 1.0 / (double) (lager+hoger+1) ;
		//System.out.println(lager + " " + hoger +  " " + nu);
		return cache[lager][hoger] = nu;
	}
       
}
