import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;


public class Solution {
public static void main(String[] args) {
	
	Scanner scanner = new Scanner(System.in);
	int t = scanner.nextInt();
	while(t>0){
		t--;
		int n = scanner.nextInt();
		int total = n;
		HashMap<Integer,Integer> unique = new HashMap<Integer,Integer>();
		while(n>0){
			n--;
			int next = scanner.nextInt();
			if(unique.containsKey(next)) unique.put(next, unique.get(next)+1);
			else unique.put(next,1);
		}
		int[] elements = new int[unique.size()];
		int ind = 0; 
		for(int i: unique.keySet()){
		 elements[ind] = i;
		 ind++;
		}
		Arrays.sort(elements);
		int[] sum = new int[elements.length];
		sum[0] = unique.get(elements[0]);
		for(int i=1; i<elements.length; i++){
			sum[i] = sum[i-1] + unique.get(elements[i]);
		}
	    double ans = 0;
	    for(int i: elements){
	    	int q = Arrays.binarySearch(elements,i);
	    	int less = 0;
	    	if(q>0) less = sum[q-1];
	    	double curr = 0;
	    	for(int d = 0; d<=total-1; d++){
	    		curr += getContribution(total-less-1, less, d, total);
	    		
	    	}
	    	ans += curr * unique.get(i);
	    }
	    System.out.println(new DecimalFormat("0.00").format(ans));
		
	}
	
}
public static double getContribution(int p, int q, int x, int n){
	
	double c = 0;
	for(int d=1; d<=x; d++){
		//if(d-1<=q && x==2) System.out.println("adding "+(d*p*fact(x-d)*getPer(q,d-1)));
		if(d-1<=q)c += d*p*fact(x-d)*getPer(q,d-1)*getCom(n-d-1, n-x-1);
	}
	if(q>=x)c+= (x+1)*getPer(q,x);
	
	//System.out.println(c+" with per "+getPer(n,x+1)+" and retunring "+(c/getPer(n,x+1)));
	
	return (c*fact(n-x-1)/fact(n));
}

private static double fact(int a) {
    double total = 1;
    for (int b=a;b>=1;b--) {
        total *= b;
    }
    return total;
}

public static double getPer(int n, int r){

	return fact(n)/(fact(n-r));
}

public static double getCom(int n, int r){

	return fact(n)/(fact(n-r)*fact(r));
	
}

}
