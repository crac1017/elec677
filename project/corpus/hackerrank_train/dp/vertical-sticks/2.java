import java.util.*;

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int cases = in.nextInt();
		while(cases-->0) {
			int numbers = in.nextInt();
			int[] data = new int[numbers];
			for(int i=0;i<numbers;++i) {
				data[i] = in.nextInt();
			}	
			Arrays.sort(data); // necessary? don't think so.
			double answer = 0.0;
			for(int i=0;i<numbers;++i) {
				int totalSmaller = i;
				
				for(int j=i-1;j>=0;--j) {
					if (data[j] == data[i]) {
						--totalSmaller;
					}
					else {
						break;
					}
				}
				for(int j=1;j<=numbers;++j) {
					int smaller = totalSmaller;
					// j is the location that data[i] is going in the array
					double thisValue = 0.0; // this number's contribution to the final answer
					double probContinue = 1;
					for(int k=j-1;k>=0;--k) {
						if (smaller == 0 || k == 0) {
							thisValue += probContinue * (j-k);
							break;
						}
						int numbersLeft = numbers - (j-k-1) - 1;
						double probThisOneNotSmaller = 1 - (1.0*smaller/numbersLeft);
						thisValue += (j-k)*probContinue*probThisOneNotSmaller;
						--smaller;
						probContinue *= (1-probThisOneNotSmaller);
					}
					// System.err.println(thisValue);
					answer += thisValue/numbers;
				}
			}
			System.out.printf("%.2f\n", answer);
		}
	}
}
