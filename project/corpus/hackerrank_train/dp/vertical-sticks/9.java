/* Enter your code here. Read input from STDIN. Print output to STDOUT */

import java.util.*;
import java.text.*;

public class Solution{

	public static double[] factorial = new double[51];
	public static ArrayList<Integer> rawdata = new ArrayList<Integer>();
	public static ArrayList<Integer> data = new ArrayList<Integer>();
	public static int TotalTest;
	public static int TotalSticks;
	public static double answer;

	public static void main(String[] args){
		double times=1;
		factorial[0]=times;
		for(int i=1;i<=50;i++){
			times *= i;
			factorial[i] = times;
		}
		Scanner sc = new Scanner(System.in);
		TotalTest = sc.nextInt();
		for(int t=0;t<TotalTest;t++){
			TotalSticks = sc.nextInt();
			rawdata.clear();	data.clear();
			for(int i=0;i<TotalSticks;i++){
				rawdata.add(sc.nextInt());
			}
			for(int i=0;i<TotalSticks;i++){
				int count=1;
				for(int j=0;j<TotalSticks;j++){
					if(j==i) continue;
					if(rawdata.get(j)>=rawdata.get(i)) count++;
				}
				data.add(count);
			}
			answer = 0;
			for(int i=0;i<TotalSticks;i++){
				int a= data.get(i);
				for(int j=0;j<=(TotalSticks-a);j++){
					answer += (factorial[TotalSticks-a]/factorial[TotalSticks-a-j]*factorial[TotalSticks-1-j]*a*(j+1));
					//System.out.println(answer+" a:"+a);
				}
			}
			answer /= factorial[TotalSticks];
			System.out.printf("%.2f", answer);
			System.out.println();
		}
	}

}
