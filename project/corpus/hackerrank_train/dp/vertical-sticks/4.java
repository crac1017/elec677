/* Enter your code here. Read input from STDIN. Print output to STDOUT */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution
{
	public static void main(final String[] args)
	{
		final List<Double> sums = new Solution().calculateExpectedSums(new Scanner(System.in));

		for (final Double sum : sums)
		{
			System.out.println(String.format("%.2f", sum));
		}
	}

	List<Double> calculateExpectedSums(final Scanner scanner)
	{
		final int count = scanner.nextInt();

		final List<Double> sums = new ArrayList<Double>(count);

		for (int i = 0; i < count; i++)
		{
			sums.add(expectedSequenceLengthSum(readNumbers(scanner)));
		}

		return sums;
	}

	protected boolean isDebugEnabled()
	{
		return false;
	}

	List<Integer> readNumbers(final Scanner scanner)
	{
		final int count = scanner.nextInt();

		final List<Integer> numbers = new ArrayList<Integer>(count);

		for (int i = 0; i < count; i++)
		{
			numbers.add(scanner.nextInt());
		}

		return numbers;
	}

	double expectedSequenceLengthSum(final List<Integer> numbers)
	{
		double expectedSequenceLengthSum = 0.0;

		for (final Integer number : numbers)
		{
			// final int smallerNumbers = filter(numbers,
			// lessThan(number)).size();
			final int smallerNumbers = countSmaller(numbers, number);

			for (int position = 0; position < numbers.size(); position++)
			{
				final double expectedLengthAtPosition = expectedSuccessfulDraws(smallerNumbers, numbers.size() - 1, position, 1) + 1.0;

				if (isDebugEnabled())
				{
					System.out.println(String.format("%s -- %d at %d: %f", numbers, number, position, expectedLengthAtPosition));
				}

				expectedSequenceLengthSum += expectedLengthAtPosition / numbers.size();
			}
		}

		return expectedSequenceLengthSum;
	}

	double expectedSuccessfulDraws(final int successfulOptions, final int totalOptions, final int maxDraws, final int draws)
	{
		if (successfulOptions == 0)
		{
			return 0;
		}

		if (draws > maxDraws)
		{
			return 0;
		}

		final double p = (double) successfulOptions / totalOptions;

		return p * (1 + expectedSuccessfulDraws(successfulOptions - 1, totalOptions - 1, maxDraws, draws + 1));
	}

	private int countSmaller(final List<Integer> numbers, final Integer testNumber)
	{
		int count = 0;

		for (final Integer number : numbers)
		{
			if(number < testNumber)
			{
				count++;
			}
		}

		return count;
	}
}
