/* Enter your code here. Read input from STDIN. Print output to STDOUT */import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;
import java.util.StringTokenizer;

public class Solution implements Runnable {

    // leave empty to read from stdin/stdout
    private static final String TASK_NAME_FOR_IO = "";

    // file names
    private static final String FILE_IN = TASK_NAME_FOR_IO + ".in";
    private static final String FILE_OUT = TASK_NAME_FOR_IO + ".out";

    BufferedReader in;
    PrintWriter out;
    StringTokenizer tokenizer = new StringTokenizer("");

    public static void main(String[] args) {
        new Solution().run();
    }

    int n;
    int[] a;

    private void solve() throws IOException {
        precalc();

        // timing();
        // stress();

        int tc = nextInt();
        for (int tcIdx = 0; tcIdx < tc; tcIdx++) {
            n = nextInt();
            a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = nextInt();
            }

            out.printf("%.2f", solveFast());
            out.println();
        }
    }

    private void timing() {
        Random r = new Random(123456789L);
        for (int tc = 0; tc < 100; tc++) {
            n = 50;
            a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = r.nextInt(15);
            }
            out.printf("%.2f", solveFast());
            out.println();
        }
    }

    private void stress() {
        Random r = new Random(123456789L);
        int tcIdx = 0;
        for (;;) {
            n = r.nextInt(11);
            a = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = r.nextInt(n);
            }

            double x = solveNaive();
            double y = solveFast();
            if (Math.abs(x - y) > 1e-8) {
                throw new IllegalStateException("Difference is: " + Math.abs(x - y));
            }
            System.out.println(tcIdx++);
        }
    }

    int MAXN = 50;

    BigInteger[] F = new BigInteger[MAXN + 1];
    BigInteger[][] C = new BigInteger[MAXN + 1][MAXN + 1];
    BigInteger[][] P = new BigInteger[MAXN + 1][MAXN + 1];
    double[][][] PP = new double[MAXN + 1][MAXN + 1][MAXN + 1];

    private void precalc() {
        F[0] = BigInteger.ONE;
        for (int i = 1; i <= MAXN; i++) {
            F[i] = F[i - 1].multiply(BigInteger.valueOf(i));
        }

        for (int i = 0; i <= MAXN; i++) {
            C[i][0] = BigInteger.ONE;
            C[i][i] = BigInteger.ONE;
            for (int k = 1; k < i; k++) {
                C[i][k] = C[i - 1][k - 1].add(C[i - 1][k]);
            }
            for (int k = i + 1; k <= MAXN; k++) {
                C[i][k] = BigInteger.ZERO;
            }
        }

        for (int i = 0; i <= MAXN; i++)
            for (int k = 0; k <= MAXN; k++) {
                P[i][k] = C[i][k].multiply(F[k]);
            }

        for (int i = 0; i <= MAXN; i++)
            for (int j = 0; j <= MAXN; j++)
                for (int k = 0; k <= MAXN; k++) {
                    PP[i][j][k] = new BigDecimal(P[i][j].multiply(F[k])).doubleValue();
                }
    }

    double[][][] PPD = new double[MAXN + 1][MAXN + 1][MAXN + 1];

    private void precalcForN() {
        double dvd = new BigDecimal(F[n]).doubleValue();
        for (int i = 0; i <= MAXN; i++)
            for (int j = 0; j <= MAXN; j++)
                for (int k = 0; k <= MAXN; k++) {
                    PPD[i][j][k] = PP[i][j][k] / dvd;
                }
    }

    private double solveFast() {
        precalcForN();

        int[] cntSmaller = new int[n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                if (i != j && a[j] < a[i]) {
                    cntSmaller[i]++;
                }

        double result = 0;
        for (int i = 0; i < n; i++)
            for (int iPos = 0; iPos < n; iPos++) {

                // put larger element in front
                for (int j = 0; j < n; j++)
                    if (i != j && a[j] >= a[i]) {
                        for (int jPos = 0; jPos < iPos; jPos++) {
                            // how many permutations are there, such that:
                            //  JPOS                 IPOS
                            // a[j] ................ a[i]
                            //      all numbers < a[i]
                            //
                            // count: C(num_smaller_than_a[i], IPOS - JPOS - 1) * F(IPOS - JPOS - 1) * F(N - (IPOS - JPOS + 1))
                            // multiplier: IPOS - JPOS
                            result += PPD[cntSmaller[i]][iPos - jPos - 1][n - (iPos - jPos + 1)] * (iPos - jPos);
                        }
                    }

                // do not put larger element in front
                {
                    // how many permutations are there, such that:
                    //                        IPOS
                    // ...................... a[i]
                    //   all numbers < a[i]
                    //
                    // count: C(num_smaller_than_a[i], IPOS) * F(iPos) * F(N - (IPOS + 1))
                    // multiplier: IPOS + 1
                    result += PPD[cntSmaller[i]][iPos][n - (iPos + 1)] * (iPos + 1);
                }

            }

        return result;
    }

    int[] used, perm;

    private double solveNaive() {
        perm = new int[n];
        used = new int[n];
        double result = rec(0);
        for (int i = 2; i <= n; i++) {
            result /= i;
        }
        return result;
    }

    private long rec(int pos) {
        if (pos >= n) {
            long result = 0;
            for (int i = 0; i < n; i++) {
                int jMax = -1;
                for (int j = i - 1; j >= 0; j--)
                    if (a[perm[j]] >= a[perm[i]]) {
                        jMax = j;
                        break;
                    }
                result += (i - jMax);
            }
            return result;
        }

        long result = 0;
        for (int i = 0; i < n; i++)
            if (used[i] == 0) {
                used[i] = 1;
                perm[pos] = i;
                result += rec(pos + 1);
                used[i] = 0;
            }
        return result;
    }

    public void run() {
        long timeStart = System.currentTimeMillis();

        boolean fileIO = TASK_NAME_FOR_IO.length() > 0;
        try {

            if (fileIO) {
                in = new BufferedReader(new FileReader(FILE_IN));
                out = new PrintWriter(new FileWriter(FILE_OUT));
            } else {
                in = new BufferedReader(new InputStreamReader(System.in));
                out = new PrintWriter(new OutputStreamWriter(System.out));
            }

            solve();

            in.close();
            out.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        long timeEnd = System.currentTimeMillis();

        if (fileIO) {
            System.out.println("Time spent: " + (timeEnd - timeStart) + " ms");
        }
    }

    private String nextToken() throws IOException {
        while (!tokenizer.hasMoreTokens()) {
            String line = in.readLine();
            if (line == null) {
                return null;
            }
            tokenizer = new StringTokenizer(line);
        }
        return tokenizer.nextToken();
    }

    private int nextInt() throws IOException {
        return Integer.parseInt(nextToken());
    }

}
