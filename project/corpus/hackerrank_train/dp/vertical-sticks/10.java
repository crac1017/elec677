import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Scanner;

public class Solution {
	static final int maxn = 55;
	
	static int n;
	static int []v = new int[maxn];
	static BigInteger []f = new BigInteger[100];
	static int []cnt = new int[1005];
	
	static BigInteger [][]save = new BigInteger[55][55];
	
	public static BigInteger c( int n, int k ){
		if (k > n) return new BigInteger("0");
		else if( n == 0 || k == 0 ) return new BigInteger("1");
		else if( save[n][k] != null ) return save[n][k];
		else{
			save[n][k] = new BigInteger( c(n-1,k-1).toString());
			save[n][k] = save[n][k].add( c(n-1,k) );
			return save[n][k];
		}
	}
	
	public static void main(String[] args) throws IOException {
		f[0] = f[1] = new BigInteger(1+"");
		for (int i = 1; i < 100; ++i)
			f[i] = f[i-1].multiply( new BigInteger(i+""));
		
		Scanner scan = new Scanner(System.in);
		
		int tests = scan.nextInt();
		while( (tests--)>0 ){ 
			for (int i = 0; i <= 1000; ++i)
				cnt[i] = 0;
			
			n = scan.nextInt();
			for (int i = 0; i < n; ++i){
				v[i] = scan.nextInt();
				cnt[ v[i] ] ++;
			}
			java.util.Arrays.sort(v,0,n);
			
			BigInteger sol = new BigInteger( "0" );
			
			for (int i = 0; i < n; ++i) {
				for (int j = i+1; j < n; ++j) {
					int d = j - i;
					int less = 0;
					for (int k = 0; k <= 1000; ++k) {
						if (cnt[k] == 0) continue;
						int out = i + (n-j-1);
						
						BigInteger curr =  c(less,j-i-1).multiply( BigInteger.valueOf(n-less-1));
						curr = curr.multiply(f[j-i-1]);
						curr = curr.multiply(f[out]);
						curr = curr.multiply( BigInteger.valueOf(cnt[k]*d));
						
						sol = sol.add(curr);
						less += cnt[k];
					}
				}
			}
			
			for (int i = 1; i <= n; ++i) {
				int d = i, less = 0;
				for (int k = 0; k <= 1000; ++k) {
					if (cnt[k] == 0) continue;
					int out = n - i;
					
					BigInteger curr = c(less,i-1);
					curr = curr.multiply(f[i-1]);
					curr = curr.multiply(f[out]);
					curr = curr.multiply( BigInteger.valueOf(d*cnt[k]));
					sol = sol.add(curr);
					less += cnt[k];
				}
			}
			
			BigDecimal fin = new BigDecimal( sol );
			fin = fin.divide( new BigDecimal(f[n]), 2,RoundingMode.HALF_UP);
			System.out.println(fin);
		}
	}
}
