
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.util.StringTokenizer;
//

public class Solution {

    public static void main(String[] args) throws Exception {
        (new Solution()).run();
    }
    
    int getSmaller(int idx, int[] arr) {
      int i;
      for (i = idx-1; i >=0; i--) {
        if (arr[i] < arr[idx]) break;
      }
      
      return i+1;
    }
    public void run() throws Exception {
        Scanner sc = new Scanner(System.in);
        
        int T = sc.nextInt();
        
        for (int cs = 0; cs < T; ++cs) {
          int N = sc.nextInt();
          int[] arr = new int[N];
          double[] res = new double[N]; 
          
          for (int i = 0; i < N; i++) {
            arr[i] = sc.nextInt();
          }
          
          Arrays.sort(arr);
          
          double ret = 0;
          for (int pos = 0; pos < N; ++pos) {
            
            for (int i = 0; i < N; ++i) { // if I put i-th element in this position
              double score = 1;
              double probability = 1;
              int total = N-1;
              int smaller = getSmaller(i, arr);
              int largerOrEq = total - smaller;
              
              if (pos > 0) {
                score *= largerOrEq * 1.0 / total;
              }
              for (int j = 1; j<=pos; j++) { // I would score j
                if (smaller > 0) {
                  probability *= smaller;
                  probability /= total;
                  
                  smaller--; total--;
                  
                  
                  double curProbability = probability;
                  if (j < pos) {
                    curProbability *= largerOrEq *1.0 / total;
                  }
                  
                  score += curProbability * (j+1);
                }
                else break;
              }
              
              ret += score;
//              System.out.println(">> " + pos + " " + i + " " + ret + " " + score);
            }
            
          }
          
          ret /= N;
          System.out.format("%.2f\n", ret);
        }
    }
}
