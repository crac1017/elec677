import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T,N;
        int maxContiguousSum ;
        int maxNonContiguousSum ;
        T = in.nextInt();
        while(T-- >0){
            maxContiguousSum = 0;
            maxNonContiguousSum = 0;
            N=in.nextInt();
            int arr[] = new int[N];
            int maxArr[] = new int[N+1];
            int k=0;
            for(int i=0;i<N;i++){
                arr[i] = in.nextInt();
                maxArr[i]=0;
            }
            maxArr[N]=0;
            for(int i=0;i<N;i++){
                if(arr[i] > 0) {
                    maxNonContiguousSum += arr[i];
                } else {
                    maxArr[k++] = maxContiguousSum;
                }
                if(maxContiguousSum + arr[i] > 0){
                    maxContiguousSum += arr[i];
                } else {
                    maxContiguousSum = 0;
                }
            }
            maxArr[k]=maxContiguousSum;
            maxContiguousSum = maxArr[0];
            for(int j=0;j<=k;j++){
                if(maxArr[j] > maxContiguousSum)
                    maxContiguousSum = maxArr[j];
            }
            if(maxNonContiguousSum == 0){
                maxNonContiguousSum = arr[0]; maxContiguousSum = arr[0];
                for(int i=1;i<N;i++){
                    if(maxNonContiguousSum < arr[i]){
                        maxNonContiguousSum = arr[i];maxContiguousSum = arr[i];
                    }
                        
                    
                }
            }
            System.out.println(maxContiguousSum + " "+ maxNonContiguousSum);
        }
    }
}
