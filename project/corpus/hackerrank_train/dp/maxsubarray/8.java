import java.util.Scanner;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.io.PrintWriter;
import java.io.InputStream;

public class Solution {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        Scanner in = new Scanner(inputStream);
		PrintWriter out = new PrintWriter(outputStream);
        int t = in.nextInt();
        while(t != 0) {
            int n = in.nextInt();
            int A[] = new int[100005];
            int flag = 0;
            for(int i = 0; i < n; i++) {
                A[i] = in.nextInt();
                if(A[i] >= 0) flag = 1;
            }
            int a = A[0], b = A[0], c = 0, ans = -100005;
            for(int i = 1; i < n; i++) {
                a = Math.max(A[i], a + A[i]);
                b = Math.max(a, b);
            }
            out.printf("%d ",b);
            if(flag == 1) {
                for(int i = 0; i < n; i++) if(A[i] >= 0) c += A[i];
                out.printf("%d\n",c);
            } else {
                for(int i = 0; i < n; i++) ans = Math.max(ans, A[i]);
                out.printf("%d\n",ans);
            }
            t--;
        }
        out.close();
    }
}
