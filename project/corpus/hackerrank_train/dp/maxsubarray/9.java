import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        for (int t = 0; t < T; t++){
            int N = sc.nextInt();            
            
            int maxSum = 0;
            int current_sum = 0;
            int posetiveSum = 0;            
            int max = Integer.MIN_VALUE;
            boolean hasPos = false;
            
            for (int i = 0; i < N ; i++){ 
            
                int n =  sc.nextInt();             
                if (n > max) max = n;
                
                int val = current_sum + n;
                
                if (n > 0) {
                    posetiveSum += n;
                    hasPos = true;
                }
                
                if (val > 0)                
                    current_sum = val;
                else
                    current_sum = 0;    
                
                if (current_sum > maxSum) maxSum = current_sum;
            }
            
            if (!hasPos){ 
                maxSum = max;
                posetiveSum = max;
            }
            
            System.out.println(maxSum + " " + posetiveSum);
        }        
    }
}
