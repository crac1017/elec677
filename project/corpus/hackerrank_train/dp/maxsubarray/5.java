import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {


    static int maxContSubArray(String[] array) {
        int currentMax = Integer.MIN_VALUE;
        int currentSum = 0;
        for (int i = 0; i < array.length; i++) {
            int elem = Integer.parseInt(array[i]);
            currentSum = Math.max(currentSum + elem, elem);
            currentMax = Math.max(currentSum, currentMax);
        }
        return currentMax;
    }

    static int maxSubArray(String[] array) {
        int currentSum = Integer.parseInt(array[0]);
        for (int i = 1; i < array.length; i++) {
            int elem = Integer.parseInt(array[i]);
            currentSum = Math.max(currentSum, currentSum + elem);
            currentSum = Math.max(currentSum, elem);
        }

        return currentSum;
    }

   
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
 
        String input;
 
        int counter = 0;
        while(in.hasNextLine()){
            input = in.nextLine();
            if (counter % 2 == 0 && counter != 0) {
                String[] array = input.split(" ");
                System.out.println(maxContSubArray(array) + " " + maxSubArray(array));   
            }
            counter++;
        }
    }
}
