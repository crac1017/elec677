import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T;
        int N;
        T = in.nextInt();
        for(int i = 0; i < T; i++){
            N = in.nextInt();
            int[] arr = new int[N];
            for(int j = 0; j < N; j++){
                arr[j] = in.nextInt();
            }
            int max_sum = arr[0];
            int sum = arr[0];
            for(int j = 1; j < N; j++){
                if(sum < 0){
                    sum = arr[j];
                }
                else{
                    sum += arr[j];
                }
                if(sum > max_sum){
                    max_sum = sum;
                }
            }
            System.out.print(max_sum);
            sum = 0;
            for(int j = 0; j < N; j++){
                if(arr[j] > 0){
                    sum += arr[j];
                }
            }
            if(sum == 0){
                sum = arr[0];
                for(int j = 0; j < N; j++){
                    if(arr[j] > sum)
                        sum = arr[j];
                }
            }
            System.out.print(" ");
            System.out.println(sum);
        }
    }
}
