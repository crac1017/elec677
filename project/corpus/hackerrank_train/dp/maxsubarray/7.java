import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws Exception {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.valueOf(r.readLine());
        for(int  i=0;i<t;++i){
            r.readLine();
            int[] arr=convert(r.readLine());
            maxSubArray(arr);
        }
    }
    
    public static void maxSubArray(int[] a){
        int maxSum = a[0];
        int startIndex = 0;
        int maxSoFar = a[0];
        int maxnc=0;
        int max = a[0];
        if(a[0]>0)
            maxnc=a[0];
        for(int i=1;i<a.length;++i){
            if(max < a[i]){
                max = a[i];
            }
            maxSoFar += a[i];
            if(a[i]>0)
                maxnc+=a[i];
            if(maxSoFar < 0){
                maxSoFar = 0;
            }
            if(maxSoFar>maxSum){
                maxSum = maxSoFar;
            }
        }
        if(maxSum ==0)
            maxSum = max;
        if(maxnc==0){
            maxnc = max;
        }
        System.out.println(maxSum+" "+maxnc);
    }
    
    public static int[] convert(String l){
        String[] a=l.split(" ");
        int[] ar=new int[a.length];
        for(int i=0;i<a.length;++i){
            ar[i]=Integer.valueOf(a[i]);
        }
        return ar;
    }
}
