import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int num_testcases = in.nextInt();
        for (int i =0; i < num_testcases; i++) {
            int size = in.nextInt();
            int a[] = new int[size];
            for (int j =0; j < size; j++)
                a[j] = in.nextInt();
            System.out.print(maxsubarray(a)+" ");
            System.out.println(maxsubarray_nc(a));
        }
    }

    public static int maxsubarray(int[] a) {
        int maxsum_ending = a[0];
        int maxsum = maxsum_ending;
        for (int i = 1; i < a.length; i++) {
        	if (maxsum_ending < 0 && a[i] < 0){
        		maxsum_ending = a[i];
        	}else if (a[i] < (a[i] + maxsum_ending)) {
                maxsum_ending = a[i] + maxsum_ending;
            } else {
                maxsum_ending = a[i];
            }
        	if (maxsum_ending > maxsum)
        		maxsum = maxsum_ending;
        }
        return maxsum;
    }

    public static int maxsubarray_nc(int[] a) {
        int maxsum = a[0];
        for (int i = 1; i < a.length; i++) {
            /* if maxsum is negative */
            if (maxsum < 0) {
                if (a[i] > maxsum)
                    maxsum = a[i];
            }
            else{
                if (a[i] > 0)
                    maxsum = maxsum + a[i];
            }
        }
        return maxsum;
    }
}
