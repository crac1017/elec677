import java.util.Scanner;

public class MaximumSubarray {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		
		while (T-- > 0) {
			int N = in.nextInt();
			
			int[] input = new int[N];
			int answer2 = 0;
			boolean empty = true;
			int maxValue = Integer.MIN_VALUE;
			for (int i = 0; i < N; i++) {
				input[i] = in.nextInt();
				maxValue = Math.max(maxValue, input[i]);
				if (input[i] > 0) {
					empty = false;
					answer2 += input[i];
				}
			}			
			
			if (empty) {
				System.out.println(maxValue + " " + maxValue);
				continue;
			}
			
			int currentSeq = 0;	
			int bestSeq = 0;			
			for (int i = 0; i < N; i++) {
				currentSeq += input[i];				
				if (currentSeq < 0) {					
					currentSeq = 0;
				}
				if (currentSeq > bestSeq) 
					bestSeq = currentSeq;	
			}		
			
			
			System.out.println(bestSeq + " " + answer2);
		}
		in.close();		
	}
	
}
