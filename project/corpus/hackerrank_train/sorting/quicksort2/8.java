

/* Head ends here */
import java.util.*;
public class Solution {
       
    static void quickSort(int[] ar) {                                          
        sort(ar, ar.length);
    } 
    
    static void sort(int[] ar, int length){          
        if(length == 2){
            if(ar[0] > ar[1]){
                int temp = ar[0];
                ar[0] = ar[1];
                ar[1] = temp;
            }
        }else{
           int[] greater = new int[length];
           int[] smaller = new int[length];
           
           int value  = ar[0];
           
           int gIndex = 0; 
           int sIndex = 0;
           
           for(int i = 1 ; i < length; i++){
               
               if(value > ar[i]){
                   smaller[sIndex] = ar[i];
                   sIndex++;
               }else{
                   greater[gIndex] = ar[i];
                   gIndex++;
               }    
           }
           
          
           
           if(sIndex > 1 ){
               sort(smaller, sIndex);
           }    
            
            
           if(gIndex > 1){
               sort(greater, gIndex);
           }
           
           int index = 0;
           for(int i  = 0 ; i < sIndex; i++){
               ar[index] = smaller[i];
               index++;
           } 
             
           ar[index] = value;
           index++;
           
           for(int i  = 0 ; i < gIndex; i++){
               ar[index] = greater[i];
               index++;
           }    
        }
           print(ar, length);
    }
    
    
    static void print(int[] ar, int length){
        String s = "";
        for(int i = 0 ; i < length; i++){
            s += ar[i]+" ";
        }  
        System.out.println(s);
    }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           quickSort(ar);
       }    
   }
