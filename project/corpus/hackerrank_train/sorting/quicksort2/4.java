

/* Head ends here */
import java.util.*;
public class Solution {
	    
    static ArrayList<Integer> sortArray( ArrayList<Integer> ar) {
        
        if( ar.size() <= 1 ) {
            return ar;
        } else {
            // choose a pivot element
            int v = ar.get(0);
            
            ArrayList<Integer> arLeft = new ArrayList<Integer>();
            ArrayList<Integer> arRight = new ArrayList<Integer>();
        
            for( int i = 1; i < ar.size(); i++ ) {
                if( ar.get(i) <= v ) {
                    //System.out.println("Adding element " + ar.get(i) + " to the left");
                    arLeft.add(ar.get(i));
                } else {
                    //System.out.println("Adding element " + ar.get(i) + " to the right");
                    arRight.add(ar.get(i));
                }
            }
            arLeft = sortArray(arLeft);
            arRight = sortArray(arRight);
            
            // merging arLeft and arRight in returnArrayList
            ArrayList<Integer> returnArrayList = new ArrayList<Integer>();
            returnArrayList.addAll( arLeft );
            returnArrayList.add( v );
            returnArrayList.addAll( arRight );
            
            // print array
            int j = 0;
            String returnArrayString = "";
            for (Integer h : returnArrayList) {
                returnArrayString += h;
                j++;
            }
            System.out.println(returnArrayString);
                
            return returnArrayList; 
        }
        
    }   
    
    
    static void quickSort(int[] ar) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        // converting ar to arrayList
        for( int i = 0; i < ar.length; i++ ) {
            arrayList.add( ar[i] );
        }
	    sortArray( arrayList );  		    	
	}	

/* Tail starts here */
 
 static void printArray(int[] ar) {
			for(int n: ar){
				System.out.print(n+" ");
			}
	        System.out.println("");
		}
	    
	   public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int[] ar = new int[n];
	        for(int i=0;i<n;i++){
	           ar[i]=in.nextInt(); 
	        }
	        quickSort(ar);
	    }    
	}
