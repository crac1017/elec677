

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void quickSort(int[] ar) {
            ArrayList<Integer> list = new ArrayList<Integer>();
            for (int num : ar) {
                list.add(num);
            }
            partition(list);
          }
    static void printList(ArrayList<Integer> list) {
        for(int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i));
        }
        System.out.print("\n");
    }
    static ArrayList<Integer> partition(ArrayList<Integer> ar) {
              if(ar.size() <= 1) return ar;
              ArrayList<Integer> left = new ArrayList<Integer>();
              ArrayList<Integer> right = new ArrayList<Integer>();
              int p = ar.get(0);
              for(int i = 0; i < ar.size(); i++) {
                  if(ar.get(i) > p) {
                      right.add(ar.get(i));
                  } else if(ar.get(i) < p) {
                      left.add(ar.get(i));
                  }
              }
              left = partition(left);
              right = partition(right);
              ArrayList<Integer> merged = new ArrayList<Integer>();
              merged.addAll(left);
              merged.add(p);
              merged.addAll(right);
              printList(merged);
              return merged;
    }            

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           quickSort(ar);
       }    
   }
