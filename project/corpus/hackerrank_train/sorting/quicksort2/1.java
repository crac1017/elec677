

import java.util.Arrays;
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Manoj
 */
public class Solution {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar,0,ar.length);
           
    }
    static void printArray(int[] ar,int s, int t) {
         for(int n=s;n<t;n++){
            System.out.print(ar[n]+" ");
         }
           System.out.println("");
    }
    static void partition(int[] ar,int start,int stop) {
        
        if((stop-start)<=1)
            return;
        int p=ar[start];
        
        int[] copy=Arrays.copyOf(ar, ar.length);
        int c=start;
        for(int i=start+1;i<stop;i++){
            if(copy[i]<=p){
                ar[c]=copy[i];
                c++;
               
            }
        }
        
        
        ar[c]=p;
        c++;  

        int tmp=c;
        
        for(int j=start+1;j<stop;j++){
            if(copy[j]>p){
                ar[c]=copy[j];
                c++;
            }
        }
         
        
        partition(ar,start,tmp-1);
        partition(ar,tmp,stop);
        printArray(ar,start,stop);
        
       
        
    }   
}
