/* Head ends here */
import java.util.*;

public class Solution {

	static void quickSort(int[] ar) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		for (int number : ar) {
			arrayList.add(number);
		}
		quickSort(arrayList);
	}

	static ArrayList<Integer> quickSort(ArrayList<Integer> arrayList) {
		if (arrayList.size() <= 1) {
			return arrayList;
		}
		ArrayList<Integer> less = new ArrayList<Integer>();
		ArrayList<Integer> greater = new ArrayList<Integer>();
		for (int index = 1; index < arrayList.size(); index++) {
			if (arrayList.get(index) < arrayList.get(0)) {
				less.add(arrayList.get(index));
			} else {
				greater.add(arrayList.get(index));
			}
		}
		ArrayList<Integer> arrayListTwo = quickSort(less);
		arrayListTwo.add(arrayList.get(0));
		arrayListTwo.addAll(quickSort(greater));
		printArray(arrayListTwo);
		return arrayListTwo;
	}

	/* Tail starts here */

	static void printArray(ArrayList<Integer> ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int i = 0; i < n; i++) {
			ar[i] = in.nextInt();
		}
		quickSort(ar);
	}
}
