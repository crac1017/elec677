

/* Head ends here */
import java.util.*;
public class Solution {
       
    static void quickSort(int[] ar) {
        if (ar == null || ar.length == 0) {
            return;
        }
        if (ar.length == 1) {
            System.out.println(ar[0]);
        } else {
            quickSort(ar, ar.length);
        }
    }   

    private static int[] quickSort(int[] ar, int size) {
        if (size < 2) return ar;
        
        int p = ar[0];
        
        int ls = 0;
        int rs = 0;
        int[] l = new int[size];
        int[] r = new int[size];
        
        for (int i = 1; i < size; i++) {
            if (ar[i] < p) l[ls++] = ar[i];
            else r[rs++] = ar[i];
        }
        
        l = quickSort(l, ls);
        r = quickSort(r, rs);
        
        int j = 0;
        for (int i = 0; i < ls; i++) {
            System.out.print(l[i] + " ");
            ar[j++] = l[i];
        }
        System.out.print(p + " ");
        ar[j++] = p;
        for (int i = 0; i < rs; i++) {
            System.out.print(r[i] + " ");
            ar[j++] = r[i];
        }
        System.out.println();
        
        return ar;
    }   

    
    /* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           quickSort(ar);
       }    
   }
