import java.io.*;
import java.util.*;

public class Solution {
    
    public static void insertionSort(int[] A){
      for(int j = 1; j < A.length; j++){
              int sorting = A[j];
              for(int i = j; i >= 0; i--){
                      if(i == 0 || A[i-1] <= sorting){
                          A[i] = sorting;
                          i = -1;
                      }else{
                          A[i] = A[i-1];
                      }
              }
                  
    }
        printArray(A);
    }

    
    static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
      }
/* Tail starts here */
public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
