

/* Head ends here */
import java.util.*;
public class Solution {
       
static void partition(int[] ar) {
    if(ar.length<2) return;
    int [] lt = new int[ar.length];
    int [] gt = new int[ar.length];
    int [] eq = new int[ar.length];
    int i=0,j=0,k=0;
    for(int r:ar)
    {
        if(r<ar[0])
            lt[i++]=r;
        if(r>ar[0])
            gt[j++]=r;
        if(r==ar[0])
            eq[k++]=r;
    }
    printArray(lt,i);
    printArray(eq,k);
    printArray(gt,j);
}

/* Tail starts here */
 
 static void printArray(int[] ar,int len) {
         for(int n=0;n<len;n++){
            System.out.print(ar[n]+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
