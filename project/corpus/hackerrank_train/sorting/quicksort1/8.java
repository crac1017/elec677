

/* Head ends here */
import java.util.*;
public class Solution {
    static void partition(int[] ar) {
        int len = ar.length;
        int p = ar[0];
        int len1 = 0, len2 = 0;
        int[] ar1 = new int[len];
        int[] ar2 = new int[len];
        for(int i=1; i<len; i++){
            if(ar[i]<=p){
                ar1[len1++] = ar[i];
            } else{
                ar2[len2++] = ar[i];
            }
        }
        
        int i;
        
        for(i=0; i<len1; i++){
            ar[i] = ar1[i];
        }
        
        ar[i] = p;
        
        for(i=0; i<len2; i++){
            ar[len1+1+i] = ar2[i];
        }
        
        printArray(ar);
    }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
