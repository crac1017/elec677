

/* Head ends here */
import java.util.*;
public class Solution {
       
    static void partition(int[] ar) {
        int pivot = ar[0];
        List<Integer> first = new LinkedList<Integer>();
        List<Integer> second = new LinkedList<Integer>();
        for(int i = 1; i < ar.length; i ++) {
            if(ar[i] < pivot) {
                first.add(ar[i]);
            } else {
                second.add(ar[i]);
            }
        }
        first.add(pivot);
        first.addAll(second);
        printArray(first.toArray(new Integer[0]));
        
    }   

/* Tail starts here */
 
 static void printArray(Integer[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
