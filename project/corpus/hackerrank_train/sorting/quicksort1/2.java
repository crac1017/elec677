

/* Head ends here */
import java.util.*;
public class Solution {
       
    static void partition(int[] ar) {
        int p=ar[0];
        ArrayList<Integer> leftList=new ArrayList<Integer>();
        ArrayList<Integer> rightList=new ArrayList<Integer>();
        for(int i=1;i<ar.length;i++){
            if(ar[i]>p){
                rightList.add(ar[i]);
            }
            else{
                leftList.add(ar[i]);
            }
        }
        copy(leftList,ar,0);
        ar[leftList.size()]=p;
        copy(rightList,ar,leftList.size()+1);
        printArray(ar);
    }   
    static void copy(ArrayList<Integer> list,int[] ar,int startIdx){
        for(int num:list){
            ar[startIdx++]=num;
        }
    }

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
