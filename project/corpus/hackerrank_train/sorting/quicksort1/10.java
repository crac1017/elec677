

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void partition(int[] ar) {
              
              int comp_elem=ar[0];
                  
              for(int i=1;i<ar.length;i++)
              {
                  /* Shifting elemnts small than the compare element move left */
                  /* Right Elements get automatically adjusted, no need to check them */
                  if(ar[i]<comp_elem)
                  {
                      int j;
                      int small_elem=ar[i];
                      for(j=i-1;j>=0;j--)
                      {
                          if(ar[j]>=comp_elem)
                          {
                              ar[j+1]=ar[j];
                          }
                          else
                          {
                              ar[j+1]=small_elem;
                              break;
                          }
                      }
                      
                      /* In case the element found is the smallest element of the list */
                      if(j<0)
                      {
                          ar[0]=small_elem;
                      }
                  }
              }
              
              printArray(ar);
                    
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
