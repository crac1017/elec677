

/* Head ends here */
import java.util.*;
public class Solution {
       
	static void partition(int[] ar) {
        int pivot = ar[0];
        int[] left = new int[ar.length-1];
        int[] right = new int[ar.length-1];
        int indexL = 0;
        int indexR = 0;
        for(int i=1; i<ar.length; i++){
            if(ar[i] < pivot)
                left[indexL++] = ar[i];
            else
                right[indexR++] = ar[i];
        }
        for(int i=0; i<ar.length; i++){
            if(i < indexL)
                ar[i] = left[i];
            else if(i == indexL)
                ar[i] = pivot;
            else
                ar[i] = right[i - indexL - 1];
        }
        printArray(ar);
    }

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
