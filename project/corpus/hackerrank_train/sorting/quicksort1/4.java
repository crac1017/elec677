

/* Head ends here */
import java.util.*;
public class Solution {
       
    static void partition(int[] ar) {
        int v = ar[0];
        ArrayList<Integer> arLeft = new ArrayList<Integer>();
        ArrayList<Integer> arRight = new ArrayList<Integer>();
        
        for( int i = 1; i < ar.length; i++ ) {
            if( ar[i] <= v ) {
            //    System.out.println("Adding element " + ar[i] + " to the left");
                arLeft.add(ar[i]);
            } else {
            //    System.out.println("Adding element " + ar[i] + " to the right");
                arRight.add(ar[i]);
            }
        }
        
        for (Integer h : arLeft)
        {
            System.out.print( h );
        }
        
        System.out.print( v );
        
        for (Integer h : arRight)
        {
            System.out.print( h );
        }
                    
    }   
    
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
