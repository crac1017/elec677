

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void partition(int[] ar) {
              
                      if(ar==null || ar.length == 1) {
            return;
        }
        
        ArrayList<Integer> L1 = new ArrayList<Integer>();
        ArrayList<Integer> L2 = new ArrayList<Integer>();
        
        int key = ar[0];
        
        for(int i = 1;i<ar.length;i++) {
            if(ar[i]<=key) {
                L1.add(ar[i]);
            }
            else {
                L2.add(ar[i]);
            }
        }
        
        int j = -1;
        for(int x : L1) {
            ar[++j] = x;
        }
        
        ar[++j] = key;
        
        for(int x : L2) {
            ar[++j] = x;
        }
        
        printArray(ar);
                    
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
