

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void partition(int[] ar) {
                   int pivot = ar[0] ;
              int[] smaller = new int[ar.length];
              int[] larger = new int [ar.length];
              int leftIndex =0, rightIndex =0;
              for(int i=1;i<ar.length;i++){
                  if(ar[i]<=pivot){
                      smaller[leftIndex]=ar[i];
                      leftIndex++;
                  }else{
                         larger[rightIndex]=ar[i];
                      rightIndex++;
                  }
              }
              int i=0;
              while(i<leftIndex){
                  System.out.print(" "+smaller[i]);
                  i++;
              }
              System.out.print(" "+ pivot);
              i=0;
              while(i<rightIndex){
                  System.out.print(" "+larger[i]);
                  i++;
              }
              
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           partition(ar);
       }    
   }
