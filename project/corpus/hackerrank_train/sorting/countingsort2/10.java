import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int [] a = new int[size];
        int [] freq = new int[size];
        int [] final_a = new int[size];
        
        for(int i=0; i<size; i++)
            a[i] = s.nextInt();
        
        for(int i=0; i<size; i++)
            freq[i]=0;
        
        for(int i=0; i<size; i++)
            freq[a[i]]++;
        
        for(int i=1; i<size; i++)
            freq[i]+=freq[i-1];
        
        for(int m=size-1; m>=0; m--)
        {
            final_a[freq[a[m]]-1] = a[m];
			freq[a[m]]--;
        }
        
        for(int i=0; i<size; i++)
            System.out.print(final_a[i]+" ");
    }
}
