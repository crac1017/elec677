import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int N = s.nextInt();
        int[] c = new int[N];
        for (int i = 0; i < N; ++i) c[s.nextInt()]++;
        for (int i = 0; i < N; ++i) for (int j = 0; j < c[i]; ++j) System.out.print("" + i + " ");
    }
}
