import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void count(int[] ar){        
        for (int i=0; i<100; i++){    
            for (int j=0; j<ar.length; j++){
                if(ar[j]==i){
                    System.out.print(ar[j]+" ");
                }
            }
        }
    }
    
    static void printArray(int[] ar) {
        for(int n: ar){
            System.out.print(n+" ");
        } 
        
        System.out.println("");
    }

    public static void main(String[] args) {
      
        
        Scanner in = new Scanner(System.in);
        
        int n = in.nextInt();
        int[] ar = new int[n];
        
        for(int i=0; i<n; i++){
            ar[i] = in.nextInt();
        }
        
        count(ar);
    }
}
