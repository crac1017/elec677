

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        String[] s = br.readLine().split(" ");
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = Integer.parseInt(s[i]);

        }
        int [] fq = new int[100];
        Arrays.fill(fq, 0);
        for (int i = 0; i < n; i++) {
            fq[a[i]]++;
        }

       
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < fq[i]; j++) {
                System.out.print(i + " ");
            }
        }


    }
}
