import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        String input=in.nextLine();
        int N=Integer.parseInt(input);
        
        int arr[]=new int[N];
        input=in.nextLine();
        String[] s=input.split(" ");
        for(int i=0;i<N;i++){
            arr[i]=Integer.parseInt(s[i]);       
        }
        int[] count=new int[100];
        for(int i=0;i<N;i++){
            count[arr[i]]++;
        }
        for(int i=0;i<100;i++){
            System.out.print(count[i]+" ");
        }
    }
}
