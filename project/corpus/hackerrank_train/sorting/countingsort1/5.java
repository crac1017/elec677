import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n=in.nextInt();
        List<Integer> list = new ArrayList<Integer>();
        
        for (int i=0;i<n;i++){
            list.add(in.nextInt());
        }        
        
        int occs[]=new int[100];
        
        for (int i=0;i<n;i++){        
            occs[list.get(i)]++;
        }
        
        for (int i=0;i<100;i++){           
            System.out.print(occs[i]+" ");
        }
            
    }
}
