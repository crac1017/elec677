import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int[] nums = new int[100];
        for(int i = 0; i < num; i++)
            nums[in.nextInt()] += 1;
        for(int i = 0; i < 99; i++)
            System.out.print(nums[i] + " ");
        System.out.println(nums[99]);
    }
}
