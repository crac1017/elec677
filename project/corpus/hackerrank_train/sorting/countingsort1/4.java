import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static void CountSort(int[] arr,int n)
    {
        int K=100;
        int[] C = new int[K];
        
        for(int i=0;i<K;++i)
            C[i]=0;
        
        for(int i=0;i<n;++i)
            C[arr[i]] = C[arr[i]]+1;
        
        for(int i=0;i<K;++i)
            System.out.print(C[i]+" ");
    }
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int[] arr = new int[N];
        
        for(int i=0;i<N;++i)
            arr[i] = sc.nextInt();
        
        CountSort(arr,N);
    }
}
