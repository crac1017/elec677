


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());

        int[] a = new int[n];
        String[] str = new String[n];
        for (int i = 0; i < n; i++) {
            String s = br.readLine();
            String [] p = s.split(" ");
            a[i] = Integer.parseInt(p[0]);
            str[i] = p[1];
        }
        int [] fq = new int[100];
        int [] countSmaller = new int[100];
        Arrays.fill(fq, 0);
        for (int i = 0; i < n; i++) {
            fq[a[i]]++;
        }
       
        countSmaller[0] = fq[0];
        for (int i = 1; i < countSmaller.length; i++) {
            countSmaller[i] = countSmaller[i-1] + fq[i];
        }

        for (int i = 0; i <= 99; i++) {
            System.out.print(countSmaller[i] + " ");
        }
//        for (int i = 0; i < 100; i++) {
//            for (int j = 0; j < fq[i]; j++) {
//                System.out.println(i + " ");
//            }
//        }


    }
}
