import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int [] a = new int[size];
        String [] b = new String[size];
        int [] freq = new int[100];
        
        for(int i=0; i<size; i++)
        {
            a[i] = s.nextInt();
            b[i] = s.nextLine();
        }
        
        for(int i=0; i<100; i++)
            freq[i]=0;
        
        for(int i=0; i<size; i++)
        {
            if(a[i]>=0 && a[i]<=99)
                freq[a[i]]++;
        }
        
        for(int i=1; i<100; i++)
            freq[i]+=freq[i-1];
        
        for(int i=0; i<100; i++)
            System.out.print(freq[i]+" ");
    }
}
