import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        try{
            BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
            int n=Integer.parseInt(buf.readLine());
            int[] count=new int[100];
            String[] str;
            for(int i=0;i<n;i++){
                str=buf.readLine().split(" ");
                count[Integer.parseInt(str[0])]++;
            }
            int sum=0;
            for(int i=0;i<100;i++){
                sum+=count[i];
                System.out.print(sum+" ");
            }            
        }catch(Exception e){}
    }
}
