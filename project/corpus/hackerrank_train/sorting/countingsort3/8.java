import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int N = s.nextInt();
        Map<Integer,List<String>> c = new HashMap<Integer,List<String>>();
        for (int i = 0; i < N; ++i) {
            int k = s.nextInt();
            if (!c.containsKey(k)) {
                c.put(k, new ArrayList<String>());
            }
            c.get(k).add(s.next());
        }
        int t = 0;
        for (int i = 0; i < 100; ++i) {
            if (c.containsKey(i)) {
                t += c.get(i).size();
            }
            System.out.print("" + t + " ");
        }
    }
}
