import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    	int n;
    	int key;
    	String value;
    	
    	int [] a = new int[100];
    	int [] b = new int[100];
    	
    	Scanner in = new Scanner(System.in);
    	n = in.nextInt();
    	while(in.hasNext())
    	{
    		key = in.nextInt();
    		value = in.next();
    		a[key]++;
    	}
    	
    	for(int i=0;i<100;i++)
    	{
    		if (i==0) b[i] = a[i];
    		else
    		{
    			b[i] = b[i-1] + a[i];
    		}
    	}
    	
    	for(int i=0;i<100;i++)
    	{
    		if (i!=99) System.out.print(b[i]+" ");
    		else System.out.println(b[i]);
    	}
    	in.close();
    }
}
