import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        String input=in.nextLine();
        int N=Integer.parseInt(input);
        
        int arr[]=new int[N];        
        String[] s;
        for(int i=0;i<N;i++){
            input=in.nextLine();
            s=input.split(" ");
            arr[i]=Integer.parseInt(s[0]);       
        }
        int[] count=new int[100];
        for(int i=0;i<N;i++){
            count[arr[i]]++;
        }
        for(int i=1;i<100;i++){
            count[i]+=count[i-1];
        }
        for(int i=0;i<100;i++){            
                System.out.print(count[i]+" ");
        }
    }
}
