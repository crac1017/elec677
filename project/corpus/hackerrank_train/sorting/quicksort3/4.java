
import java.util.*;
public class Solution {
       
    static void quickSort(int[] a) {
        sort(a, 0, a.length - 1);
    }
    
    static void sort( int[]a, int lo, int hi){
        if (hi <= lo) return;
        int j = partition(a, lo, hi);
        printArray( a );
        sort(a, lo, j-1);
        sort(a, j+1, hi);
    }
    
    private static int partition(int[] a, int lo, int hi) {
        int i = lo;
        int j = lo-1;
        int v = a[hi];  
        for ( ; i < hi ; i++ ) {    
            if( a[i] < v ) {
                swap(a, i, ++j);
            }
        }
        
        swap( a, hi, j+1);
        return j+1;
    }
    
    private static void swap( int[]a, int lo, int hi){
        if( lo==hi ) return;
        int sw = a[ lo ];
        a[ lo ] = a[ hi ];
        a[ hi ] = sw;
    }
 
      static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
    
      static void printArray(int[] ar, int lo, int hi) {
         for(int j = lo ; j <=hi; j++){
            System.out.print(ar[j]+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           quickSort(ar);
       }    
   }
