import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static int partition(int[] array, int left, int right) {
        int pivot = array[right];
        int j = -1;
        for (int i = left; i <= right; i++) {
            if (array[i] <= pivot && j != -1) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                j++;
            } else if (array[i] > pivot && j == -1) {
                j = i;
            }
        }
        if (j == -1) {
            return right;
        } else {
            return j - 1;
        }
        
        
    }
    public static void quickSort(int[] array, int left, int right) {
        int index = partition(array, left, right);
        printArray(array);
        if (index - 1 > left) {
            quickSort(array, left, index-1);
        }
        if (index + 1 < right) {
            quickSort(array, index+1, right);
        }
        
    }
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = in.nextInt();
        }
        quickSort(array, 0, array.length - 1);
    }
}
