import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void quickSort(int ar[], int start, int end){
        if(start < end){
            int p = partition(ar, start, end);
            printArray(ar);
            quickSort(ar, start , p - 1);
            quickSort(ar, p+1 , end);
        }
    }
    public static int partition(int ar[], int start , int end){
        int p = ar[end];
        int index = start;
        for(int i = start ; i < end ; i++){
                if(ar[i] < p){
                    int t = ar[index];
                    ar[index] = ar[i];
                    ar[i] = t;
                    index++;
                }
        }
        int t = ar[end];
        ar[end] = ar[index];
        ar[index] = t;
        return index;
    }
    public static void printArray(int a[]){
        for(int i : a){
            System.out.print(i + " ");
        }
        System.out.println();
    }
    public static void main(String[] args) throws Exception{
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bf.readLine());
        int ar[] = new int[n];
        StringTokenizer st = new StringTokenizer(bf.readLine());
        for(int i = 0 ; i < n ; i++)
            ar[i] = Integer.parseInt(st.nextToken());
        quickSort(ar, 0 , n - 1);
       
    }
    
}
