import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static void quickSortNonStable(int[] ar, int low, int high) {
		int pivot = ar[high];
		int m = low - 1; // last element smaller than pivot
		int n = low; // last element larger than pivot
		
		while (n <= high - 1) {
			if (ar[n] < pivot) {
                int temp = ar[m + 1];
                ar[m + 1] = ar[n];
                ar[n] = temp;
                m++;
                n++;		
			} else {
				n++;
			}
		}
		
        int temp = ar[m + 1];
        ar[m + 1] = ar[high];
        ar[high] = temp;
        
        printArray(ar, 0, ar.length - 1);
		
		if (low < m) quickSortNonStable(ar, low, m);
		if (m + 2 < high) quickSortNonStable(ar, m + 2, high);
	}
	
	static void printArray(int[] ar, int low, int high) {
		if (high - low + 1 <= 1) return;
		for (int i = low; i <= high; i++) System.out.print(ar[i] + " ");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int i = 0; i < n; i++) {
			ar[i] = in.nextInt();
		}
		quickSortNonStable(ar, 0, ar.length - 1);
		in.close();
	}
}
