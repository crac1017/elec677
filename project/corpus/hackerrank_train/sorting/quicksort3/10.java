
import java.util.*;
public class Solution {
       
          static int partition(int[] ar,int l,int h) {
            int x = ar[h];    // pivot
               int i = (l - 1);
                   
                for (int j = l; j <= h- 1; j++)
                    {
        
                if (ar[j] <= x)
                    {
                     i++;    
                    //swap(&arr[i], &arr[j]);  
                    int temp = ar[i];
                        ar[i] = ar[j];
                        ar[j] = temp;
                    }
                }
         
                int temp = ar[i+1];
                    ar[i+1] = ar[h];
                    ar[h] = temp;
            
       //swap(&arr[i + 1], &arr[h]); 
    return (i + 1);
            //return j;  
       }   
 static void quick(int []ar , int low, int high) {
     
     if(low >= high)
     return ;
     else{ 
     int pivot = partition(ar,low,high);
         printArray(ar);
        quick(ar,low,pivot-1);
         quick(ar,pivot+1,high);
     }
 }
     
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           quick(ar,0,n-1);
       }    
   }
