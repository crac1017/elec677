import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    private static int partition(int[] ar, int low, int high){
        int pivot = high;
        int small = low;
        int large = low;
        while(small < high && large < high){
            while (low < high && ar[low] < ar[pivot]){
                low++;
            }
            small = low;
            large = low;
            while (large < high && ar[large] <= ar[pivot]){
                large++;
            }
            while (small < high && ar[small] > ar[pivot]){ //find first smaller
                small++;
            }
            if (small > large){
                int temp = ar[small];
                ar[small] = ar[large];
                ar[large] = temp;
                large++;
            }
        }
        int temp = ar[pivot];
        ar[pivot] = ar[small];
        ar[small] = temp;
        for (int i =0; i < ar.length; i++){
            System.out.print(ar[i] + " ");
        }
        System.out.println();
        return low;
    }
    
    private static void quicksort(int[] ar, int low, int high){
        if (high > low){
            int pivot = partition(ar, low, high);
            quicksort(ar, low, pivot - 1);
            quicksort(ar, pivot + 1, high);
        }
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner stdin = new Scanner(System.in);
        int n = stdin.nextInt();
        int[] ar = new int[n];
        for (int i=0; i <n; i++){
            ar[i] = stdin.nextInt();
        }
        quicksort(ar, 0, n-1);
    }
}
