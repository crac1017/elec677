


import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public class Main {
	static FasterScanner sc;
	static int N = 100000;
	static boolean prime[] = new boolean[N+1];
	static int[][] A;
	
        /*
        int n = sc.nextInt(), q = sc.nextInt();
        long arr[][] = new long[3][n+1];
        long arr2[][] = new long[3][n+1];
        for(int i = 0;i<q;i++){
        	int a = sc.nextInt();
        	if(a==1){
        		int x = sc.nextInt(), y = sc.nextInt(), z = sc.nextInt();
        		long val = sc.nextLong();
        		for(int j=x;j<=n;j++){
        			arr[0][j] += val;
        		}
        		for(int j=x;j<=n;j++){
        			arr[1][j] += val;
        		}
        		for(int j=x;j<=n;j++){
        			arr[2][j] += val;
        		}
        		for(int j=Math.max(x,y);j<=n;j++){
        			arr2[0][j] += val;
        		}
        		for(int j=Math.max(z,y);j<=n;j++){
        			arr2[1][j] += val;
        		}
        		for(int j=Math.max(x,z);j<=n;j++){
        			arr2[2][j] += val;
        		}
        	}
        	else{
        		int x = sc.nextInt(), y = sc.nextInt(), z = sc.nextInt(),X = sc.nextInt(), Y = sc.nextInt(), Z = sc.nextInt();
        		long temp1=arr[0][x],temp2=arr[1][y],temp3=arr[2][z],temp4=arr2[0][x],temp5=arr2[1][y],temp6=arr2[2][z];
        		if(x==0) {
        			temp1 = 0;temp4 = 0;
        		}
        		else {
        			temp1 = arr[0][x-1];
        			temp4 = arr2[0][x-1];
        		}
        		if(y==0) {
        			temp2 = 0;temp5 = 0;
        		}
        		else {
        			temp2 = arr[1][y-1];
        			temp5 = arr2[1][y-1];
        		}
        		if(z==0) {
        			temp3 = 0;temp6 = 0;
        		}
        		else {
        			temp3 = arr[2][z-1];
        			temp6 = arr2[2][z-1];
        		}
        		log.write(arr[0][X]-temp1 + arr[1][Y]-temp2 + arr[2][Z]+temp4+temp5+temp6-temp3-arr2[0][X]-arr[1][Y]-arr[2][Z]+"\n");
        	}
        }*/
	
	
	public static void main(String args[] ) throws Exception {
        FasterScanner sc=  new FasterScanner();
        BufferedWriter log = new BufferedWriter(new OutputStreamWriter(System.out));
        long n = sc.nextLong();
        System.out.println((long)Math.pow(2, countBits(n)));
	}
	static int countBits(long n)
	{
	  int count = 0,cnt = 0;
	  while(n>0)
	  {
		  cnt++;
	    count += n & 1;
	    n >>= 1;
	  }
	  return cnt-count;
	}
   /* public static void dfs(int x, HashSet<Integer> hs){
    	hs.add(x);
    	b[x] = true;
    	for(int i = 0;i<arr[x].size();i++){
    		if(!b[arr[x].get(i)]) dfs(arr[x].get(i),hs);
    	}
    }*/
	
	public static void mergeSort(int[] arr, int start, int end){
		
		if(start<end){
			int mid = (start+end)/2;
			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);
		merge(arr,start,mid,end);
		}
	}
	public static void merge(int arr[],int start, int mid, int end){
		int left[] = new int[mid-start+1];
		int right[] = new int[end-mid];
		for(int i= 0;i<mid-start+1;i++){
			left[i] = arr[i+start];
			//System.out.println(left[i-start]);
		}
		for(int i = 0;i<end-mid;i++){
			right[i] = arr[i+mid+1];
			//System.out.println(right[i-mid]+"*");
		}
		int k = start, i =0, j = 0;
		while(i<mid-start+1 && j<end-mid){
			if(left[i]<right[j]){
				arr[k++] = left[i];
				i++;
			}
			else{
				arr[k++] = right[j];
				j++;
			}
			
		}
		while(i<mid-start+1) {
			arr[k++] = left[i];i++;
		}
		while(j<end-mid) {
			arr[k++] = right[j];j++;
		}
	}
	
	
		
	public static class FasterScanner {
		private byte[] buf = new byte[1024];
		private int curChar;
		private int numChars;

		public int read() {
			if (numChars == -1)
				throw new InputMismatchException();
			if (curChar >= numChars) {
				curChar = 0;
				try {
					numChars = System.in.read(buf);
				} catch (IOException e) {
					throw new InputMismatchException();
				}
				if (numChars <= 0)
					return -1;
			}
			return buf[curChar++];
		}

		public String nextLine() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isEndOfLine(c));
			return res.toString();
		}

		public String nextString() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			StringBuilder res = new StringBuilder();
			do {
				res.appendCodePoint(c);
				c = read();
			} while (!isSpaceChar(c));
			return res.toString();
		}

		public long nextLong() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			long res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}

		public int nextInt() {
			int c = read();
			while (isSpaceChar(c))
				c = read();
			int sgn = 1;
			if (c == '-') {
				sgn = -1;
				c = read();
			}
			int res = 0;
			do {
				if (c < '0' || c > '9')
					throw new InputMismatchException();
				res *= 10;
				res += c - '0';
				c = read();
			} while (!isSpaceChar(c));
			return res * sgn;
		}
	        
	    public int[] nextIntArray(int n) {
	        return nextIntArray(n, 0);
	    }
	    
	    public int[] nextIntArray(int n, int off) {
	    	int[] arr = new int[n + off];
	    	for (int i = 0; i < n; i++) {
	    		arr[i + off] = nextInt();
	    	}
	    	return arr;
	    }
	    
	    public long[] nextLongArray(int n) {
	    	return nextLongArray(n, 0);
	    }
        
		public long[] nextLongArray(int n, int off) {
		    long[] arr = new long[n + off];
		    for (int i = 0; i < n; i++) {
		        arr[i + off] = nextLong();
		    }
		    return arr;
		}

	    private boolean isSpaceChar(int c) {
			return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
		}

		private boolean isEndOfLine(int c) {
			return c == '\n' || c == '\r' || c == -1;
		}
	}


}
