import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        
        String bin=Long.toBinaryString(n);
        long ans=1;
        for(int i=0;i<bin.length();i++)
            {
            if(bin.charAt(i)=='0')
                ans*=2;
        }
        if(n==0)
            ans--;
        System.out.println(ans);
    }
}
