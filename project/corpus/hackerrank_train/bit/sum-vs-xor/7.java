import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        int zeroes=0;
        while(n>1){
            if(n%2==0)
                zeroes++;
            n/=2;
        }
        long ans =(long)(Math.pow(2,zeroes));
        System.out.println(ans);
    }
}
