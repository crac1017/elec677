Sum vs XOR
Given an integer, , find each  such that:



where  denotes the bitwise XOR operator. Then print an integer denoting the total number of 's satisfying the criteria above.
