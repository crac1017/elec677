import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        
        if(n == 0){
            System.out.println("1");
            return;
        }
        
        String s = Long.toBinaryString(n);
        long ans = 1;
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '0'){
                ans *= 2;
            }
        }
        
        System.out.println(ans);
    }
}
