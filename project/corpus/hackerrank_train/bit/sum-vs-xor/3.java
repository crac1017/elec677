import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextLong();
        int count = 0;
        while (n != 0) {
            int r = (int) n % 2;
            n = n / 2;
            if (r == 0) {
                count++;
            }
        }
        long result = 1;
        for (int i = 0; i < count; i++) {
            result = result * 2;
        }
        System.out.println(result);
    }
}
