

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
	
    public static void main(String[] args)  throws Exception {
    	Scanner inputStream = null;
        try {
        	inputStream = new Scanner(System.in);
            int length = Integer.valueOf(inputStream.nextLine());
            String []inputArray = inputStream.nextLine().split(" ");
            findaOnlyOneOccurence(inputArray,length);
        } finally {
            inputStream.close();
        }
        
    }
    
    public static void findaOnlyOneOccurence(String []inputArray, int length) {
    	String result = "";
    	int temp = 0;
    	if(inputArray.length == 1) {
    		System.out.println(inputArray[0]);
    		return;
    	}
    	for(int i = 0; i < length; i++) {
    		temp =  0;
    		for(int j= 0; j < length; j++) {
    			if(inputArray[i].equals(inputArray[j])) {
    				temp++;
    			}
    		}
    		if(temp == 1) {
    			result = inputArray[i];
    		}
    	}
    	System.out.println(result);
    }
    
    
}
