import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] A = new int[101];
        for (int n=0; n<N; n++) {
            A[input.nextInt()]++;
        }
        for (int n=0; n<=100; n++) {
            if (A[n]%2 == 1) {
                System.out.println(n);
            }
        }
    }
    
}
