import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
static int lonelyinteger(int[] a) {
    int loneNumber = 0;
    if(a.length>=1)
        loneNumber = a[0];
    boolean flag = false;
    for(int i=0;i<a.length;i++){
        for(int j=0;j<a.length;j++){
            if(i!=j)
            if(a[i]==a[j]){
                flag=false;
                break;
            }else{
                flag = true;
            }
        }
        if(flag)
            loneNumber = a[i];
    }
return loneNumber;

    }
public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int res;
        
        int _a_size = Integer.parseInt(in.nextLine());
        int[] _a = new int[_a_size];
        int _a_item;
        String next = in.nextLine();
        String[] next_split = next.split(" ");
        
        for(int _a_i = 0; _a_i < _a_size; _a_i++) {
            _a_item = Integer.parseInt(next_split[_a_i]);
            _a[_a_i] = _a_item;
        }
        
        res = lonelyinteger(_a);
        System.out.println(res);
        
    }
}
