import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    private static long xorAll(long N){
        if(N<=0) return 0;
        return N & ((N % 2)==1 ? 0L : ~0L) | ( ((N & 2)>>1) ^ (N & 1L) );
    }
    private static long xorEven(long n){
        if(n<=0) return 0;
        return xorAll(n/2) << 1;
    }
    private static long xorOdd(long n){
        if(n<=0) return 0;
        return xorAll(n) ^ xorEven(n-1);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int Q = in.nextInt();
        for(int a0 = 0; a0 < Q; a0++){
            long L = in.nextLong();
            long R = in.nextLong();
            long result = 0;
            if(L%2==1 && R%2==1){
                result = xorOdd(R) ^ xorAll(L) ^ xorOdd(L);
            }
            if(L%2==0 && R%2==0){
                result = xorEven(R) ^ xorAll(L) ^ xorEven(L);
            }
            if(L%2==0 && R%2==1){
                result = xorOdd(R) ^ xorOdd(L-1);
            }
            if(L%2==1 && R%2==0){
                result = xorEven(R) ^ xorEven(L-1);
            }
            System.out.println(result);
        }
    }
}
