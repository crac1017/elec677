import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static long getA(long n){
        if (n%4==0) return n;
        if (n%4==1) return 1;
        if (n%4==3) return 0;
        return n+1;
            
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int Q = in.nextInt();
        for(int a0 = 0; a0 < Q; a0++){
            long L = in.nextLong();
            long R = in.nextLong();
            long sum=0;
            if (R-L<=80){
                for (long i=L; i<=R; i++){
                    sum^=getA(i);
                }
            }else{
                long endL=L-L%8+16-1;
                long startR=R-R%8-16;    //TODO??? +-1
                for (long i=L; i<=endL; i++){
                    sum^=getA(i);
                }
                for (long i=startR; i<=R; i++){
                    sum^=getA(i);
                }
            }
            System.out.println(sum);
        }
    }
}
