import java.util.*;
import java.io.*;
public class XorSequence
{
	public static void main(String[] args) throws Exception
	{
		FastScanner in = new FastScanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		
		int q = in.nextInt();
		for(int x = 0; x < q; x++)
		{
			long l = in.nextLong();
			long r = in.nextLong();
			
			out.println(getSum(l - 1) ^ getSum(r));
		}
		
		out.close();
	}
	
	public static long getSum(long val)
	{
		long index = val % 8;
		
		if(index == 2 || index == 3)
		{
			return 2;
		}
		else if(index == 6 || index == 7)
		{
			return 0;
		}
		else if(index == 0 || index == 1)
		{
			return 8 * (val / 8) + index;
		}
		else
		{
			return 8 * (val / 8) + index + 2;
		}
	}
	
	static class FastScanner
	{
		BufferedReader br;
		StringTokenizer st;
		
		public FastScanner(InputStream input)
		{
			br = new BufferedReader(new InputStreamReader(input));
			st = new StringTokenizer("");
		}
		
		public String next() throws IOException
		{
			if(st.hasMoreTokens())
			{
				return st.nextToken();
			}
			else
			{
				st = new StringTokenizer(br.readLine());
				return next();
			}
		}
		
		public int nextInt() throws IOException
		{
			return Integer.parseInt(next());
		}
		
		public long nextLong() throws IOException
		{
			return Long.parseLong(next());
		}
	}
}
