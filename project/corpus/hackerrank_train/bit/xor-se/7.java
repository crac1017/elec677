import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        final Scanner s = new Scanner(System.in);
        final int q = s.nextInt();
        for (int i = 0; i < q; i++) {
            final long l = s.nextLong();
            final long r = s.nextLong();
            long sum = 0;
            if (r - l < 4) {
                for (long j = l; j <= r; j++) {
                    sum ^= c(j);
                }
            } else {
                final long lP = 4 * (l / 4) + 4;
                final long rP = 4 * (r / 4);
                sum += ((rP - lP) / 4 % 2) * 2 ^ sum(l, lP - 1) ^ sum(rP, r);
            }
            System.out.println(sum);
        }
    }

    static long sum(long id1, long id2) {
        long sum = 0;
        for (long i = id1; i <= id2; i++) {
            sum ^= c(i);
        }
        return sum;
    }

    static long c(long idx) {
        if (idx % 4 == 0) return idx;
        if (idx % 2 == 0) return idx + 1;
        if ((idx + 1) % 4 == 0) return 0;
        return 1;
    }
}
