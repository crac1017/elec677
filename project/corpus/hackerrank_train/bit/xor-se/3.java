import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int Q = in.nextInt();
        
        for(int a0 = 0; a0 < Q; a0++){
            long L = in.nextLong();
            long R = in.nextLong();
            System.out.println(ant(R)^ant(L-1));
        }
        
    }
    public static long ant (long n){
        if(n%2==0){
            return 2*ans(n/2);
        }
        return ans(n)+2*ans(n/2);
    }
    public static long ans (long n){
        if(n==0) return 0;
        if(n==1) return 1;
        if(n%2==1){
            if(n%4==1){
                return 1;
            }
            return 0;
        }
        else{
            return n^(ans(n-1));
        }
    }
}
