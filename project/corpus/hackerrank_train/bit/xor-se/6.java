import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class Solution {
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		new solve().sol();
	}

}
class solve{	
	void sol() throws Exception{
		ModScanner in=new ModScanner();
		
		int q=in.nextInt();
		for(int i=0;i<q;i++){
			long l=in.nextLong();
			long r=in.nextLong();
			long div=r/8;
			long x1,x2;
			if((r-div*8)==2 ||(r-div*8)==3 ){
				x1=2;
			}
			else{
				if((r-div*8)==6 ||(r-div*8)==7 ){
					x1=0;
				}
				else{
					if(r==1)
						x1=1;
					else
						if((r-div*8)==1 ||(r-div*8)==0)
						  x1=r;
						else
						  x1=r+2;	
				}
			}
			if(r==1)
				x1=1;
			r=l-1;
			div=r/8;
			if((r-div*8)==2 ||(r-div*8)==3 ){
				x2=2;
			}
			else{
				if((r-div*8)==6 ||(r-div*8)==7 ){
					x2=0;
				}
				else{
					if(r==1)
						x2=1;
					if((r-div*8)==1 ||(r-div*8)==0)
						  x2=r;
						else
						  x2=r+2;
				}
			}
			if(l==1)
				x2=0;
			
			//System.out.println(x1+" "+x2);
			long ans=x1^x2;
			System.out.println(ans);
		}
	}
}
class ModScanner {
	BufferedReader br;
	StringTokenizer st;
 
	public ModScanner() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}
 
	String nextToken() throws Exception {
		while (st == null || !st.hasMoreElements()) {
				st = new StringTokenizer(br.readLine());
			
		}
		return st.nextToken();
	}
 
	int nextInt() throws Exception, Exception {
		return Integer.parseInt(nextToken());
	}
 
	long nextLong() throws Exception {
		return Long.parseLong(nextToken());
	}
 
	double nextDouble() throws Exception {
		return Double.parseDouble(nextToken());
	}
} 
