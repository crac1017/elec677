import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.*;


public class A
{
    String line;
    StringTokenizer inputParser;
    BufferedReader is;
    FileInputStream fstream;
    DataInputStream in;
    String FInput="";
    
    void openInput(String file)
    {
        if(file==null)is = new BufferedReader(new InputStreamReader(System.in));//stdin
        else
        {
                try{
        
                        
                fstream = new FileInputStream(file);
                in = new DataInputStream(fstream);
                is = new BufferedReader(new InputStreamReader(in));
                }catch(Exception e)
                {
                        System.err.println(e);
                }
        }
    }
    
    void readNextLine()
	{
		try {
			line = is.readLine();
			inputParser = new StringTokenizer(line, " ,\t");
			//System.err.println("Input: " + line);
		} catch (IOException e) {
			System.err.println("Unexpected IO ERROR: " + e);
		}	
		catch (NullPointerException e)
		{
			line=null;
			
		}
		
	}
    
    long NextLong()
    {
            String n = inputParser.nextToken();
            
            long val = Long.parseLong(n);
            
             return val;
    }
    
    int NextInt()
    {
            String n = inputParser.nextToken();
            int val = Integer.parseInt(n);
            
            //System.out.println("I read this number: " + val);
            return val;
    }
    
    double NextDouble()
    {
            String n = inputParser.nextToken();
            double val = Double.parseDouble(n);
            
            //System.out.println("I read this number: " + val);
            return val;
    }
    
    String NextString()
    {
            String n = inputParser.nextToken();
            return n;
    }
    
    void closeInput()
    {
            try {
                    is.close();
            } catch (IOException e) {
                    System.err.println("Unexpected IO ERROR: " + e);
            }
                    
    }
    
    
    public static void main(String [] argv)
    {
            //String filePath="circles.in";
            String filePath=null;
            if(argv.length>0)filePath=argv[0];
            new A(filePath);
            
    }
    
    public A(String inputFile)
    {
    	openInput(inputFile);
		//readNextLine();
		int T=1;//NextInt();
		StringBuilder sb = new StringBuilder();
		for(int t=1; t<=T; t++)
		{
			readNextLine();
			int Q=NextInt();
			for(int i=0; i<Q; i++)
			{
				readNextLine();
				long L = NextLong();
				long R = NextLong();
				long ret=0;
				for(long j=L; j<=R; j++)
				{
					if(j%4==0)ret^=j;
					else if(j%4==1)ret^=1;
					else if(j%4==2)ret^=(j+1);
					else {
						long z=R;
						for(; z>0; z--)
						{
							if(z%4==3)break;
						}
						if(z>j)
						{
							long num=(z-j)/4;
							if(num%2==1)ret^=2;
							j=z;
						}
					}
				}
				sb.append(ret);
				sb.append("\n");
			}
			
		}
		System.out.print(sb);
		
		closeInput();		
	}

}
