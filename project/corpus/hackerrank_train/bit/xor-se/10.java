import java.io.*;
import java.util.*;
import java.math.*;

/*
 * Created by Epsilon Alpha on 27-Apr-16 at 12:52 AM.
 */

class XORSequence
{
    public static void main(String[] args) throws IOException
    {
        int Q = Reader.nextInt();
        while(Q-->0)
        {
            long L = Reader.nextLong();
            long R = Reader.nextLong();
            long answer = 0;
            for(long i = L; i<=R;i++)
            {
                if(i%4==3)
                    continue;
                else if(i%4==0)
                {
                    if(i+8<R)
                        i+=8*((R-i)/8);
                    if(i+4<R)
                    {
                        answer^=2;
                        i+=3;
                        continue;
                    }
                    else
                        answer ^= i;
                }
                else if(i%4==2)
                    answer^=(i+1);
                else if(i%4==1)
                {
                    if((answer&1)==0)//even
                        answer++;
                    else
                        answer--;
                }
            }
            System.out.println(answer);
        }
    }
}

class Reader
{
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static StringTokenizer st = new StringTokenizer("");

    static String next() throws IOException
    {
        while (!st.hasMoreTokens())
            st = new StringTokenizer(br.readLine());
        return st.nextToken();
    }

    static int nextInt() throws IOException
    {
        return Integer.parseInt(next());
    }

    static long nextLong() throws IOException
    {
        return Long.parseLong(next());
    }

    static BigInteger nextBI() throws IOException
    {
        return new BigInteger(next());
    }
}
