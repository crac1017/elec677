import java.util.Scanner;

/**
 * Created by kunals726 on 2/1/2016.
 */
public class Xor1 {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t--!=0) {
            long l=sc.nextLong(),r=sc.nextLong();
            long mod=(l-1)%8,modr=(r)%8;
            long x=0,y=0;
            l=l-1;
          //  System.out.println(mod+" dd"+modr);
            if(mod-1==0||(mod==0)){
                x=l;
            }
            if(modr-1==0||(modr==0)){
                y=r;
            }
            if(mod-1==4||(mod==4)){
                x=l+2;
            }
            if(modr-1==4||(modr==4)){
                y=r+2;
            }
            if(mod-1==6||(mod==7)){
                x=0;
            }
            if(modr-1==6||(modr==6)){
                y=0;
            }
            if(mod-1==2||(mod==2)){
                x=2;
            }
            if(modr-1==2||(modr==2)){
                y=2;
            }
           // System.out.println(x+" "+y);
            System.out.println(x^y);
        }

    }
}
