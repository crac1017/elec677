//package week8;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.TreeSet;

class Solution {

    static StringBuilder sb = new StringBuilder("");
    static HashMap<BigInteger, Boolean> powers = new HashMap<BigInteger, Boolean>();
    static TreeSet<BigInteger> sortedpowers = new TreeSet<BigInteger>();

    public static void main(String[] args) throws Exception {
        computePowers();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine());
        for (int i = 0; i < T; i++) {
            BigInteger N = new BigInteger(br.readLine());
            solve(N);
        }
        System.out.print(sb.toString());
    }

    public static void computePowers() {
        for (int i = 0; i <= 64; i++) {
            BigInteger t = BigInteger.valueOf(2).pow(i);
            powers.put(t, Boolean.TRUE);
            sortedpowers.add(t);
        }
    }

    public static void solve(BigInteger N) {
        if (N.equals(BigInteger.ONE)) {
            sb.append("Richard").append("\n");
            return;
        }
        int counter = 0;
        while (!N.equals(BigInteger.ONE)) {
            if (powers.containsKey(N)) {
                N = N.subtract(N.divide(BigInteger.valueOf(2)));
            } else {
                BigInteger closest = sortedpowers.first();
                for (BigInteger curr : sortedpowers) {
                    if (curr.compareTo(N) == -1) {
                        closest = curr;
                    } else {
                        break;
                    }
                }
                N = N.subtract(closest);
            }
            counter++;
        }
        if (counter % 2 == 0) {
            sb.append("Richard").append("\n");
        } else {
            sb.append("Louise").append("\n");
        }
    }
}
