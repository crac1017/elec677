import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        BigInteger[] bits = new BigInteger[65];
        bits[0] = BigInteger.ONE;
        for (int i = 1; i < bits.length; i++)
            bits[i] = bits[i - 1].shiftLeft(1);

        for (int T = cin.nextInt(); T > 0; T--) {
            BigInteger n = new BigInteger(cin.next());
            String win = "Richard";

            while (!n.equals(BigInteger.ONE)) {
                if (n.and(n.subtract(BigInteger.ONE)).equals(BigInteger.ZERO))
                    n = n.shiftRight(1);
                else {
                    for (int i = bits.length - 1; i >= 0; i--)
                        if (n.and(bits[i]).equals(bits[i])) {
                            n = n.subtract(bits[i]);
                            break;
                        }
                }
                if (win.equals("Louise"))
                    win = "Richard";
                else
                    win = "Louise";
            }

            System.out.println(win);
        }

        cin.close();
    }

    static class Pair<U extends Comparable<U>, V extends Comparable<V>> implements Comparable<Pair<U, V>> {
        final U _1;
        final V _2;

        private Pair(U key, V val) {
            this._1 = key;
            this._2 = val;
        }

        public static <U extends Comparable<U>, V extends Comparable<V>> Pair<U, V> instanceOf(U _1, V _2) {
            return new Pair<U, V>(_1, _2);
        }

        @Override
        public String toString() {
            return _1 + " " + _2;
        }

        @Override
        public int hashCode() {
            int res = 17;
            res = res * 31 + _1.hashCode();
            res = res * 31 + _2.hashCode();
            return res;
        }

        @Override
        public int compareTo(Pair<U, V> that) {
            int res = this._1.compareTo(that._1);
            if (res < 0 || res > 0)
                return res;
            else
                return this._2.compareTo(that._2);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (!(obj instanceof Pair))
                return false;
            Pair<?, ?> that = (Pair<?, ?>) obj;
            return _1.equals(that._1) && _2.equals(that._2);
        }
    }
}
