import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    private static boolean solve(BigInteger n) {
        if (n.equals(BigInteger.ONE)) return true;
        return rwin(n, true);
    }

    private static boolean rwin(BigInteger n, boolean rw) {
        if (n.equals(BigInteger.ONE)) return rw;
        BigInteger t = BigInteger.ONE;
        while (t.compareTo(n) < 0) t = t.shiftLeft(1);
        if (t.compareTo(n) == 0) return rwin(n.shiftRight(1), !rw);
        return rwin(n.subtract(t.shiftRight(1)), !rw);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T = in.nextInt();
        for (int i = 0; i < T; i++) {
            System.out.println(
                solve(new BigInteger(in.next())) ?
                "Richard" : "Louise");
        }
    }

}
