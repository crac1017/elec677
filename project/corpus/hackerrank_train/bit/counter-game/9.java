import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Solution {

    
    static StringBuilder sb=new StringBuilder();
    static void parse(String l, int[] here) {
        sb.setLength(0);
        int len=l.length(), j=0;
        for (int i=0; i<len; i++) {
            char c=l.charAt(i);
            if(c==' ') {
                here[j++]=Integer.parseInt(sb.toString());
                sb.setLength(0);
            } else {
                sb.append(c);
            }
        }
        if(sb.length()>0) {
            here[j++]=Integer.parseInt(sb.toString());
        }
    }

    static int hibit(BigInteger i) {
        for (int j=64; j>=0; j--) {
            if(i.testBit(j)) {
                return j;
            }
        }
        throw new IllegalStateException();
    }
    
    static void run_stream(InputStream ins) throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(ins));
        int T=Integer.parseInt(br.readLine());
        for (int test=0; test<T; test++) {
            BigInteger n=new BigInteger(br.readLine());
            int cnt=0;
            while(!n.equals(BigInteger.ONE)) {
                cnt++;
                if(hibit(n)==n.getLowestSetBit()) {
                    n=n.shiftRight(1);
                } else {
                    n=n.clearBit(hibit(n));
                }
            }
            
            System.out.println((cnt&1)>0? "Louise": "Richard");
        }
    }

    public static void main(String[] args) throws IOException {
        run_stream(System.in);
    }

}
