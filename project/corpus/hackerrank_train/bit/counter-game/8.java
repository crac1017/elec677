import java.util.*;
import java.math.BigInteger;
public class Solution {
    public static void main (String[] args) {
        Scanner in = new Scanner (System.in);
        int T = in.nextInt();
        while(T-- > 0) {
            BigInteger M = new BigInteger(in.next());
            System.out.println(steps(M) % 2 == 0 ? "Richard" : "Louise");
        }
    }
    public static int steps(BigInteger m) {
        if (m.compareTo(BigInteger.ONE) == 0) return 0;
        BigInteger two = new BigInteger(String.valueOf(Long.highestOneBit(m.divide(new BigInteger("2")).longValue()))).multiply(new BigInteger("2"));
        if (two.compareTo(m) == 0) return 1 + steps(m.divide(new BigInteger("2")));
        else return 1 + steps(m.subtract(two));
    }
}
