import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Solution {
  public static BufferedReader in;
  public static PrintWriter out;
  private static final int mod = 1000000007;

  public static void main(String[] args) throws Exception {
    in = new BufferedReader(args.length > 0 ? new java.io.FileReader(args[0]) : new java.io.InputStreamReader(System.in));
    out = new PrintWriter(System.out);
    try {

      int t = Integer.parseInt(in.readLine());
      for (int i = 0; i < t; i++) {
        out.println(process(Integer.parseInt(in.readLine()), in.readLine()));
      }

    } catch (Exception e) {
      e.printStackTrace(out);
    } finally {
      out.flush();
      out.close();
      in.close();
    }
  }

  private static int process(int n, String tlist) {
    StringTokenizer st = new StringTokenizer(tlist);

    // binary or of all numbers
    int or = 0;
    while (st.hasMoreTokens()) {
      // the xoring ninja
      or |= Integer.valueOf(st.nextToken());
    }

    // the rest is to compute
    // (or << (n - 1)) mod (10^9 + 7)
    return mulMod(or % mod, powMod(2, n - 1));
  }

  private static int powMod(int number, int n) {
    switch (n) {
    case 0:
      return 1;
    case 1:
      return number;
    case 2:
      return mulMod(number, number);
    default:
      if ((n & 1) == 1) {
        return mulMod(2, powMod(number, n - 1));
      } else {
        int powModRecur = powMod(number, n / 2);
        return mulMod(powModRecur, powModRecur);
      }
    }
  }

  private static int mulMod(int a, int b) {
    long r = (((long) a) * ((long) b)) % mod;
    return (int) r;
  }
}
