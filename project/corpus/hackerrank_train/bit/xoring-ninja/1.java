import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;


public class Solution {

	static Solution main;
	
	static long sp;
	
	public static void main(String[] args) {
		sp = 1000000007;
		long [] fact = new long[110001];
		long [] pow2 = new long[110001];
		pow2[0] = 1;
		fact[0] = 1;
		for(int i = 1 ; i < 110001 ; i++) {
			fact[i] = fact[i-1] * i;
			fact[i] %= sp;
			pow2[i] = 2*pow2[i-1];
			if(pow2[i]>=sp) {
				pow2[i]-=sp;
			}
		}
		main = new Solution();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream bos = new BufferedOutputStream(System.out);
		String eol = System.getProperty("line.separator");
		byte[] eolb = eol.getBytes();
		try {
			String str = br.readLine();
			int t = Integer.parseInt(str);
			for(int i = 0 ; i < t ; i++) {
				str = br.readLine();
				int n = Integer.parseInt(str);
				int [] ar = new int[40];
				Arrays.fill(ar, 0);
				str = br.readLine();
				int j=0;
				int s=0;
				int length = str.length();
				while(j<length) {
					while(j<length) {
						if(str.charAt(j) == ' ') {
							break;
						}else {
							j++;
						}
					}
					int x = Integer.parseInt(str.substring(s,j)) ;	
					int iter = 0;
					while(x>0) {
						if((x%2)==1) {
							ar[iter]++;
						}
						x/=2;
						iter++;
					}
					j++;
					s=j;			
				}
				long ans = 0;
				for(int a = 0 ;a < 40 ; a++) {
					int x = ar[a];
					for(int b = 1 ; b <= x; b+=2) {
						// x choose b * 2^(n-x) * 2^a
						long tempAns = fact[x];
						tempAns *= inverse(fact[b]);
						tempAns %= sp;
						tempAns *= inverse(fact[x-b]);
						tempAns %= sp;
						tempAns *= pow2[n-x+a];
						tempAns %= sp;
						ans += tempAns;
						if(ans>sp) {
							ans -= sp;
						}
					}
				}
				bos.write(new Long(ans).toString().getBytes());
				bos.write(eolb);
			}
			bos.flush();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public static long inverse(long a) {
		return getPow(a,sp-2);
	}
	
	public static long getPow(long a , long b) {
		long ans = 1;
		long pow = a;
		while(b>0) {
			if((b%2)==1) {
				ans *= pow;
				ans %= sp;
			}
			pow *= pow;
			pow %= sp;
			b/=2;
		}
		return ans;
	}

}
