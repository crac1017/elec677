import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i=0;i<t;i++) {
            int n = sc.nextInt();
            int[] l = new int[n];
            int x = 0;
            for (int j=0;j<n;j++) {
                l[j] = sc.nextInt();
                x = x | l[j];
            }
            System.err.println(x);
            BigInteger bi = new BigInteger("2");
            bi = bi.pow(n-1);
            System.out.println(bi.multiply(new BigInteger(String.valueOf(x))).mod(new BigInteger("1000000007")));

        }
    }
}
