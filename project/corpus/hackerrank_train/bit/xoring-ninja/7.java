import java.io.InputStreamReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 * @author Nipuna Samarasekara
 */
public class Solution {
	public static void main(String[] args) {
		InputStream inputStream = System.in;
		OutputStream outputStream = System.out;
		FastScanner in = new FastScanner(inputStream);
		FastPrinter out = new FastPrinter(outputStream);
		Task3 solver = new Task3();
		solver.solve(1, in, out);
		out.close();
	}
}

class Task3 {
   /////////////////////////////////////////////////////////////
  static long mod=1000000007;
    public void solve(int testNumber, FastScanner in, FastPrinter out) {
    int T=in.nextInt();
        while(T-->0){
            int N=in.nextInt();
            int[] A=in.readIntArray(N);
           long y=0;
            for (int i : A) {
                y|=i;
            }
     long ans=0;
            for (int i = 0; i < 31; i++) {
            long kk=0;
                if((y&(1<<i))>0)kk=PowerMod(2,N-1,mod);



                ans+=(long)Math.pow(2,i)*kk;
                ans%=mod;
            }
            out.println(ans);

        }


    }
    static  long  PowerMod(long a, long b, long m) {
        long  tempo;
        if (b ==0 ){
            tempo =  1;  //EXIT condition
        }//if
        else  if (b == 1) tempo = a;
        else { long temp = PowerMod(a,b/2,m);
            if (b%2 == 0)
                tempo = (temp*temp)%m;
            else
                tempo = ((temp*temp)%m)*a%m;
        }
        return tempo;
    } //POWERMOD method

}

class FastScanner extends BufferedReader {

    public FastScanner(InputStream is) {
        super(new InputStreamReader(is));
    }

    public int read() {
        try {
            int ret = super.read();
//            if (isEOF && ret < 0) {
//                throw new InputMismatchException();
//            }
//            isEOF = ret == -1;
            return ret;
        } catch (IOException e) {
            throw new InputMismatchException();
        }
    }

    static boolean isWhiteSpace(int c) {
        return c >= 0 && c <= 32;
    }

    public int nextInt() {
        int c = read();
        while (isWhiteSpace(c)) {
            c = read();
        }
        int sgn = 1;
        if (c == '-') {
            sgn = -1;
            c = read();
        }
        int ret = 0;
        while (c >= 0 && !isWhiteSpace(c)) {
            if (c < '0' || c > '9') {
                throw new NumberFormatException("digit expected " + (char) c
                        + " found");
            }
            ret = ret * 10 + c - '0';
            c = read();
        }
        return ret * sgn;
    }

    public String readLine() {
        try {
            return super.readLine();
        } catch (IOException e) {
            return null;
        }
    }

    public int[] readIntArray(int n) {
        int[] ret = new int[n];
        for (int i = 0; i < n; i++) {
            ret[i] = nextInt();
        }
        return ret;
    }

    }

class FastPrinter extends PrintWriter {

    public FastPrinter(OutputStream out) {
        super(out);
    }

    public FastPrinter(Writer out) {
        super(out);
    }


}

