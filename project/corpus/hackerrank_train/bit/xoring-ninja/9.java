import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static long mod=1000000007;
    public static void main(String[] args) {
      int t;
      Scanner in=new Scanner(System.in);
      t=in.nextInt();
      while(t-->0){
        int n;
        n=in.nextInt();
        long ones[]=new long[64];
        long zeros[]=new long[64];
        for(int i=0;i<n;i++){
        	String num=Integer.toBinaryString(in.nextInt());
        	for(int j=num.length()-1, k=0;k<64;k++,j--){
        		if(j>=0 && num.charAt(j)=='1') ones[k]++; 
        		else zeros[k]++;
        	}
        }
        long count=0;
        for(int i=0;i<64;i++){
        	if(ones[i]==0) ;
        	else{
        		count=(count+multip((multip(powMod(2,ones[i]-1),powMod(2,zeros[i]))%mod),powMod(2,i))%mod)%mod;
        	}
        }
        System.out.println(count);
       }
    }
    static long multip(long a,long b){
    	if(b==0) return 0;
    	if(b==1)  return a;
    	a=a%mod;b=b%mod;
    	if(b%2==0){ a=(2*a)%mod; return (multip(a,b/2))%mod;}
    	else return (a+multip(a,b-1))%mod;
    }
    static long powMod(long a,long b){
    	if(b==0) return 1;
    	if(b%2==0) return (powMod(a,b/2)*powMod(a,b/2))%mod;
    	else return (a*(powMod(a,(b-1)/2)*powMod(a,(b-1)/2))%mod)%mod;
    }
}
