//package hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Solution {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    PrintWriter writer = new PrintWriter(System.out);
    StringTokenizer stringTokenizer;

    String next() throws IOException {
        while (stringTokenizer == null || !stringTokenizer.hasMoreTokens()) {
            stringTokenizer = new StringTokenizer(reader.readLine());
        }
        return stringTokenizer.nextToken();
    }

    int nextInt() throws IOException {
        return Integer.parseInt(next());
    }

    long nextLong() throws IOException {
        return Long.parseLong(next());
    }

    final int MOD = 1000 * 1000 * 1000 + 7;

    int sum(int a, int b) {
        a += b;
        return a >= MOD ? a - MOD : a;
    }
    int product(int a, int b) {
        return (int)(1L * a * b % MOD);
    }
    int pow(int x, int k) {
        int result = 1;
        while(k > 0) {
            if(k % 2 == 1) {
                result = product(result, x);
            }
            x = product(x, x);
            k /= 2;
        }
        return result;
    }
    int inv(int x) {
        return pow(x, MOD - 2);
    }

    void solveTest() throws IOException {
        int n = nextInt();
        int[] cnt = new int[32];
        for(int i = 0; i < n; i++) {
            int x = nextInt();
            for(int j = 0; j < 32; j++) {
                cnt[j] += x & 1;
                x >>= 1;
            }
        }
        int[] f = new int[n + 1];
        f[0] = 1;
        for(int i = 1; i <= n; i++) {
            f[i] = product(f[i - 1], i);
        }
        int[][] dp = new int[n + 1][2];
        dp[0][0] = 1;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < 2; j++) {
                dp[i + 1][j] = sum(dp[i + 1][j], dp[i][j]);
                dp[i + 1][j + 1 & 1] = sum(dp[i + 1][j + 1 & 1], dp[i][j]);
            }
        }
        int ans = 0;
        for(int i = 31; i >= 0; i--) {
            int m = cnt[i];
            ans = product(ans, 2);
            ans = sum(ans, product(pow(2, n - m), dp[m][1]));
        }
        writer.println(ans);
    }

    void solve() throws IOException {
        for(int i = nextInt(); i >= 1; i--) {
            solveTest();
        }
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        new Solution().solve();
    }
}
