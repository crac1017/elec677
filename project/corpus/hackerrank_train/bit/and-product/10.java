import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        int T;
        Scanner scr = new Scanner(System.in);
        T = scr.nextInt();
        long a,b;
        long N[] = new long[T];
        String str[] = new String[T];
        for(int i = 0 ; i < T ; i++)
        {
            a = scr.nextLong();
            b = scr.nextLong();
            if(a > b)   N[i] = b;
            else        N[i] = a;
        }
        for(int i = 0 ; i < T ; i++)
        {
            System.out.println(N[i]);
        }
        
    }
}
