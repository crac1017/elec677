import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.math.BigInteger;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 * @author AlexFetisov
 */
public class Solution {
	public static void main(String[] args) {
		InputStream inputStream = System.in;
		OutputStream outputStream = System.out;
		InputReader in = new InputReader(inputStream);
		PrintWriter out = new PrintWriter(outputStream);
		ANDProduct solver = new ANDProduct();
		int testCount = Integer.parseInt(in.next());
		for (int i = 1; i <= testCount; i++)
			solver.solve(i, in, out);
		out.close();
	}
}

class ANDProduct {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long A = in.nextLong();
        long B = in.nextLong();
        long result = 0;
        for (int i = 0; i < 32; ++i) {
            if (cantBeOff(A, B, i)) {
                result |= (1L << i);
            }
        }
        out.println(result);
    }

    private boolean cantBeOff(long a, long b, int bit) {
        if ((a & (1L << bit)) != 0 && (b & (1L << bit)) != 0) {
            long fullMask = 0;
            long tempA = a;
            for (int i = 0; i <= bit; ++i) {
                fullMask |= (1L << i);
                if ((a & (1L << i)) != 0) {
                    tempA ^= (1L << i);
                }
            }
            tempA |= fullMask;
            if (tempA >= b) {
                return true;
            }
        }
        return false;
    }
}

class InputReader {
    private BufferedReader reader;
    private StringTokenizer stt;

    public InputReader(InputStream stream) {
        reader = new BufferedReader(new InputStreamReader(stream));
    }

    public String nextLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            return null;
        }
    }

    public String nextString() {
        while (stt == null || !stt.hasMoreTokens()) {
            stt = new StringTokenizer(nextLine());
        }
        return stt.nextToken();
    }

    public long nextLong() {
        return Long.parseLong(nextString());
    }

    public String next() {
        return nextString();
    }
}

