import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 * @author Agostinho Junior (junior94)
 */
public class Solution {
	public static void main(String[] args) {
		InputStream inputStream = System.in;
		OutputStream outputStream = System.out;
		InputReader in = new InputReader(inputStream);
		OutputWriter out = new OutputWriter(outputStream);
		ANDProduct solver = new ANDProduct();
		int testCount = Integer.parseInt(in.next());
		for (int i = 1; i <= testCount; i++)
			solver.solve(i, in, out);
		out.close();
	}
}

class ANDProduct {
	public void solve(int testNumber, InputReader in, OutputWriter out) {
		long a = in.readLong();
		long b = in.readLong();
		long res = 0;
		for (int bit = 31; bit >= 0; bit--) {
			long mask = 1L << bit;
			if ((mask & a) == (mask & b)) {
				if ((mask & a) > 0) {
					res |= mask;
				}
			} else {
				break;
			}
		}
		out.println(res);
	}
}

class InputReader {
    private BufferedReader input;
    private StringTokenizer line = new StringTokenizer("");
	public InputReader(InputStream in) {
        input = new BufferedReader(new InputStreamReader(in));
    }
	public void fill() {
        try {
            if(!line.hasMoreTokens()) line = new StringTokenizer(input.readLine());
        } catch(IOException io) { io.printStackTrace(); System.exit(0);}
    }
    public String next() {
        fill();
        return line.nextToken();
    }
	public long readLong() {
        fill();
        return Long.parseLong(line.nextToken());
    }
}

class OutputWriter {
    private PrintWriter output;
	public OutputWriter(OutputStream out) {
        output = new PrintWriter(out);
    }
	public void println(Object o) {
        output.println(o);
    }
	public void close() {
        output.close();
    }
}

