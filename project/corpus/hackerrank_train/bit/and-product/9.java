import java.math.BigInteger;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int T = scanner.nextInt();

		for (int t = 0; t < T; t++) {
			long a = scanner.nextLong();
			long b = scanner.nextLong();

			if (a == b) {
				System.out.println(a);
			} else {

				StringBuffer result = new StringBuffer();

				for (int i = 1; i <= 32; i++) {
					if (hasMultiple(a, b, i)) {
						result.insert(0, 0);
					} else {
						if (sample(a, i)) {
							result.insert(0, 1);
						} else {
							result.insert(0, 0);
						}
					}
				}

				System.out.println(new BigInteger(result.toString(), 2));
			}
		}

		scanner.close();
	}

	public static boolean sample(long from, int k) {
		return ((from >> (k - 1)) & 1) > 0;
	}

	// checks if there is a multiple of the kth 2 power present in the range
	public static boolean hasMultiple(long from, long to, int k) {
		long num = 1L << k;
		long value = from / num;
		return value * num == from || (value + 1) * num <= from;
	}

}
