import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i = 0; i < n; i++) {
            long min = sc.nextLong();
            long max = sc.nextLong();
            System.out.println(andProduct(min, max));
        }
    }
    
    private static long andProduct(long min, long max) {
        if(min == max) {
            return min;
        }
        long range = max-min;
        long powerOfTwo = 1;
        while(powerOfTwo < range) {
            powerOfTwo *= 2;
        }
        powerOfTwo /= 2;
        return min & ~powerOfTwo;
    }
}
