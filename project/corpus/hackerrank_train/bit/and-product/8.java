import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws Exception{
//        Scanner in = new Scanner(new File("input/andproduct"));
        Scanner in = new Scanner(System.in);

        int t = in.nextInt();

        for (int i = 0; i < t; i++) {
            long a = in.nextLong();
            long b = in.nextLong();

            long diff = Math.max(((long) (Math.log(b - a) / Math.log(2)) - 1),0);;
            long newA = a >> diff;
            long newB = b >> diff;

            long ret = newA;
            for (long j = newA; j <= newB; j++) {
                ret = ret & j;
            }

            ret = ret << diff;
            System.out.println(ret);
        }
    }
}
