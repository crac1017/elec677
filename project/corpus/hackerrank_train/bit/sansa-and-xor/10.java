import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        int T = Integer.valueOf(line);
        for (int t = 0; t < T; t++) {
            line = br.readLine();
            int N = Integer.valueOf(line);
            line = br.readLine();
            if (N % 2 == 0) {
              System.out.println(0);    
            } else {
              int xor = 0;
              String[] input = line.split(" ");
              for (int i = 0; i < N; i+=2) {
                int a = Integer.valueOf(input[i]);
                xor ^= a;
              }
              System.out.println(xor);
            }
        }
    }
}
