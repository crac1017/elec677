import java.util.Scanner;

public class Solution {

	static long getXor(long[] t) {
		long result = 0;
		if (t.length % 2 != 0) {
			result = t[0];
			for (int i=2; i < t.length; i+=2) {
				result ^= t[i];
			}
		}
		
		return result;
	}

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		int c = in.nextInt();
		long[] result = new long[c];

		for (int i = 0; i < c; i++) {
			int n = in.nextInt();

			long[] t = new long[n];

			for (int j = 0; j < n; j++) {
				t[j] = in.nextLong();
			}

			result[i] = getXor(t);
		}

		for (int i = 0; i < c; i++)
			System.out.println(result[i]);

	}
}
