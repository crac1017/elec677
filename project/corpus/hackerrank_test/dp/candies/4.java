/*
 Enter your code here. Read input from STDIN. Print output to STDOUT
 Your class should be named Solution
*/


import java.util.Scanner;

/**
 *
 * @author chaitanya
 */
public class Solution {
	
	public static int find(int [] students){
		int totalCount=0;
		int present=Integer.MAX_VALUE;
		
		int incCount=0;
		int decCount=0;
		boolean dec=false; 
		for(int i=0;i<students.length;i++){
			if(students[i]<present){
				decCount++;
				dec=true;
				if(decCount==incCount){
					decCount++;
				}
				totalCount=totalCount+decCount;
			}else if(students[i]>present){
				if(dec){
					incCount=1;
					dec=false;
                                        decCount=0;
				}
				incCount++;
				totalCount=totalCount+incCount;
			}else{
				decCount=0;
				incCount=1;
				dec=false;
				totalCount++;
			}
			present=students[i];
		}
		
		return totalCount;
	}

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int totalStudents=sc.nextInt();
		
		int[] students= new int[totalStudents];
		for(int i=0;i<totalStudents;i++)
			students[i]=sc.nextInt();
		
		System.out.println(find(students));
		
//		System.out.println(find(new int[]{1,2,2}));
//		System.out.println(find(new int[]{1,2,3,4}));
		
	}
	
}
