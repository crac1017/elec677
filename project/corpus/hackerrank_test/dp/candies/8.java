/*
 Enter your code here. Read input from STDIN. Print output to STDOUT
 Your class should be named Solution
*/
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Solution {
    public static int inputs;
    public static int [] ratings = new int [100010];
    public static Solution candie = new Solution ();
    public static int [] candies = new int [100010];
    public static PriorityQueue<child> children = new PriorityQueue<Solution.child>(100010,candie.new comp());
    public static void main(String[] args) {
    	 for (int i=0;i<100010;i++) {
         	ratings[i]=1000000;
         }
        Scanner sc = new Scanner(System.in);
        int i=1;
        while(sc.hasNextLine()) {
        	String mAndk = sc.nextLine();
            inputs=Integer.parseInt (mAndk.split(" ")[0]);
            
            while (i<=inputs) {
            	ratings[i] = Integer.parseInt(sc.nextLine());
            	candies[i] = 1;
            	children.add(candie.new child (i, ratings[i]));
            	i++;
            }
            if (i>inputs) break;
        }
        System.out.println (calc ());
    }
    public static int calc () {
    	int count = 0;
    	while (!children.isEmpty()) {
    		//System.out.println (" children = " + children);
    		child minChild = children.poll();
    		int selfRating = minChild.rating;
    		int leftRating = ratings[minChild.index-1];
    		int rightRating = ratings[minChild.index+1];
    		if (selfRating==leftRating && leftRating==rightRating)  candies[minChild.index] = 1;
    		if (leftRating<selfRating && rightRating>=selfRating) candies[minChild.index] = candies[minChild.index-1]+1;
    		if (leftRating>=selfRating && rightRating<selfRating) candies[minChild.index] = candies[minChild.index+1]+1;
    		if (leftRating<selfRating && rightRating<selfRating) {
    			/*if (leftRating>=rightRating) candies[minChild.index] = candies[minChild.index-1]+1;
    			else candies[minChild.index] = candies[minChild.index+1]+1;*/
    			if (candies[minChild.index-1]>candies[minChild.index+1]) candies[minChild.index] = candies[minChild.index-1]+1;
    			else candies[minChild.index] = candies[minChild.index+1]+1;
    		}
    		//System.out.println ("index = " + minChild.index + " candies = " + candies[minChild.index]);
    	}
    	for (int i=1;i<=inputs;i++) {
    		count+=candies[i];
    	}
    	return count;
    }
    public class child{
    	int rating;
    	int index;
    	public child (int index, int rating) {
    		this.index = index;
    		this.rating = rating;
    	}
    	public String toString () {
    		return index + " " + rating; 
    	}
    }
    public class comp implements Comparator<child> {
    	 @Override
    	    public int compare(child x, child y)
    	    {
    		 	if (x.rating < y.rating) return -1;
    		 	else if (y.rating < x.rating) return 1;
    		 	else return 0;
    	    }
    }
}
