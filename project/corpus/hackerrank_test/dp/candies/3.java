
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

public class Solution {

    static int n;
    static int b[] = new int[100000];
    static int a[] = new int[100000];
    static int dem[] = new int[100000];
    static Integer tt[] = new Integer[100000];
    static int idx = 0;

    static void build_maxpos() {
        int vt = 0;
        while (vt < n) {
            int res = 1;
            int j = vt;
            while (j < n - 1 && a[j] < a[j + 1] && b[j + 1] == 0) {
                j++;
                res++;
            }
            dem[vt] = res;
            vt = j + 1;
        }

        vt = n - 1;
        while (vt >= 0) {

            int r1 = 1;

            int j = vt;
            while (j > 0 && a[j - 1] > a[j] && b[j - 1] == 0) {

                j--;
                r1++;
            }
            dem[vt] = Math.max(r1, dem[vt]);
            vt = j - 1;
        }
    }

    public static void main(String args[]) throws IOException {

        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
        n = Integer.parseInt(buf.readLine());
        for(int i = 0;i < n; ++i) {
            a[i] = Integer.parseInt(buf.readLine());
            tt[i] = i;
        }
        build_maxpos();
        Arrays.sort(tt, 0, n,new Comparator<Integer>() {

            @Override
            public int compare(Integer t, Integer t1) {
                return Integer.valueOf(dem[t1]).compareTo(dem[t]);
            }
        });
        for(int i = 0 ; i <  n; ++i) {
            int vt = tt[i];

            if (b[vt] != 0) {
                continue;
            }

            int j = vt;
            b[j] = 1;

            while (j < n - 1 && a[j] < a[j + 1] && b[j + 1] == 0) {
                b[j + 1] = b[j] + 1;
                j++;
            }

            j = vt;
            while (j > 0 && a[j - 1] > a[j] && b[j - 1] == 0) {
                b[j - 1] = b[j] + 1;
                j--;
            }
        }

        for(int i = 0 ; i <  n - 1; ++i) {
            if (a[i] < a[i + 1] && !(b[i] < b[i + 1])) {
                b[i + 1] = b[i] + 1;
            }

            if (a[i] > a[i + 1] && !(b[i] > b[i + 1])) {
                b[i] = b[i + 1] + 1;
            }
        }

        long ans = 0l;
        
        for(int i = 0 ; i <  n; ++i)
		ans += (long) b[i];
        System.out.println(ans);

        
        buf.close();
    }
}
