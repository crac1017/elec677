import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    private static int[] score;
    private static int[] candies;
    private static int N;
    
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        N = input.nextInt();

        score = new int[N];

        for (int n = 0; n < N; n++) score[n] = input.nextInt();
        
        candies = new int[N];
        Arrays.fill(candies, 1);
        
        input.close();

        int res = solve();

        System.out.println(res);

    }
    
    private static int solve() {

        for (int n = 0; n < 10; n++)
        for (int i = 0; i < N; i++) {
            if (i + 1 < N && score[i] > score[i+1] && candies[i] <= candies[i+1]) candies[i] = candies[i+1] + 1;
            if (i > 0 && score[i] > score[i-1] && candies[i] <= candies[i-1]) candies[i] = candies[i-1] + 1;
        }

        int res = 0;

        for (int i = 0; i < N; i++) res += candies[i];

        return res;

    }


}
