/*
 Enter your code here. Read input from STDIN. Print output to STDOUT
 Your class should be named Solution
*/
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {

	static long getMinCandies(int[] ratings){
		int[] numberOfRatings = new int[ratings.length];
		long sum =0;
		Arrays.fill(numberOfRatings,1);
		for (int i = 1; i < numberOfRatings.length; i++) {
			if(ratings[i] > ratings[i-1])
				numberOfRatings[i] = numberOfRatings[i-1] +1;
		}
		for (int i = ratings.length-2; i >=0; i--) {
			if(ratings[i] > ratings[i+1] && numberOfRatings[i] <= numberOfRatings[i+1] )
				numberOfRatings[i] = numberOfRatings[i+1] +1;
		}
		for (int i = 0; i < numberOfRatings.length; i++) {
			sum += numberOfRatings[i];
		}
		
		//System.out.println();
		for (int i = 0; i < numberOfRatings.length; i++) {
		//	System.out.print(numberOfRatings[i] + " ");
		}
		//System.out.println();
	return sum;
	}
	public static void main(String[] args) {
		Scanner s = new Scanner(new InputStreamReader(System.in));
		int N = s.nextInt();
		int[] ratings = new int[N];
		for (int i = 0; i < ratings.length; i++) {
			ratings[i] = s.nextInt();
		}
		System.out.println(getMinCandies(ratings));
	}

}
