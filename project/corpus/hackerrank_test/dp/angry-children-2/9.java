import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

 public static Long min_value = Long.MAX_VALUE;
    public static int range;
 public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int total_packs = Integer.parseInt(stdin.nextLine());
         range= Integer.parseInt(stdin.nextLine());
        //given the packs, first, sort all values to array sorted
        //the difference will be sorted[i+range]-sorted[i]
        //calculate this for total_packs-range times and return the min value
        Long[] inputArray = new Long[total_packs];
        for(int i=0;i<total_packs;i++){
            //add current value to unsorted array
            inputArray[i] = Long.parseLong(stdin.nextLine());
        }
        Arrays.sort(inputArray);
     Long sum = 0L;
     Long current_dif = 0L;
     for(int i=range-1;i>=0;i--){
         current_dif+= inputArray[i]*(2*i-range+1);
         sum+=inputArray[i];
         min_value = current_dif;
     }
     
         for(int i=1;i<total_packs-range;i++){
             sum-=inputArray[i-1];
             current_dif+=(inputArray[i-1])*(range-1)-sum;
             current_dif+=(inputArray[i+range-1])*(range-1)-sum;
             sum+=inputArray[i+range-1];
//              System.out.println(current_dif);
             if(current_dif<min_value){
                 min_value=current_dif;
             }
         }
         System.out.println(min_value);
    }
    
}
