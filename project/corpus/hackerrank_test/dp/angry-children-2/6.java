import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution {

	public static void main(String [] args ) {
		try{
			String str;			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			BufferedOutputStream bos = new BufferedOutputStream(System.out);
			String eol = System.getProperty("line.separator");
			byte [] eolb = eol.getBytes();
			str  = br.readLine();
			int n = Integer.parseInt(str);
			str  = br.readLine();
			int k = Integer.parseInt(str);
			long [] ar = new long[n];
			for(int i = 0 ; i < n ; i++) {
				str  = br.readLine();
				ar[i] = Integer.parseInt(str);
			}
			Arrays.sort(ar);
			long [] sum = new long[n];
			sum[0] = ar[0];
			for(int i = 1 ; i < n ; i++) {
				sum[i] = sum[i-1] + ar[i];
			}
			long ans = 0;
			long mult = k-1;
			mult*=-1;
			for(int i = 0 ; i < k ; i++) {
				ans += mult*ar[i];
				mult += 2;
			}
			long unf = ans;
			for(int i = 1 ; i <= n-k ; i++) {
				long newUnf = unf + (k-1) * ar[i+k-1] + (k-1) * ar[i-1] - 2 * (sum[i+k-2] - sum[i-1]) ;
				if(newUnf<ans) {
					ans = newUnf;
				}
				unf = newUnf;
			}
			bos.write(new Long(ans).toString().getBytes());
			bos.write(eolb);
			bos.flush();
		}  catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
