import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * @author Student
 */

public class Solution {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        FastScanner in = new FastScanner(inputStream);
        FastPrinter out = new FastPrinter(outputStream);
        TaskE solver = new TaskE();
        solver.solve(1, in, out);
        out.close();
    }
}


class FastScanner extends BufferedReader {

    boolean isEOF;

    public FastScanner(InputStream is) {
        super(new InputStreamReader(is));
    }

    @Override
    public int read() {
        try {
            int ret = super.read();
//            if (isEOF && ret < 0) {
//                throw new InputMismatchException();
//            }
//            isEOF = ret == -1;
            return ret;
        } catch (IOException e) {
            throw new InputMismatchException();
        }
    }

    public String next() {
        StringBuilder sb = new StringBuilder();
        int c = read();
        while (isWhiteSpace(c)) {
            c = read();
        }
        if (c < 0) {
            return null;
        }
        while (c >= 0 && !isWhiteSpace(c)) {
            sb.appendCodePoint(c);
            c = read();
        }
        return sb.toString();
    }

    static boolean isWhiteSpace(int c) {
        return c >= 0 && c <= 32;
    }

    public String nextToken() {
        return next();
    }

    public int nextInt() {
        int c = read();
        while (isWhiteSpace(c)) {
            c = read();
        }
        int sgn = 1;
        if (c == '-') {
            sgn = -1;
            c = read();
        }
        int ret = 0;
        while (c >= 0 && !isWhiteSpace(c)) {
            if (c < '0' || c > '9') {
                throw new NumberFormatException("digit expected " + (char) c
                        + " found");
            }
            ret = ret * 10 + c - '0';
            c = read();
        }
        return ret * sgn;
    }

    public char nextChar() {
        int c = read();
        while (isWhiteSpace(c)) {
            c = read();
        }
        return (char) c;
    }

    public long nextLong() {
        return Long.parseLong(next());
    }

    public double nextDouble() {
        return Double.parseDouble(next());
    }

    public String nextLine() {
        int c = read();
        String ret = readLine();
        if (ret == null) {
            return ret;
        }
        if (c != 13) {
            return (char) c + ret;
        }
        return ret;
    }

    public String readLine() {
        try {
            return super.readLine();
        } catch (IOException e) {
            return null;
        }
    }

    public int readTimeHM(char delim) {
        String s = next();
        int pos = s.indexOf(delim);
        if (pos < 0) {
            throw new NumberFormatException("no delim found");
        }
        return Integer.parseInt(s.substring(0, pos)) * 60 + Integer.parseInt(s.substring(pos + 1));
    }

    public int readTimeHMS(char delim1, char delim2) {
        String s = next();
        int pos = s.indexOf(delim1);
        if (pos < 0) {
            throw new NumberFormatException("no delim found");
        }
        int pos2 = s.indexOf(delim2, pos + 1);
        if (pos2 < 0) {
            throw new NumberFormatException("no second delim found");
        }
        return Integer.parseInt(s.substring(0, pos)) * 3600 + Integer.parseInt(s.substring(pos + 1, pos2)) * 60
                + Integer.parseInt(s.substring(pos2 + 1));
    }

    public int readTimeHMS(char delim) {
        return readTimeHMS(delim, delim);
    }

    public int[] readIntArray(int n) {
        int[] ret = new int[n];
        for (int i = 0; i < n; i++) {
            ret[i] = nextInt();
        }
        return ret;
    }

    public int[][] readInt2DArray(int n, int m) {
        int[][] ret = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                ret[i][j] = nextInt();
            }
        }
        return ret;
    }

    public long[] readLongArray(int n) {
        long[] ret = new long[n];
        for (int i = 0; i < n; i++) {
            ret[i] = nextLong();
        }
        return ret;
    }

    public double[] readDoubleArray(int n) {
        double[] ret = new double[n];
        for (int i = 0; i < n; i++) {
            ret[i] = nextDouble();
        }
        return ret;
    }

    public String[] readTokenArray(int n) {
        String[] ret = new String[n];
        for (int i = 0; i < n; i++) {
            ret[i] = next();
        }
        return ret;
    }

    public char[][] readCharacterFieldTokens(int n, int m) {
        char[][] ret = new char[n][];
        for (int i = 0; i < n; i++) {
            ret[i] = next().toCharArray();
            if (ret[i].length != m) {
                throw new AssertionError("length expected " + m + ", found " + ret[i].length);
            }
        }
        return ret;
    }

}


class FastPrinter extends PrintWriter {

    public FastPrinter(OutputStream out) {
        super(out);
    }

    public FastPrinter(Writer out) {
        super(out);
    }

    public void printArray(int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (i > 0) {
                print(' ');
            }
            print(a[i]);
        }
        println();
    }


}
/////////////////////////////////////////////////////////////////////////

class TaskE {
    public void solve(int testNumber, FastScanner in, FastPrinter out) {
        int N = in.nextInt(), k = in.nextInt();

        long[] A = in.readLongArray(N);

        long sum = 0;
        long sum_1 = 0;
        long ans;
        Arrays.sort(A);
        long mul = -k + 1;
        for (int i = 0; i < k; i++) {
            sum += A[i];
            sum_1 += A[i] * mul;
            mul += 2;

        }

        ans = sum_1;
        for (int i = k; i < N; i++) {
            sum += A[i] - A[i - (k)];
            sum_1 += (long) (k - 1) * A[i - k];
            sum_1 -= 2 * sum;
            sum_1 += (long) (k + 1) * A[i];
            ans=Math.min(ans, sum_1);
            //   System.out.println("kjvdsc "+sum_1);
        }

        out.println(ans);
    }
}
