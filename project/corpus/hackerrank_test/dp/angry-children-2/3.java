
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution {

	public static void main(String[] args) throws NumberFormatException,
			IOException {
		BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in), 1024 * 1024 * 2);

		int n = Integer.parseInt(br.readLine());
		int k = Integer.parseInt(br.readLine());
		int[] v = new int[n];
		for (int i = 0; i < n; i++) {
			v[i] = Integer.parseInt(br.readLine());
		}
		Arrays.sort(v);

		long sums[] = new long[n];
		sums[0] = v[0];
		for (int i = 1; i < n; i++) {
			sums[i] = sums[i - 1] + v[i];
		}
		long min = computeVal(0, v, k, sums);
		long sumPrec = min;
		for (int i = 1; i + k - 1 < n; i++) {
			sumPrec = computeValUpdate(i, v, k, sums, sumPrec);
			min = Math.min(min, sumPrec);
		}
		System.out.println(min);
	}

	private static long computeValUpdate(int pos, int[] v, int k, long[] sums,
			long sumPrec) {
		int p = pos - 1;
		return sumPrec + (long) (k - 1) * (v[pos + k - 1] + v[pos - 1]) - 2L
				* (sums[p + k - 1] - sums[p]);
	}

	private static long computeVal(int pos, int[] v, int k, long[] sums) {
		long sum = 0;
		for (int i = pos; i < pos + k; i++) {
			long x = sums[pos + k - 1] - sums[i];
			x -= (long) (pos + k - 1 - i) * v[i];
			sum += x;
		}
		return sum;
	}
}
