import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        long[] c = new long[n];
        for (int i = 0; i < n; i++) {
            c[i] = in.nextInt();
        }
        Arrays.sort(c);
        long minDiff = 0;
        for (int i = 0; i < k/2; i++) {
            minDiff+=((c[k-i-1] - c[i])*(k-1-2*i));
        }
        //System.out.println("firstDiff " + minDiff);
        long[] sumK1 = new long[n-k+1];
        sumK1[0] = 0;
        for (int i = 0; i < k-1; i++)
            sumK1[0]+=c[i];
        //System.out.println("0 " + sumK1[0]);
        for (int i = 1; i <= n-k; i++) {
            sumK1[i] = sumK1[i-1] + c[i+k-2] - (i == 0? 0 : c[i-1]);
            //System.out.println(i + " " +sumK1[i]);
        }
        
        long diff = minDiff;
        for (int i = 1; i <= n-k; i++) {
            diff = diff + (c[i+k-1] + c[i-1]) * (k - 1) - sumK1[i] * 2;
            minDiff = minDiff < diff ? minDiff : diff;
        }
        System.out.println(minDiff);
    }
}
