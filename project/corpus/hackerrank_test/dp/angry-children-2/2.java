import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) {
        Scanner S = new Scanner(System.in);
        int N = S.nextInt(), K = S.nextInt();
        int[] x = new int[N];
        long[] u = new long[N-K+1];
        for (int i = 0; i < N; ++i) x[i] = S.nextInt();
        Arrays.sort(x);
        long l = 0, f = 0;
        for (int i = 1; i < K; ++i) {
            l += i*(x[i]-x[i-1]);
            u[0] += l;
            f += x[i] - x[0];
        }
        for (int i = 1; i < N-K+1; ++i) {
            l -= x[i+K-2] - x[i-1];
            l += (K-1)*(x[i+K-1] - x[i+K-2]);
            u[i] = u[i-1] - f + l;
            f -= (K-1)*(x[i]-x[i-1]);
            f += x[i+K-1] - x[i];
        }
        Arrays.sort(u);
        System.out.println(u[0]);
    }
}
