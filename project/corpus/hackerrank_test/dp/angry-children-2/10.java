import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
     public static Long min_v = Long.MAX_VALUE;
     public static int range;
     public static Long dif = 0L;
     public static Long mid = 0L; 
 public static void main(String[] args) {   
        Scanner stdin = new Scanner(System.in);
        int total_packs = Integer.parseInt(stdin.nextLine());
        range= Integer.parseInt(stdin.nextLine());
        Long[] inputArray = new Long[total_packs];
        for(int i=0;i<total_packs;i++){
            //add current value to unsorted array
            inputArray[i] = Long.parseLong(stdin.nextLine());
        }
     Arrays.sort(inputArray);
     for(int i=range-1;i>=0;i--){
         dif+= inputArray[i]*(2*i-range+1);
         mid+=inputArray[i];
         min_v = dif;
     }
    for(int i=1;i<total_packs-range;i++){
         mid-=inputArray[i-1];
         dif+=(inputArray[i+range-1]+inputArray[i-1])*(range-1)-2*mid;
         mid+=inputArray[i+range-1];
         if(dif<min_v){
             min_v=dif;
             }
         }
         System.out.println(min_v);
    }
    
}
