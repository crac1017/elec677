
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Solution {

    private void printArray(long[] a ){
        for (long k : a) {
            System.out.print(k+" ");
        }
        System.out.println("");
    }
    
    public long solve(long[] a, int K) throws IOException {
        long cost = 0, val = 0;
        int i = 0, N = a.length;
        Arrays.sort(a);
        //printArray(a);
        long[] sum = new long[N+1];
        for (i = N - 1; i >= 0; i--) {
            sum[i] = a[i] + sum[i + 1];
        }
        //printArray(sum);
        for (i = 0; i < K; i++) {
            cost += sum[i + 1] - sum[K] - (K - 1 - i) * a[i];
//            System.out.print(cost+", ");
        }
        for (i = K, val = cost; i < N; i++) {
            val = val + 2 * sum[i] - 2 * sum[i - K + 1] + (K - 1) * (a[i] + a[i - K]);
            cost = Math.min(val, cost);
        }
        return cost;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(br.readLine());
        int K = Integer.parseInt(br.readLine());
        long[] arr = new long[N];
        for (int i = 0; i < N; i++) {
            arr[i] = Long.parseLong(br.readLine());
        }
        Solution sol = new Solution();
        System.out.println(sol.solve(arr, K));
    }
}
