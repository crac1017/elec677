import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        Solution obj = new Solution();
        int n = sc.nextInt();
        int k = sc.nextInt();
        long[] candies = new long[n];
        for (int i=0;i<n;i++) candies[i]=sc.nextLong();

        Arrays.sort(candies);
        long max=0;
        long sum = 0;
        long temp_max=0;
        int j=1,factor= 1-k;
        
        for (int i=0;i<k;i++) {
            max+=(factor*candies[i]);
            factor+=2;
            sum+=candies[i];
        }
        temp_max=max;
        sum-=candies[0];
        
        while (j+k<=n) {
            temp_max+=((k-1)*(candies[j-1]+candies[j+k-1]))-(2*sum);
            sum+=(candies[j+k-1]-candies[j]);
            if (temp_max<max) max = temp_max;
            j++;
        }
        
        
        
        
        
        System.out.println(max);
    }
}
