import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;


public class Abbreviation {
	public static void main(String[] args) {
		Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in), 256 << 10));
		int testsNumber = in.nextInt();
		for (int test = 0; test < testsNumber; test++) {
			String orig = in.next();
			String dest = in.next();
			int n = orig.length();
			int m = dest.length();
			boolean[][] pos = new boolean[m+1][n+1];
			pos[0][0] = true;
			for (int i = 0; i < m; i++) {
				char cc = dest.charAt(i);
				int j = 0;
				while (j < n) {
					while (j < n && !pos[i][j])
						j++;
					while (j < n && !Character.isUpperCase(orig.charAt(j))) {
						if (Character.toUpperCase(orig.charAt(j)) == cc) {
							pos[i+1][j+1] = true;
						}
						j++;
					}
					if (j < n && orig.charAt(j) == cc) {
						pos[i+1][j+1] = true;
					}
                    j++;
				}
			}
			int last = n;
            while (last >= 0 && !pos[m][last])
            	last--;
            if (last < 0) {
            	System.out.println("NO");
            }
            else {
            	boolean noMoreUppercase = true;
            	for (int j = last; j < n; j++)
            		if (Character.isUpperCase(orig.charAt(j))) {
            			noMoreUppercase = false;
            			break;
            		}
            	System.out.println(noMoreUppercase ? "YES" : "NO");
            }
		}
		in.close();
	}
}
