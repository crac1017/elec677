import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
		 int q = Integer.parseInt(in.nextLine());
		 for(int i=0 ; i < q; i++) {
			 String a = in.nextLine();
			 String b = in.nextLine();
			 String c;StringBuilder bui = new StringBuilder();
			 for(int j=0; j < a.length(); j++) {
				 if(Character.getType(a.charAt(j)) == Character.UPPERCASE_LETTER) {
					bui.append(Character.toUpperCase(a.charAt(j))); 
				 }
			 }
			 c = bui.toString();
			 
			 int match1 = 0; int cur1 = 0;
			 for(int j=0; j < b.length(); j++) {
				 if(cur1 >= c.length()) break;
				 if(Character.toUpperCase(b.charAt(j)) == c.charAt(cur1)) {
					 cur1++;match1++;
				 }
				 
			 }
			 
			 if(match1 != c.length()) {
				 System.out.println("NO");
			 }
			 else {
			 
			 int match = 0; int cur = 0;
			 for(int j=0; j < a.length(); j++) {
				 if(cur >= b.length()) break;
				 if(Character.toUpperCase(a.charAt(j)) == b.charAt(cur)) {
					 cur++;match++;
				 }
				 
			 }
			 if(match == b.length()) {
				 System.out.println("YES");
			 }
			 else {
				 System.out.println("NO");
			 }
			 
			 }
		 }
		 
    }
}
