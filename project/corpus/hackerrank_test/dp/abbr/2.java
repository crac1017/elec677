import java.io.*;
import java.util.StringTokenizer;

public class Solution {
    private static final int MAX = 1000;
    private static final boolean[][] dp = new boolean[MAX+1][MAX+1];

    private static void solve() throws Exception {
        int Q = in.nextInt();
        while (Q-- > 0) {
            char[] a = in.next().toCharArray();
            char[] b = in.next().toCharArray();
            int A = a.length;
            int B = b.length;

            for (int i = 0; i <= A; i++) {
                dp[i][0] = true;
            }
            for (int i = 1; i <= A; i++) {
                for (int j = 1; j <= B; j++) {
                    dp[i][j] = false;
                    if (Character.isLowerCase(a[i-1])) {
                        dp[i][j] |= dp[i-1][j];
                    }
                    if (Character.toUpperCase(a[i-1]) == b[j-1]) {
                        dp[i][j] |= dp[i-1][j-1];
                    }
                }
            }

            /*for (int i = 1; i <= A; i++) {
                for (int j = 1; j <= B; j++) {
                    out.print(dp[i][j] ? 1 : 0);
                }
                out.println();
            }*/

            out.println(dp[A][B] ? "YES" : "NO");
        }
    }

    private static Reader in = new Reader();
    private static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out));
    public static void main(String[] args) throws Exception {
        solve();
        out.flush();
    }

    private static class Reader {
        private BufferedReader reader;
        private StringTokenizer tokenizer;

        Reader() {
            reader = new BufferedReader(new InputStreamReader(System.in));
            tokenizer = new StringTokenizer("");
        }

        String next() throws IOException {
            while (!tokenizer.hasMoreTokens()) {
                tokenizer = new StringTokenizer(reader.readLine());
            }
            return tokenizer.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }
    }
}
