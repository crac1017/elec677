import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    static boolean isDone = false;
    static int lastCap;
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = in.nextInt();
        for(int test = 0; test<testCases; test++){
            char [] a = in.next().toCharArray();
            char [] b = in.next().toCharArray();
            findLastCap(a);
            myCompare(a,b,0,0);
            if(isDone) System.out.println("YES");
            else System.out.println("NO");
            isDone = false;
        }
    }
    public static void myCompare(char [] a, char [] b, int aIndex, int bIndex){
        if(isDone) return;
        int aLen = a.length, bLen = b.length;
        if(bIndex == bLen){
            if(aIndex<=lastCap) return; 
            isDone = true;
        }else{
            if(aIndex == aLen) return;
            char c1 = a[aIndex];
            char c2 = Character.toUpperCase(c1);
            if(c2 == b[bIndex]){
                myCompare(a,b,aIndex+1, bIndex+1);
                if(c1>='a' && c1<='z') myCompare(a,b,aIndex+1, bIndex);
            }else{
                if(c1>='A' && c1<='Z') return;
                else myCompare(a,b, aIndex+1, bIndex);
            }
        }
    }
    public static void findLastCap(char [] a){
        lastCap = a.length-1;
        while(lastCap>=0){
            if(a[lastCap]>='A' && a[lastCap]<='Z') break;
            lastCap--;
        }
    }
    
}
