import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    private static final String YES = "YES";
    private static final String NO = "NO";
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        
        final int query = scanner.nextInt();
        scanner.nextLine(); //skip new line
        for (int q = 0; q < query; q++) {
            
            String a = scanner.nextLine();
            String b = scanner.nextLine();
            
            System.out.println(detect(a, b) ? YES : NO);
            
        }
    }
    private static boolean detect(String a, String b) {
        char[] ac = a.toCharArray();
        char[] bc = b.toCharArray();
        int[] bInA = new int[bc.length];
        for (int j = 0; j < bc.length; j++) {
            bInA[j] = -1;
        } 

        int bIndex = 0;
        for (int i = 0; i < ac.length; i++) {
            if (Character.isUpperCase(ac[i])) {
                boolean found = false;
                for (int j = bIndex; j < bc.length; j++) {
                    if (bc[j] == ac[i]) {
                        bIndex = j + 1;
                        bInA[j] = i;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    //System.out.println("no 1 " + "i " + i);
                    return false;
                }
            }
        }
        
        ac = a.toUpperCase().toCharArray();
        int aIndex = 0;
        int prevJ = -1;
        for (int j = 0; j < bc.length; j++) {
            if (bInA[j] == -1) {
                continue;
            }
            
            for (int jj = prevJ + 1; jj < j; jj++) {
                int startI = prevJ == -1 ? 0 : bInA[prevJ] + 1;
                
                boolean found = false;
                for (int i = startI; i < bInA[j]; i++) {
                    if (ac[i] == bc[jj]) {
                        prevJ = jj;
                        bInA[jj] = i;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    return false;
                }
            }
        }
        // take care the remaining
        for (int j = prevJ + 1; j < bc.length; j++) {
            int startI = prevJ == -1 ? 0 : bInA[prevJ] + 1;
            boolean found = false;
            for (int i = startI; i < ac.length; i++) {
                if (ac[i] == bc[j]) {
                    prevJ = j;
                    bInA[j] = i;
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        return true;
    }
}
