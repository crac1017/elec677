import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);

        int q = sc.nextInt();
        for(int i=0; i<q; i++) {

            String base = sc.next();
            String query = sc.next();

            String regex = createRegex(query);
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(base);
            if (m.matches()) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }
        }

    }

    private static String createRegex(String query) {
        StringBuilder regex = new StringBuilder();

        char[] characters = query.toCharArray();
        regex.append("^[a-z]*?");
        for (char c: characters) {
            regex.append("(");
            regex.append(c).append("|");
            regex.append(String.valueOf(c).toLowerCase());
            regex.append(")");

            regex.append("[a-z]*?");
        }
        regex.append("$");

//        System.out.println(regex.toString());

        return regex.toString();

    }
}
