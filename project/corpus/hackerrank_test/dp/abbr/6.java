import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int cases = in.nextInt();
        in.nextLine();
        
        for(int i = 0; i < cases; i++){
            String x = in.nextLine();
            char a[] = x.toCharArray();
            char c[] = x.toUpperCase().toCharArray();
            char b[] = in.nextLine().toCharArray();
            
            int k = 0;
            int cn = 0, dn = 0;
            boolean flag = true;
            
            for(char ak : a){
                if(Character.isUpperCase(ak)){
                    cn++;
                }
            }
            for(char bk : b){
                if(Character.isUpperCase(bk)){
                    dn++;
                }
            }
            
            for(int j = 0; j < b.length; j++){
                for(; k < a.length; k++){
                    if(Character.isUpperCase(b[j])){
                        if(b[j] == c[k] || b[j] == a[k]) break;
                    }
                    else{
                        if(b[j] == a[k]) break;
                    }
                }
                if(k == a.length){
                    flag = false;
                    break;
                }
            }
            System.out.println((flag && (cn <= dn)) ? "YES" : "NO");
        }
    }
}
