import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
	static char[] a, b;
	static Boolean[][] dp;

	static boolean go(int i, int j) {
		if (i == a.length) {
			if (j == b.length)
				return true;
			return false;
		}
		if (dp[i][j] != null)
			return dp[i][j];
		if (Character.isUpperCase(a[i])) {
			if (j < b.length && a[i] == b[j]) {
				return dp[i][j] = go(i + 1, j + 1);
			}
			return dp[i][j] = false;
		} else {
			boolean can = go(i + 1, j);
			if (j < b.length && Character.toUpperCase(a[i]) == b[j]) {
				can |= go(i + 1, j + 1);
			}
			return dp[i][j] = can;
		}

	}

	public static void main(String[] args) {
		InputReader r = new InputReader(System.in);
		int n = r.nextInt();
		while (n-- > 0) {
			a = r.next().toCharArray();
			b = r.next().toCharArray();
			dp = new Boolean[a.length + 5][b.length + 5];
			System.out.println(go(0, 0) ? "YES" : "NO");
		}

	}

	static class InputReader {
		private BufferedReader reader;
		private StringTokenizer tokenizer;

		public InputReader(InputStream stream) {
			reader = new BufferedReader(new InputStreamReader(stream));
			tokenizer = null;
		}

		public InputReader(FileReader stream) {
			reader = new BufferedReader(stream);
			tokenizer = null;
		}

		public String nextLine() {
			try {
				return reader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}

		public String next() {
			while (tokenizer == null || !tokenizer.hasMoreTokens()) {
				try {
					tokenizer = new StringTokenizer(reader.readLine());
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			return tokenizer.nextToken();
		}

		public int nextInt() {
			return Integer.parseInt(next());
		}

		public long nextLong() {
			return Long.parseLong(next());
		}

		public double nextDouble() {
			return Double.parseDouble(next());
		}
	}
}
