

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Solution {
	public static void main(String[] a) {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		int numbers = 0;
		try {
			line = br.readLine();
			numbers = Integer.parseInt(line);
			line = br.readLine();
			String[] input2 = line.split(" ");
			Integer[] listArray = new Integer[numbers];
			for (int i = 0; i < numbers; i++) {
				listArray[i] = Integer.parseInt(input2[i]);
			}
			insert(listArray);

		} catch (IOException e) {
			e.printStackTrace();
			return;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return;
		}
	}

	private static void insert(Integer[] listArray) {
		int key = 0;
		for (int i = 1; i < listArray.length; i++) {
			key = listArray[i];
			int j = i - 1;
			int k = i;
			while (j >= 0 && key < listArray[j]) {
				listArray[k] = listArray[j];
				j--;
				k--;
			}
			listArray[j + 1] = key;

				for (Integer integer : listArray) {
					System.out.print(integer + " ");
				}
				System.out.println("");
		}
	}
}

 
