

/* Head ends here */
import java.util.*;
public class Solution {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           for(int c=2;c<n+1;c++){
               int[] copy= Arrays.copyOfRange( ar,0, c);
               
               insertionSort(copy);
               for(int f=0;f<copy.length;f++)
                   ar[f]=copy[f];
               printArray(ar);
           }
    }
        
    static void insertionSort(int[] ar) {
          int tmp=ar[ar.length-1];
          for(int i=ar.length-2;i>=0;i--){
            if(ar[i]>tmp){
                ar[i+1]=ar[i];
                if(i==0)
                    ar[i]=tmp;
               // printArray(ar);
            }
            else{
                ar[i+1]=tmp;
               // printArray(ar);
             return;
            }
          }
                    
    } 
       static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }}
    
