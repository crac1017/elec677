

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void insertionSort(int[] ar) {
              for (int i=1; i<ar.length; i++){
                  int tmp = ar[i];
                  int j = i-1;
                  for (; j>=0; j--)
                      if (ar[j]>tmp)
                          ar[j+1]=ar[j];
                      else 
                          break;
                  ar[j+1] = tmp;
                  
                   printArray(ar);  
              }
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
