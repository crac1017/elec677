

/* Head ends here */
import java.util.*;
public class Solution {
    
    static void orderElementInArray( int [] ar, int elementPosition) {
        // there is a value at elementPosition in the wrong position
        // let's iterate to find the right position until we reach position 0. If no right position is found then 0 will be the right position
        int v = ar[ elementPosition ];
        // System.out.println("v is " + ar[ elementPosition ]);
        for( int i = elementPosition; i >= 0; i--) {
            if( i == 0 || v > ar[i-1] ) {
                // assigning the right position to v
                ar[i] = v;
                // printing array
                //printArray( ar );
                break;
             } else {
                // switching
                ar[ i ] = ar[ i - 1];
                // while switching printing array copy                
                //System.out.println("printing array copy " );
                //printArray( ar );
             }
         }
    }
       
    static void insertionSort(int[] ar) {
        // searching for elements in the wrong position
        for( int i = 0; i < ar.length - 1; i ++ ) {
            if( ar[ i + 1 ] < ar [ i ] ) {
                // found a value ar[i + 1] in the wrong position i + 1
                //System.out.println("found a value " + ar[i + 1] + " in the wrong position " + (i + 1) + "");
                orderElementInArray( ar, i + 1 );
            }
            printArray( ar );
        }
                    
     }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
