

/* Head ends here */
import java.util.*;
public class Solution {
       
       static void insertionSort(int[] ar)
       {
           int length = ar.length;
           for(int i = 1; i < length; i++)
           {
               if(ar[i] > ar[i-1])
               {
               }
               else
               {
                   int n = ar[i];
                   for(int j = i; j >= 0; j--)
                   {
                       if(j != 0)
                       {
                           ar[j] = ar[j-1];
                           if(ar[j] < n)
                           {
                               ar[j] = n;
                               break;
                           }
                       }
                       else
                       {
                           ar[j] = n;
                       }
                   }
               }
               printArray(ar);
            }
       }
 

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
