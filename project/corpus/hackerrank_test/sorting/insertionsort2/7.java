

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void insertionSort(int[] ar) {
              int temp=0;    
        	 
              for(int i=1;i<ar.length;i++){
            	  
            	  if(ar[i]>ar[i-1])
            		 printArray(ar);
            	  
            	  else
            	  {
            		  for(int j=0;j<i;j++){
            			  if(ar[i]<ar[j])
            			  {
            				  temp=ar[i];
            				  ar[i]=ar[j];
            				  ar[j]=temp;
            			  }
            		  }
            		  printArray(ar);
            	  }
            	  
              }
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
