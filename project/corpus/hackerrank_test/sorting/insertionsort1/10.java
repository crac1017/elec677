

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void insertionSort(int[] ar) {
              int pos = ar.length-1;
              int cur = ar[pos];
              while(cur < ar[pos-1]) {
                  ar[pos] = ar[pos-1];
                  pos--;
                  for(int i = 0; i < ar.length; i++)
                     System.out.print(ar[i] + " ");
                  System.out.println();
                  if(pos == 0) break;
              }
              ar[pos] = cur;
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
           for(int i = 0; i < ar.length; i++)
               System.out.print(ar[i] + " ");
       }    
   }
