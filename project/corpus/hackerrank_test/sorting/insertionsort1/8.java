

/* Head ends here */
import java.util.*;
public class Solution {
    static void insertionSort(int[] ar) {
        int V = ar[ar.length - 1];
        int i = ar.length - 1;

        for (; i > 0; i--) {
            if (ar[i - 1] < V) {
                break;
            }
            ar[i] = ar[i - 1];
            printArray(ar);
        }
        ar[i] = V;
        printArray(ar);
    }

    static void printArray(int[] ar) {
        for (int n : ar) {
            System.out.print(n + " ");
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];
        for (int i = 0; i < n; i++) {
            ar[i] = in.nextInt();
        }
        insertionSort(ar);
    }  
   }
