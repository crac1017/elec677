import java.util.*;

public class Solution {
    
    public static void main(String... args)
    {
        Scanner sc = new Scanner(System.in);
        
        int N = sc.nextInt();
        int[] arr = new int[N];
        
        for(int i = 0; i<N; i++)
        {
            arr[i] = sc.nextInt();
        }
        
        insertionSort(N-1, arr);
        
        
        sc.close();
    }
       
    static void insertionSort(int pos, int[] ar)
    {
        if( ar.length > 0 )
        {
            int el = ar[ pos-- ];
            
            while( pos>=0 && ar[pos] > el ){
                ar[pos + 1] = ar[pos];
                print(ar);
                pos--;
            }
            ar[pos+1] = el;
            print(ar);
        }
    }
    
    static void print(int... ar){
        int s = ar.length;
        for(int a : ar){
            System.out.print(a);
            if(--s > 0){
                System.out.print(" ");
            }
        }
        System.out.println();
    }
}
