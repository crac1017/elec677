

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void insertionSort(int[] ar) {
                 int element = ar[ar.length-1];
                 int j =ar.length-1;
              while(j>0 && element<ar[j-1]){
                j--;
              }
 
              int swapIndex = j;
              j = ar.length-1;
              while(j>swapIndex){
              ar[j] = ar[j-1];
                  j--;
                  for(int k=0;k<ar.length;k++){
                  System.out.print(" " + ar[k]);
                  }
                  System.out.println();
              }
              ar[swapIndex] = element;
              for(int k=0;k<ar.length;k++){
                  System.out.print("" + ar[k]);
                  }
              
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
