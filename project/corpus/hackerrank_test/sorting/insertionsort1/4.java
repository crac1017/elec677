

/* Head ends here */
import java.util.*;
public class Solution {
       
static void insertionSort(int[] ar) {
              if(ar.length<2) return;
                    int last = ar[ar.length-1], i=ar.length-1;
              while(i>0 && ar[--i]>last)
              {
                  ar[i+1]=ar[i];
                  printArray(ar);
                  //for(int j:ar) System.out.print(j+" ");
                  //System.out.println();
              }
              if(i==0 && ar[0]>last)
                  ar[i--]=ar[0];
              ar[i+1]=last;
    printArray(ar);          
    //for(int j:ar) System.out.print(j+" ");
              //
       }
/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
