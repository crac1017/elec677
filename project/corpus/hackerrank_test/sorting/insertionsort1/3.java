

/* Head ends here */
import java.util.*;
public class Solution {
       
          static void insertionSort(int[] ar) {
              int r = ar.length-1;
              int num = ar[r];
              r--;
              while(r>=0 && ar[r]>num){
                  ar[r+1]=ar[r];
                  r--;                   
                  printArray(ar);
              }
              ar[r+1]=num;
              printArray(ar);
              
                    
       }   

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
