

/* Head ends here */
import java.util.*;
public class Solution 
{
       
       static void insertionSort(int[] ar)
       {
           int length = ar.length;
           List<Integer> nums = new ArrayList<Integer>();
           for(int i =0; i < length-1; i++)
           {
               nums.add(ar[i]);
           }
           int newNum = ar[length-1];
           for(int i = length-1; i >= 0; i--)
           {
               if(i != 0)
               {
                   ar[i] = ar[i-1];
                   if(ar[i] < newNum)
                   {
                       ar[i] = newNum;
                       printArray(ar);
                       break;
                   }
               }
               else
               {
                   ar[i] = newNum;
               }
               printArray(ar);
           }
       }

/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
