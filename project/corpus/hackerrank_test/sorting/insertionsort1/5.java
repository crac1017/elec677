

/* Head ends here */
import java.util.*;
public class Solution {
       
    static void insertionSort(int[] ar) {
              int v = ar[ ar.length - 1 ];
              for( int i = ar.length; i > 0; i--) {
                  if( i == 1 || v > ar[i-2] ) {
                      // assigning the right position to v
                      ar[i-1] = v;
                      // printing array
                      printArray( ar );
                      break;
                  } else {
                      // switching
                      ar[ i-1 ] = ar[ i - 2];
                      // printing array
                      printArray( ar ); 
                  }
              }
       }   
/* Tail starts here */
 
 static void printArray(int[] ar) {
         for(int n: ar){
            System.out.print(n+" ");
         }
           System.out.println("");
      }
       
      public static void main(String[] args) {
           Scanner in = new Scanner(System.in);
           int n = in.nextInt();
           int[] ar = new int[n];
           for(int i=0;i<n;i++){
              ar[i]=in.nextInt(); 
           }
           insertionSort(ar);
       }    
   }
