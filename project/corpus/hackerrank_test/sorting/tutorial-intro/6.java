import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int V = sc.nextInt();
        int N = sc.nextInt();
        int val = 0;
        
        for(int i=0;i<N;++i)
        {
            val = sc.nextInt();
            if(val==V)
            {
                System.out.println(i);
                break;
            }
        }
            
    }
}
