import java.io.*;
import java.util.Scanner;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws Exception{
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        int V = Integer.parseInt(sc.readLine());
        int n =Integer.parseInt(sc.readLine());
        String ar[]= sc.readLine().split(" ");
        int arr[]=new int[n];
        int j=0;
        for(int i =0; i<n; i++)
        {
            arr[i]=Integer.parseInt(ar[i]);
            if(arr[i]==V)
                j=i;
        }
        System.out.println(j);
    }
}
