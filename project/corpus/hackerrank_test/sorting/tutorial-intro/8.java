import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int val = Integer.parseInt(scan.nextLine());
        int size = Integer.parseInt(scan.nextLine());
        
        String[] string_array = scan.nextLine().split(" ");
        
        for(int i = 0; i < size; i++) {
            if (Integer.parseInt(string_array[i])==val) {System.out.println(i);}
        }
        
    }
}
