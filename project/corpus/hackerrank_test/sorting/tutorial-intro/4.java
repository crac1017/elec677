import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        try{
            BufferedReader buf=new BufferedReader(new InputStreamReader(System.in));
            int V=Integer.parseInt(buf.readLine());
            int N=Integer.parseInt(buf.readLine());
            int[] arr=new int[N];
            String[] str=buf.readLine().split(" ");
            for(int i=0;i<N;i++){
               arr[i]=Integer.parseInt(str[i]);
            }
            int ans=-1;
            for(int i=0;i<N;i++){
                if(arr[i]==V){
                    ans=i;
                    break;
                }                    
            }
            if(ans!=-1)
                System.out.println(ans);
        }catch(Exception e){}
    }
}
