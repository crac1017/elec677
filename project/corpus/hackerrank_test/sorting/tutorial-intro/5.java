import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int V = in.nextInt();
        int N = in.nextInt();
        for(int i = 0; i < N; i++) {
            if(in.nextInt() == V) {
                System.out.println(i);
                break;
            }
        }
    }
}
