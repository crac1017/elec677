import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    public static class Edge {
        int v;
        int weight;
        
        public Edge(int v, int weight) {
            this.v = v;
            this.weight = weight;
        }
        
        public boolean equals(Object o) {
            if (this == o)            return true;
            if (!(o instanceof Edge)) return false;
            Edge e = (Edge) o;
            if (e.v == this. v)       return true;
            
            return false;
        }
        
        public int hashCode() {
            return v;
        }
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int tests = in.nextInt();
        
        for (int test = 0; test < tests; test++) {
            int n = in.nextInt();
            int m = in.nextInt();
            
            ArrayList<Edge>[] adj = new ArrayList[n+1];
            for (int i = 0; i <= n; i++) adj[i] = new ArrayList<Edge>();
            
            for (int i = 0; i < m; i++) {
                int u = in.nextInt(), v = in.nextInt(), r = in.nextInt();
                adj[u].add(new Edge(v, r));
                adj[v].add(new Edge(u, r));
            }
            
            Dijkstra dijkstra = new Dijkstra(adj, in.nextInt());
            dijkstra.printDistances();
        }
    }
    
    public static class Dijkstra {
        
        int[] distance;
        boolean[] marked;
        ArrayList<Edge>[] adj;
        PriorityQueue<Edge> pq;
        int s;
        
        public Dijkstra(ArrayList<Edge>[] adj, int s) {
            int n = adj.length;
            this.marked = new boolean[n];
            this.distance = new int[n];
            this.adj = adj;
            for (int i = 1; i < n; i++) this.distance[i] = -1;
            Comparator<Edge> edgeComp = new Comparator<Edge>() {
                public int compare(Edge i, Edge j) {
                    return i.weight - j.weight;
                }
            };
            pq = new PriorityQueue<Edge>(n, edgeComp);
            this.s = s;
        }
        
        public void printDistances() {
            this.distance[this.s] = 0;
            this.pq.offer(new Edge(this.s, this.distance[this.s]));
            
            while (this.pq.size() > 0) {
                int u = this.pq.poll().v;
                this.marked[u] = true;
                
                for (Edge e : this.adj[u]) {
                    int v = e.v;
                    if (!this.marked[v]) {
                        if (this.distance[v] == -1) {
                            this.distance[v] = this.distance[u] + e.weight;
                            this.pq.offer(new Edge(v, this.distance[v]));
                        }
                        else if (this.distance[u] + e.weight < this.distance[v]) {
                            this.pq.remove(new Edge(v, this.distance[v]));
                            this.distance[v] = this.distance[u] + e.weight;
                            this.pq.offer(new Edge(v, this.distance[v]));
                        }
                    }
                }
            }
            
            for (int i = 1; i < this.distance.length; i++) {
                if (this.distance[i] != 0) System.out.print(this.distance[i] + " ");
            }
            System.out.println();
        }
    }
}
