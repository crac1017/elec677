import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

 class ShortestReach2 {

    public static void main(String args[]) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine());
        while(T-->0) {
            String input[] = br.readLine().split(" ");
            int N = Integer.parseInt(input[0]);
            int E = Integer.parseInt(input[1]);
            ArrayList<Node> nodes = new ArrayList<Node>();
            long adjMatrix[][] = new long[N + 1][N + 1];

            for (int i = 1; i <= N; i++) {
                nodes.add(new Node(i));
            }

            for(int i=1; i<=N; i++) {
                for(int j=1; j<=N; j++) {
                    adjMatrix[i][j] = Integer.MAX_VALUE;
                }
            }

            for (int i = 1; i <= E; i++) {
                input = br.readLine().split(" ");
                int A = Integer.parseInt(input[0]);
                int B = Integer.parseInt(input[1]);
                long W = Long.parseLong(input[2]);
                adjMatrix[A][B] = Math.min(adjMatrix[A][B], W);
                adjMatrix[B][A] = Math.min(adjMatrix[B][A], W);
            }

            int S = Integer.parseInt(br.readLine());

            nodes.get(S-1).setDistance(0);
            PriorityQueue<Node> queue = new PriorityQueue<Node>();
            for (Node n : nodes) {
                queue.add(n);
            }

            while (!queue.isEmpty()) {
                Node u = queue.poll();
                int uId = u.getId();
                for (int i = 1; i <= N; i++) {
                    if (!(adjMatrix[uId][i] == Integer.MAX_VALUE)) {
                        Node v = nodes.get(i - 1);
                        long temp = adjMatrix[uId][i] + u.getDistance();
                        if (v.getDistance() > temp) {
                            queue.remove((Node) v);
                            v.setDistance(temp);
                            v.setParent(u);
                            queue.add(v);
                        }
                    }
                }
            }

            for (int i = 0; i < nodes.size(); i++) {
                if (i == S - 1) continue;
                if (nodes.get(i).getDistance() == Integer.MAX_VALUE) {
                    System.out.print("-1 ");
                } else {
                    System.out.print(nodes.get(i).getDistance() + " ");
                }
            }
            System.out.println();
        }
    }

}
class Node implements Comparable<Node> {

    int nodeId;
    Node parent;
    long distance;

    public Node(int nodeId) {
        this.nodeId = nodeId;
        parent = null;
        distance = Integer.MAX_VALUE;
    }

    public int getId() {
        return this.nodeId;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node getParent() {
        return this.parent;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public long getDistance() {
        return this.distance;
    }

    @Override
    public int compareTo(Node node) {
        return (this.distance - node.distance < 0) ? -1 : 1;
    }
}
