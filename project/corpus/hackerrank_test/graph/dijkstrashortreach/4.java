import static java.lang.System.out;

import java.util.Scanner;
import java.util.Vector;


//https://www.hackerrank.com/challenges/dijkstrashortreach
public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		int nNodes, nEdges;
		for (int i = 0; i < t; i++) {
			nNodes = in.nextInt();
			nEdges = in.nextInt();
			mainTest(in, nNodes, nEdges);
		}
		in.close();
	}

	static void mainTest(Scanner in, int nNodes, int nEdges) {
		Graph g = new Graph(nNodes);

		int n1, n2, dist;
		for (int i = 0; i < nEdges; i++) {
			n1 = in.nextInt();
			n2 = in.nextInt();
			dist = in.nextInt();
			g.addEdge(n1, n2, dist);
		}

		int s = in.nextInt();

		int[] shortest = g.dijkstra(s);

		boolean leadingSpace = false;
		for (int i = 1; i <= nNodes; i++) {
			if (s == i) {
				continue;
			}

			// leading space just once
			if (leadingSpace) out.print(" ");
			leadingSpace = true;

			out.print(shortest[i - 1]);
		}
		out.println();
	}

	static class Graph {

		int[][] adjancency;

		public Graph(int n) {
			adjancency = new int[n][n];
			for (int i = 0; i < n; i++) {
				for (int j = i; j < n; j++) {
					adjancency[i][j] = adjancency[j][i] = -1;
				}
			}
		}

		public void addEdge(int i, int j, int dist) {
			i--; j--;
			if (adjancency[i][j] >= 0 && adjancency[i][j] <= dist) {
				return; // do nothing
			}
			adjancency[i][j] = adjancency[j][i] = dist;
		}

		public int[] dijkstra(int i) {
			i--;
			int n = adjancency.length;

			boolean[] visited = new boolean[n];
			int[] shortest = new int[n];
			for (int k = 0; k < n; k++) {
				shortest[k] = -1;
			}
			shortest[i] = 0;
			Vector<Integer> set = new Vector<Integer>();

			_dijkstra(i, set, visited, shortest);

			return shortest;
		}

		private void _dijkstra(int i, Vector<Integer> set, boolean[] visited, int[] shortest) {
			visited[i] = true;

			int d, d1, d2;
			for (int k = 0; k < adjancency.length; k++) {
				if (k == i) {
					continue; // same point
				}
				d1 = adjancency[i][k];
				if (d1 < 0) {
					// no adjancency
					continue;
				}
				if (visited[k]) {
					continue; // don't go back
				}

				if (!set.contains(k)) {
					set.add(k);
				}

				d2 = shortest[i];
				if (d2 < 0) {
					d = -1;
				}
				else {
					d = d1 + d2;
				}

				if (d < shortest[k] || shortest[k] < 0) {
					shortest[k] = d;
				}
			}

			if (set.size() == 0) {
				return;
			}

			int minPos = 0;
			for (int s = 0; s < set.size(); s++) {
				if (shortest[set.elementAt(s)] < shortest[set.elementAt(minPos)] || shortest[set.elementAt(minPos)] < 0) {
					minPos = s;
				}
			}
			int target = set.elementAt(minPos);
			set.removeElementAt(minPos);
			_dijkstra(target, set, visited, shortest);
		}

	}

}
