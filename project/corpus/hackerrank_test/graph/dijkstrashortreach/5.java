import java.util.Scanner;

public class Solution {
	static int[][] inArr = null;
	static boolean[] isVisited = null;
	static int[] distance = null;
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int noOfTestCases = scan.nextInt();

		while(noOfTestCases>0)
		{
			int nodes = scan.nextInt();
			inArr = new int[nodes][nodes]; 
			isVisited = new boolean[nodes];
			distance = new int[nodes];
			/*for(int i=0;i<nodes;i++)
			{
				for(int j=0;j<nodes;j++)
				{
					inArr[i][j] = 0;
				}
			}*/
			int edges = scan.nextInt();
			while(edges>0)
			{
				int rowNum = scan.nextInt();
				int colNum = scan.nextInt();
				int weight = scan.nextInt();
				if(inArr[rowNum-1][colNum-1]>0)
				{
					if(weight<inArr[rowNum-1][colNum-1])
					{
					inArr[rowNum-1][colNum-1] = weight;
					inArr[colNum-1][rowNum-1] = weight;
					}
				}
				else
				{
					inArr[rowNum-1][colNum-1] = weight;
					inArr[colNum-1][rowNum-1] = weight;
				}
				edges--;
			}

			for(int i=0;i<nodes;i++)
			{
				distance[i] = Integer.MAX_VALUE;
			}
			int startNode = scan.nextInt();
			distance[startNode-1] = 0;

			for(int i=0;i<(nodes-1);i++)
			{
				int u = minDistance(distance,isVisited);
				isVisited[u] = true;

				for(int j=0;j<nodes;j++)
				{
					if(!isVisited[j] && inArr[u][j]>0 && distance[u]!=Integer.MAX_VALUE &&((distance[u]+inArr[u][j])<distance[j]))
					{
						distance[j] = distance[u]+inArr[u][j];
					}
				}

			}
			String result = "";
			for(int i=0;i<distance.length;i++)
			{
				if(i!=(startNode-1))
				{
					if(distance[i]<Integer.MAX_VALUE)
					{
						result = result+" "+distance[i];
					}
					else
					{
						result = result+" "+-1;
					}
				}

			}
			System.out.println(result.trim());
			noOfTestCases--;
		}
	}
	private static int minDistance(int[] distance2, boolean[] isVisited2) {

		int min = Integer.MAX_VALUE;
		int min_index = 0;
		for(int i=0;i<distance2.length;i++)
		{
			if(distance2[i]<=min && !isVisited2[i])
			{
				min = distance2[i];
				min_index = i;
			}
		}
		return min_index;
	}





}
