import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;



public class Solution {
    
   
     
    private static HashMap<Integer, Node> graph;
    private static LinkedList<Node> unVisited;    
    private static Node searchPoint;
   
    private static class Node implements Comparable<Node>{
        private int name;
        private ArrayList<Edge> myChildren;
        private int distanceCost;

        public Node(int name) {
            this.name = name;
            myChildren = new ArrayList<Edge>();
            distanceCost = Integer.MAX_VALUE;
        }

        public int getName() {
            return name;
        }
        
        public ArrayList<Edge> getChildren(){
           return myChildren; 
        }
  
        public void createEdge(Edge e) {
            //if (!myChildren.contains(e)) {
                myChildren.add(e);              
           // }
        }

      
        @Override
        public boolean equals(Object object) {
            if (object instanceof Node && ((Node) object).getName() == this.name) {
                return true;
            } else {
                return false;
            }
        }
        
        @Override 
        public int hashCode(){            
           return (name+1)*37;
        }


        public ArrayList<Edge> getEdges() {
            return myChildren;
        }
        
        public int getCostFor(Node destination){
            for(Edge e : myChildren){
                if(e.getDestination().equals(destination)){
                    return e.getCost();
                }
            }
          throw new IllegalArgumentException("No such child");   
        }
        
        public int getDistanceCost(){
            return distanceCost;
        }
        
        public void setDistanceCost(int distance){
            this.distanceCost = distance;
        }
        
        
        @Override        
        public int compareTo(Node n){
            if(n.getDistanceCost() == this.getDistanceCost()){
                return 0;
            }
            if(this.getDistanceCost()< n.getDistanceCost()){
                return -1;
            }
            return 1;
        }
        
    }
    
    private static class Edge{
        private int cost;
        private Node destination;
     
        
        public Edge(Node destination, int value){
            this.destination = destination;
            this.cost=value;
        }
        
        @Override
        public boolean equals(Object object) {
            if (object instanceof Edge) {
                if( ((Edge) object).getDestination() == this.destination){
                    return true;
                }else{
                    return false;
                }
            } else {
                return false;
            }
        }
        
        @Override 
        public int hashCode(){            
           return  (((destination.getName()+1)*31) );
        }
        
      
         public Node getDestination(){
            return destination;
        }
        
        public int getCost(){
            return cost;
        }

    }
    
      
    public static void main(String[] args) {
       
        Scanner in = new Scanner(System.in);
        
        int numberOfTests = in.nextInt();
        
        for(int j =0; j < numberOfTests; j++){
            graph = new HashMap<Integer, Node>();
            int nodes = in.nextInt();
            int edges = in.nextInt();
            
            for(int i =1;i<= nodes; i++){
                graph.put(i, new Node(i));
            }
            
            for (int i = 0; i < edges; i++) {
                Integer origin = (Integer)in.nextInt();
                Integer destination = (Integer)in.nextInt();                               
                int value = in.nextInt();               
                graph.get(origin).createEdge(new Edge(graph.get(destination), value));
                graph.get(destination).createEdge(new Edge(graph.get(origin), value));
              
            }
            
            searchPoint = graph.get(in.nextInt());
            unVisited = new LinkedList<Node>();
            unVisited.addAll(graph.values()); 
            djikstraGraph(searchPoint);
            
            for (int k=1; k<= graph.values().size(); k++ ){
                Node n = graph.get(k);
                if(n.getDistanceCost() == Integer.MAX_VALUE){
                    System.out.print("-1 ");
                }else if(n.getDistanceCost() == 0){
                    
                }else{
                    System.out.print(n.getDistanceCost() + " ");
                }
            }
            
            System.out.print("\n");       
        }
        
    }   
    
    public static void djikstraGraph(Node n){
             
        if(n.getName() == (searchPoint.getName())){
            unVisited.remove(n);
            n.setDistanceCost(0);
        }
        
        if(n.getDistanceCost() != Integer.MAX_VALUE){
            for(Edge e : n.getChildren()){
                Node dest = e.getDestination();
                int distanceCost = n.getDistanceCost() + e.getCost();
                if(distanceCost < dest.getDistanceCost()){
                    dest.setDistanceCost(distanceCost);
                }
            }
        }
            
        unVisited.remove(n); 
        
        Collections.sort(unVisited);
        if(unVisited.size()>0){
            Node next = unVisited.getFirst();
            if(next!=null){
                djikstraGraph(next); 
            }else{
               return; 
            }
        }                
    }                      
}
