import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;

public class Solution {
	static InputStream is;
	static PrintWriter out;
	static String INPUT = "";
	
	static void solve()
	{
		for(int T = ni();T >= 1;T--){
			long n = nl(), m = nl();
			long low = 0;
			long high = n+3;
			while(high - low > 1){
				long r = (high + low) / 2;
				long b = (n*n-(n%r)*((n+r-1)/r)*((n+r-1)/r)-(r-n%r)*(n/r)*(n/r))/2;
//				long b = (r-1)*n*n/2/r;
				if(m > b){
					low = r;
				}else{
					high = r;
				}
			}
			out.println(high);
		}
	}
	
	static long nl() {
		try{
			long num = 0;
			boolean minus = false;
			while ((num = is.read()) != -1
					&& !((num >= '0' && num <= '9') || num == '-'))
				;
			if(num == '-'){
				num = 0;
				minus = true;
			}else{
				num -= '0';
			}

			while (true){
				int b = is.read();
				if(b >= '0' && b <= '9'){
					num = num * 10 + (b - '0');
				}else{
					return minus ? -num : num;
				}
			}
		}catch (IOException e){
		}
		return -1;
	}
	
	public static void main(String[] args) throws Exception
	{
		long S = System.currentTimeMillis();
		is = INPUT.isEmpty() ? System.in : new ByteArrayInputStream(INPUT.getBytes());
		out = new PrintWriter(System.out);
		
		solve();
		out.flush();
		long G = System.currentTimeMillis();
		tr(G-S+"ms");
	}
	
	static boolean eof()
	{
		try {
			is.mark(1000);
			int b;
			while((b = is.read()) != -1 && !(b >= 33 && b <= 126));
			is.reset();
			return b == -1;
		} catch (IOException e) {
			return true;
		}
	}
		
	static int ni()
	{
		try {
			int num = 0;
			boolean minus = false;
			while((num = is.read()) != -1 && !((num >= '0' && num <= '9') || num == '-'));
			if(num == '-'){
				num = 0;
				minus = true;
			}else{
				num -= '0';
			}
			
			while(true){
				int b = is.read();
				if(b >= '0' && b <= '9'){
					num = num * 10 + (b - '0');
				}else{
					return minus ? -num : num;
				}
			}
		} catch (IOException e) {
		}
		return -1;
	}
	
	static void tr(Object... o) { if(INPUT.length() != 0)System.out.println(Arrays.deepToString(o)); }
}
