import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in=new Scanner(new InputStreamReader(System.in));
        int tc=in.nextInt();
        while(tc--!=0){
            int n=in.nextInt();
            int e=in.nextInt();
            int k,low = 1,high = n + 1 ;
             while(low + 1 < high){
              int mid = low + (high - low)/2 ;
              k = solve1(n,mid) ;
              if(k < e) low = mid ;
              else high = mid ;
             }
        System.out.println(high);
        }
    }
    static int solve1(int n,int k){
     int g1 = n%k ;
     int g2 = k - g1 ;
     int sz1 = n/k + 1 ;
     int sz2 = n/k ;
     int ret = g1*sz1*g2*sz2 + g1*(g1-1)*sz1*sz1/2 + g2*(g2-1)*sz2*sz2/2 ;
     return ret ; 
    }
}
