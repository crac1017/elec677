//package com.prebeg.interviewstreet.clique;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {
	
	private static int turan(int n, int k)
	{
		int g1 = n%k;
		int g2 = k - g1;
		int sz1 = n/k + 1;
		int sz2 = n/k;
		int ret = g1*sz1*g2*sz2 + g1*(g1-1)*sz1*sz1/2 + g2*(g2-1)*sz2*sz2/2;
		return ret;
	}
	
	public static int solve(int n, int m)
	{
		int k;
		int low = 1;
		int high = n+1;
		
		while (low + 1 < high)
		{
			int mid = low + (high - low)/2;
			k = turan(n, mid);
			if (k < m) low = mid;
			else high = mid;
		}
		return high;
	}

	static InputStream in = System.in;
	static PrintStream out = System.out;
	
	static int N;
	static int M;
	
	static int T;
		
	public static void main(String[] args) throws Exception {
		
		if (args.length != 0) {
			in = new FileInputStream(new File(args[0]));
			out = new PrintStream(new File(args[1]));
		}
			
		Scanner s = new Scanner(in);
		            
		T = s.nextInt();

        for (int i = 0; i < T; i++) 
        {
        	N = s.nextInt();
        	M = s.nextInt();
            int r = solve(N,M);
            out.println(r);
        }
	}

}
