import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    private static int minClique(int n, int m) {
        int count = 1;
        int edges = 0;
        while (edges < m) {
            ++count;
            int p = n / count;
            int k = n % count;
            int l = count - k;
            if (p == 0) {
                break;
            }
            edges  = (p + 1) * l * p * k + (p + 1) * (k - 1) * (p + 1) * k;
            edges += p * (l - 1) * p * l + p * k * (p + 1) * l;
            edges /= 2;
        }
        
        return count;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner;
        if (args.length == 0) {
            scanner = new Scanner(new BufferedInputStream(System.in));
        }
        else {
            scanner = new Scanner(new BufferedInputStream(new FileInputStream(args[0])));
        }
        int t = scanner.nextInt();
        for (int i = 0; i < t; ++i) {
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            System.out.println(minClique(n, m));
        }
    }
}
