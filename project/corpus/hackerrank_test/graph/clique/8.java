import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

	public static void main(String [] args ) {
		try{
			String str;			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			BufferedOutputStream bos = new BufferedOutputStream(System.out);
			String eol = System.getProperty("line.separator");
			byte [] eolb = eol.getBytes();
			str = br.readLine();
			int test = Integer.parseInt(str);
			for(int i = 0 ; i < test ; i++) {
				str  = br.readLine();
				int blank = str.indexOf( " ");
				long n = Integer.parseInt(str.substring(0,blank));
				long m = Integer.parseInt(str.substring(blank+1));
				long ans = solve(n,m);
				bos.write(new Long(ans).toString().getBytes());
				bos.write(eolb);
			}
			bos.flush();
		}  catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	public static long solve(long a , long b) {
		long ans = 1;
		for(long i = 1 ; i <= a ; i++) {
			long temp =  getMax(a,i) ;
			if(temp>=b) {
				ans = i;
				break;
			}
		}
		return ans;
	}
	
	public static long getMax(long a,long b) {
		long q = a/b;
		long r = a%b;
		long num1 = b - r;
		long ans = 0;
		long num2 = r;
		ans += (num1 * (num1-1)*q*q)/2l;
		ans += (num2 * (num2-1)*(q+1)*(q+1))/2l;
		ans += (num1*num2*q*(q+1));
		return ans;
	}
}
