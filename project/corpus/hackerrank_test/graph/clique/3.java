import java.io.PrintWriter;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
        PrintWriter out = new PrintWriter(System.out);
        Scanner in = new Scanner(System.in);
        Solution solver = new Solution(in, out);
        solver.run();
        out.close();
        in.close();
    }

    Scanner in;
    PrintWriter out;

    public Solution(Scanner i, PrintWriter o) {
        in = i;
        out = o;
    }

    int calc(int n, int k) {
        int g1 = n % k;
        int g2 = k - g1;
        int sz1 = n / k + 1;
        int sz2 = n / k;
        int ret = g1 * sz1 * g2 * sz2 + g1 * (g1 - 1) * sz1 * sz1 / 2 + g2 * (g2 - 1) * sz2 * sz2 / 2;
        return ret;
    }

    void solve() {
        int n = in.nextInt();
        int e = in.nextInt();
        int k, lo, hi, m;
        lo = 1;
        hi = n + 1;
        while (lo + 1 < hi) {
            m = lo + (hi - lo) / 2;
            k = calc(n, m);
            if (k < e) {
                lo = m;
            } else {
                hi = m;
            }
        }
        out.println(hi);
    }

    void run() {
        int T = in.nextInt();
        for (int i = 0; i < T; i++) {
            solve();
        }
    }
}
