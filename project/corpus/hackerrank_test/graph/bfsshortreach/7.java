import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {

		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();
		for (int i = 0; i < T; i++) {
			int N = in.nextInt();
			int M = in.nextInt();
			ArrayList<Integer>[] graph = (ArrayList<Integer>[]) new ArrayList[N+1];
			for (int j = 0; j < M; j++) {
				int a = in.nextInt();
				int b = in.nextInt();
				if (graph[a]==null) 
					graph[a] = new ArrayList<Integer>();
				if (graph[b]==null) 
					graph[b] = new ArrayList<Integer>();
				graph[a].add(b);
				graph[b].add(a);
			}
			int S = in.nextInt();
			int[] dist = new int[N+1];
			for (int j = 0; j < dist.length; j++) {
				dist[j] = -1;
			}
			Queue<Integer> q = new LinkedList<Integer>();
			q.add(S);
			dist[S] = 0;
			int dis = 0;
			while(!q.isEmpty())
			{
				dis+=6;
				Queue<Integer> s = new LinkedList<Integer>();
				while (!q.isEmpty()) {
					int k = q.poll();
					if (!(graph[k]==null)) {
						for (int j : graph[k]) {
							if (dist[j]==-1) {
								dist[j] = dis;
								s.add(j);
							}
						}
					}
					
				}
				q = s;
				
			}
			String res = "";
			for (int j = 1; j < dist.length; j++) {
				if (j!=S) {
					res+=dist[j]+" ";
				}
			}
			System.out.println(res.trim());
			
		}
	}
	
}
