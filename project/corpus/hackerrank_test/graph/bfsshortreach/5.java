import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution {
	private static class Node {
		public int v;
		public ArrayList<Node> next;
		public int cost;
		public boolean checked;

		public Node(int v) {
			this.v = v;
			this.cost = 0;
			this.checked = false;
			next = new ArrayList<Solution.Node>();
		}
	}

	private static Node[] nodes;

	private static void initalState() {
		for (int i = 0; i < nodes.length; i++) {
			nodes[i].checked = false;
			nodes[i].cost = 0;
		}
	}

	private static int getShortestPath(Node S, Node D) {// S start ,, D//
														// destination
		Queue<Node> queue = new LinkedList<Node>();
		Node father;
		S.checked = true;
		queue.add(S);
		while (!queue.isEmpty()) {
			father = queue.peek();
			for (Node next : father.next) {
				if (!next.checked) {
					next.checked = true;
					queue.add(next);
					next.cost = father.cost + 6;
					if (next.v == D.v) {
						return next.cost;
					}
				}
			}
			queue.remove();
		}
		return -1;
	}

	public static void main(String[] args) throws NumberFormatException,
			Exception {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt(), N, M, x, y, S;
		String[] result = new String[T];
		for (int t = 0; t < T; t++) {
			result[t]="";
			N = sc.nextInt();
			M = sc.nextInt();
			nodes = new Node[N];
			for (int i = 0; i < N; i++) {
				nodes[i] = new Node(i + 1);
			}
			for (int i = 0; i < M; i++) {
				x = sc.nextInt() - 1;
				y = sc.nextInt() - 1;
				if (!nodes[x].next.contains(nodes[y])) {
					nodes[x].next.add(nodes[y]);
				}
				if (!nodes[y].next.contains(nodes[x])) {
					nodes[y].next.add(nodes[x]);
				}
			}
			S = sc.nextInt();
			for (int i = 0; i < N; i++) {
				if (i != S - 1) {
					initalState();
					result[t] += "" + getShortestPath(nodes[S - 1], nodes[i])+" ";
				}
			}
		}
		for (String g : result) {
			System.out.println(g);
		}
	}
}
