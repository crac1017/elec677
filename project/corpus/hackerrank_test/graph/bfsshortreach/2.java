import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String args[])
	{
		readData(args);
	}

	static void readData(String[] inData)
	{
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		
		while (T-- > 0)
		{
			int nodes = sc.nextInt();
			int edges = sc.nextInt();
			
			int[][] data = new int[nodes + 1][nodes + 1];
			
			for (int i = 0; i < edges; i++)
			{
				int row = sc.nextInt();
				int col = sc.nextInt();
				
				data[row][col] = 1;
				data[col][row] = 1;
				
				data[row][0]++;
				data[col][0]++;
			}
			
			int start = sc.nextInt();
			
			int[] paths = new int[nodes + 1];
			boolean[] isVisited = new boolean[nodes + 1];
			
			for (int i = 0; i < paths.length; i++)
			{
				isVisited[i] = false;
				paths[i] = Integer.MAX_VALUE;
			}
			
			shortestPath(data, isVisited, paths, start);
			
			for (int i = 1; i < paths.length; i++)
			{
				if (i != start)
				{
					int ans = (paths[i] == Integer.MAX_VALUE) ? -1 : (paths[i] * 6);
					System.out.print(ans+" ");
				}
			}
			
			System.out.println("");
		}
		
		sc.close();
	}
	
	static void shortestPath(int[][] matrix, boolean[] isVisited, int[] paths, int start)
	{
		LinkedList<Integer> mQueue = new LinkedList<Integer>();
		
		paths[start] = 0;
		mQueue.addLast(start);
		
		
		
		while (mQueue.size() > 0)
		{
			int id = mQueue.pollFirst();
			
			if (matrix[id][0] == 0)
				continue;
			
			for (int i = 1; i < matrix[id].length && isVisited[id] == false; i++)
			{
				if (matrix[id][i] == 0 || isVisited[i] == true)
					continue;
				
				mQueue.addLast(i);
				
				int current = paths[i];
				int calculated = paths[id] + matrix[id][i];
				
				if (calculated < current)
					paths[i] = calculated;
			}
			
			isVisited[id] = true;
		}
	}
}
