import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


class Graph
    {
    int V; //0 o V-1
    LinkedList<Integer>[] adj;
    
    Graph(int V)
        {
        this.V=V;
        adj=(LinkedList<Integer> [] )new LinkedList[V];
        for(int i=0;i<V;i++)
            adj[i]=new LinkedList<Integer>();
    }
    
    void addEdge(int from, int to) //you will have to pass from -1 and to -1
        {
        adj[from].add(to);
        adj[to].add(from); //it is undirected!! //learn this !!!
    }
    
    LinkedList<Integer> adj(int v)
        {
        return adj[v];
    }
    
    void dfs(int source, int w)
        {
        boolean marked[]= new boolean[V];
        int distTo[]=new int[V];
        for(int i=0;i<V;i++) //learn this : distnaces! in bfs dfs!
            distTo[i]=-1; //by default it is -1!!! bravo!
        LinkedList<Integer> queue =  new LinkedList<Integer>();
        queue.addLast(source);
        marked[source]=true;
        distTo[source]=0;
        while(queue.size()>0)
            {
            int curr = queue.removeFirst();
            for(int next: adj[curr])
                {
                if(!marked[next])
                    {
                    marked[next]=true;
                    distTo[next]=distTo[curr]+w;
                    //System.out.print(distTo[next]+" ");
                    queue.addLast(next);
                }
            }
        }
        for(int i=0;i<V;i++)
           if(i!=source)
              System.out.print(distTo[i]+" ");
                
        System.out.println();
    }
}

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scan = new Scanner(System.in);
        int tests = scan.nextInt();
        while(tests-->0)
            {
            int V = scan.nextInt();
            int E = scan.nextInt();
            Graph g = new Graph(V);
            while(E-->0)
                {
                g.addEdge(scan.nextInt()-1,scan.nextInt()-1);
            }
            g.dfs(scan.nextInt()-1,6);
        }
    }
}
