import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    static boolean[] visited;
    static int[] dist;
    static LinkedList<Integer> q = new LinkedList<Integer>();
    static class Graph {
        int n, m;
        HashSet<Integer>[] adj;
        public Graph(int n, int m) {
            this.n = n;
            this.m = m;
            adj = new HashSet[n];
            for (int i = 0; i < n; i ++) {
                adj[i] = new HashSet<Integer>();
            }
        }
        public void addEdge(int i, int j) {
            adj[i].add(new Integer(j));
            adj[j].add(new Integer(i));
        }
    }
    static void bfs(int s, Graph g) {
        visited = new boolean[g.n];
        visited[s] = true;
        dist[s] = 0;
        q.add(new Integer(s));
        while (!q.isEmpty()) {
            int cur = q.poll();
            Iterator<Integer> iter = g.adj[cur].iterator();
            while (iter.hasNext()) {
                int next = iter.next();
                if (!visited[next]) {
                    dist[next] = dist[cur] + 6;
                    visited[next] = true;
                    q.add(next);
                }

            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-- > 0) {
            int n = sc.nextInt();
            int m = sc.nextInt();
            Graph g = new Graph(n, m);
            while (m--  > 0) {
                int i = sc.nextInt() - 1;
                int j = sc.nextInt() - 1;
                g.addEdge(i, j);
            }
            int s = sc.nextInt() - 1;
            visited = new boolean[n];
            dist = new int[n];
            for (int i = 0; i < n; i ++) {
                dist[i] = -1;
            }
            bfs(s, g);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < n; i++) {
                if(i != s) {
                    sb.append(dist[i]).append(" ");
                }
            }
            System.out.println(sb.deleteCharAt(sb.length() - 1));
        }
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    }
}
