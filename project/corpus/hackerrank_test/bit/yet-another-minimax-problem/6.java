import java.io.*;
import java.util.*;

public class Solution {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int cases = 1;//in.nextInt();
        for (int testcase = 0; testcase < cases; testcase++) {
            int n = in.nextInt();
            int[] sides = new int[n];
            for (int i = 0; i < n; i++) {
                sides[i] = in.nextInt();
            }
            minimax(sides);
        }
    }

    public static void minimax(int[] sides) {



        while (true) {
            int maxLength = 0;
            for (int i : sides) {
                maxLength = Math.max(maxLength, Integer.toBinaryString(i).length());
            }
            Set<Integer> maxes = new HashSet();
            Set<Integer> others = new HashSet();
            for (int i : sides) {
                if (Integer.toBinaryString(i).length() == maxLength) {
                    maxes.add(i);
                } else {
                    others.add(i);
                }
            }
            if (maxLength == 1) {
                break;
            }
            if (!others.isEmpty()) {
                break;
            }
//            System.out.println(maxLength);
            int big = 1 << (maxLength-1);
//            System.out.println(big);
            for (int i = 0; i < sides.length; i++) {
                sides[i] = sides[i]^big;
            }

        }

        int maxLength = 0;
        for (int i : sides) {
            maxLength = Math.max(maxLength, Integer.toBinaryString(i).length());
        }
        Set<Integer> maxes = new HashSet();
        Set<Integer> others = new HashSet();
        for (int i : sides) {
            if (Integer.toBinaryString(i).length() == maxLength) {
                maxes.add(i);
            } else {
                others.add(i);
            }
        }

        if (maxLength == 1) {
            for (int i = 0; i < sides.length-1; i ++) {
                if (sides[i] != sides[i+1]) {
                    System.out.println("1");
                    return;
                }
            }
            System.out.println("0");
            return;
        }

//        System.out.println(maxes);
//        System.out.println(others);

        int best = Integer.MAX_VALUE;
        for (int i : maxes) {
            for (int j : others) {
                int res = i ^ j;
                best = Math.min(best, res);
            }
        }
        System.out.println(best);
    }
}
