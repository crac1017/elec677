import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        String[] bin = new String[n];
        int maxLength = 0;
        Set<Integer> setA = new HashSet<Integer>();
        Set<Integer> setB = new HashSet<Integer>();
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
            bin[i] = Integer.toBinaryString(a[i]);
            if (bin[i].length() > maxLength) {
                maxLength = bin[i].length();
            }
        }
        int ind = 0;
        while (ind < maxLength && (setA.isEmpty() || setB.isEmpty())) {
            setA.clear();
            setB.clear();
            for (int i = 0; i < n; i++) {
                while (bin[i].length() < maxLength) {
                    bin[i] = "0" + bin[i];
                }
                if (bin[i].charAt(ind) == '1') {
                    setA.add(a[i]);
                } else {
                    setB.add(a[i]);
                }
            }
            ind++;
        }
        int res = -1;
        int tmp;
        for(int left : setA) {
            for (int right : setB) {
                tmp = left ^ right;
                if (res == -1 || tmp < res) {
                    res = tmp;
                }
            }
        }
        if (res == -1) {
            res = 0;
        }
        System.out.println(res);
    }
}
