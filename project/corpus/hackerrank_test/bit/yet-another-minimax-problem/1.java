import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        Arrays.sort(a);
        
        if (a[0]==a[n-1]) {
            System.out.println(0);
            return;
        }
        
        int min = 0;
        int max = 0;
        
        for (int i = 1; i < 31; i++) {
            if (a[0] >= (1<<i))
                min++;
        }
        
        for (int i = 1; i < 31; i++) {
            if (a[n-1] >= (1<<i))
                max++;
        }
        
        while (min==max&&min!=0) {
            for (int i = 0; i < n; i++) {
                a[i] -= 1<<min;
            }
            min = 0;
            max = 0;
            for (int i = 1; i < 31; i++) {
                if (a[0] >= (1<<i))
                    min++;
            }
            for (int i = 1; i < 31; i++) {
                if (a[n-1] >= (1<<i))
                    max++;
            }
        }
        
        if (max==0) {
            System.out.println(0);
            return;
        }
        
        ArrayList<Integer> l = new ArrayList<Integer>();
        ArrayList<Integer> h = new ArrayList<Integer>();
        
        for (int i = 0; i < n; i++) {
            if (a[i] < (1<<max))
                l.add(a[i]);
            else
                h.add(a[i]);
        }
        
        int result = Integer.MAX_VALUE;
        for (int i : l) {
            for (int j : h) {
                if ((i^j)<result)
                    result = i^j;
                if (result == (1<<max))
                    break;
            }
            if (result == (1<<max))
                break;
        }
        System.out.println(result);
    }
}
