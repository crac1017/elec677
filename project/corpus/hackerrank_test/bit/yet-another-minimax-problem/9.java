import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

	private static long minXor(long[] arr, int idx1_l, int idx1_r, int idx2_l, int idx2_r, long base) {
        if (base == 0) {
            return arr[idx1_l] ^ arr[idx2_l];
        }
		if (idx1_r == idx1_l) {
			long res = Long.MAX_VALUE;
			long a = arr[idx1_r];
			for (int i = idx2_l; i <= idx2_r; i++) {
				res = Math.min(res, a ^ arr[i]);
			}
/*			System.out.print(idx1_l+":"+idx1_r+" "+arr[idx1_l]+" "+arr[idx1_r]+" "+
							 idx2_l+":"+idx2_r+" "+arr[idx2_l]+" "+arr[idx2_r]);
			System.out.println("res01:"+res);*/
			return res;
		}
		if (idx2_r == idx2_l) {
			long res = Long.MAX_VALUE;
			long a = arr[idx2_r];
			for (int i = idx1_l; i<= idx1_r; i++) {
				res = Math.min(res, a ^ arr[i]);
			}
/*			System.out.print(idx1_l+":"+idx1_r+" "+arr[idx1_l]+" "+arr[idx1_r]+" "+
					 idx2_l+":"+idx2_r+" "+arr[idx2_l]+" "+arr[idx2_r]);
			System.out.println("res02:"+res);*/
			return res;
		}
		int idxa = idx1_r+1;
		int idxb = idx2_r+1;

		for (int i = idx1_l; i<=idx1_r; i++) {
			long x = arr[i] & base;
			if (x == base) {
				idxa = i;
				break;
			}
		}


		for (int i = idx2_l; i<=idx2_r; i++) {
			long x = arr[i] & base;
			if (x == base) {
				idxb = i;
				break;
			}
		}

		long res = Long.MAX_VALUE;
		if (idxa - idx1_l >0 && idxb - idx2_l > 0) {
			res = minXor(arr, idx1_l, idxa-1, idx2_l, idxb-1, base >>1 );
		}
		if (idx1_r - idxa >=0 && idx2_r - idxb >= 0) {
			res = Math.min(res, minXor(arr, idxa, idx1_r, idxb, idx2_r, base >> 1));
		}
		if (res != Long.MAX_VALUE) {
/*			System.out.print(idx1_l+":"+idx1_r+" "+arr[idx1_l]+" "+arr[idx1_r]+" "+
					 idx2_l+":"+idx2_r+" "+arr[idx2_l]+" "+arr[idx2_r]);
			System.out.println("res1:"+res);*/
			return res;
		}

		if (idxa - idx1_l >0 && idx2_r - idxb >=0) {
			res = minXor(arr, idx1_l, idxa-1, idxb, idx2_r, base >> 1);
		}
		if (idx1_r - idxa >=0 && idxb - idx2_l >0) {
			res = minXor(arr, idxa, idx1_r, idx2_l, idxb-1, base >> 1);
		}
/*		System.out.print(idx1_l+":"+idx1_r+" "+arr[idx1_l]+" "+arr[idx1_r]+" "+
				 idx2_l+":"+idx2_r+" "+arr[idx2_l]+" "+arr[idx2_r]);
		System.out.println("res2:"+res);*/
		return res;

	}

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
 		
 		int n = in.nextInt();
 		long[] arr = new long[n];
 		for(int i=0; i < n; i++) {
 			arr[i] = in.nextLong();
 		}
 		Arrays.sort(arr);
 		//System.out.println(Arrays.toString(arr));
 		long max = arr[n-1];
        if (max == 0) {
            System.out.println(0);
            return;
        }
 		long x = 1L;
 		
        while(max != 1) {
 			max >>>= 1;
 			x <<= 1;
 		}
 		int idx = n-1;
 		boolean found = false;
 		do {
            idx = n-1;
 			for (; idx>=0; idx--) {
 				if ((arr[idx] & x) ==0) {
 					found = true;
 					break;
 				}
 			}
 			x >>= 1;

 		} while (!found);
 		
 		long y = minXor(arr, 0, idx, idx+1, n-1, x);
 		//y = (x<<1) | y;
 		System.out.println(y);
    }
}
