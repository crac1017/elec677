import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        
        Vector<Integer> larger = new Vector<Integer>(n);
        Vector<Integer> smaller = new Vector<Integer>(n);
        
        int mask = 1 << 30;
        while (mask >= 1) {
            for (int i = 0; i < n; i++) {
                if ((mask & a[i]) != 0) {
                    larger.add(a[i]);
                } else {
                    smaller.add(a[i]);
                }
            }
            if (larger.size() != 0 && smaller.size() != 0) {
                break;
            }
            larger.clear();
            smaller.clear();
            mask = mask >>> 1;
        }
        if (mask == 0) {
            System.out.println(0);
            return;
        }
        
        int score = Integer.MAX_VALUE;
        for (int i = 0; i < larger.size(); i++) {
            for (int j = 0; j < smaller.size(); j++) {
                score = Math.min(score, larger.get(i)^smaller.get(j));
            }
        }
        
        System.out.println(score);
    }
}
