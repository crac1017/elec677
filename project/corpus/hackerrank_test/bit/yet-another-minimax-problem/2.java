import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    public static int msbSet(int n) {
        if (n == 0) return 0;
        int x = 0;
        while (n >= (1 << x)) x++;
        return x-1;
    }
    
    public static boolean isSet(int n, int b) {
        return (n & (1 << b)) == (1 << b);
    }
    
    public static boolean allSet(int[] a, int sb) {
        for (int x : a) {
            if (msbSet(x) != sb) return false; 
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        int sb = 0;
        for (int x : a) {
            sb = Math.max(sb, msbSet(x));
        }
        while (sb >= 0 && allSet(a, sb)) sb--;
        if (sb < 0) {
            System.out.println("0");
            return;
        }
        //System.out.println("sb = " + sb);
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (!isSet(a[i], sb)) continue;
            for (int j = 0; j < n; j++) {
                if (j == i) continue;
                if (isSet(a[j], sb)) continue;
                result = Math.min(result, a[i]^a[j]);
            }
        }
        System.out.println(result);
    }
}
