import java.io.*;
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        short L = Short.parseShort(br.readLine());
        short R = Short.parseShort(br.readLine());
        short out = maxXor(L, R);
        System.out.print(out);
    }
    private static short maxXor(short L, short R) {
        //Probably not the best way, will optimize later
        short max = 0;
        for(short a = L; a <= R; ++a){
            for(short b = a; b <= R; ++b){
                short xor = (short)(a ^ b);
                max = xor > max ? xor : max;
            }
        }
        return max;
    }
}
