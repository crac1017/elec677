 
 

import java.io.*;
 
public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter f0 = new PrintWriter(new OutputStreamWriter(System.out));

        Utils util = new Utils();
        int L = util.toInt(br.readLine()); 
        int R = util.toInt(br.readLine()); 
        Long max = Long.MIN_VALUE;
        for (int i = L; i <= R; i++) {
            for (int j = i; j <= R; j++) {
                if(max<(i^j)){
                    max=(long)(i^j);
                }
            }
        }
        System.out.println(max);
    }
}

class Utils {

    public Integer toInt(String s) {
        return Integer.parseInt(s.trim());
    }

    public Long toLong(String s) {
        return Long.parseLong(s.trim());
    }

    public Double toDouble(String s) {
        return Double.parseDouble(s.trim());
    }

    public int[] getIntArray(String line) {
        String s[] = line.split(" ");
        int a[] = new int[s.length];
        for (int i = 0; i < s.length; ++i) {
            a[i] = Integer.parseInt(s[i]);
        }
        return a;
    }

    public Integer[] getIntegerArray(String line) {
        String s[] = line.split(" ");
        Integer a[] = new Integer[s.length];
        for (int i = 0; i < s.length; ++i) {
            a[i] = Integer.parseInt(s[i]);
        }
        return a;
    }

    public Double[] getDoubleArray(String line) {
        String s[] = line.split(" ");
        Double a[] = new Double[s.length];
        for (int i = 0; i < s.length; ++i) {
            a[i] = Double.parseDouble(s[i]);
        }
        return a;
    }

    public Long[] getLongArray(String line) {
        String s[] = line.split(" ");
        Long a[] = new Long[s.length];
        for (int i = 0; i < s.length; ++i) {
            a[i] = Long.parseLong(s[i]);
        }
        return a;
    }

    public Long getMaxFromLongArray(Long a[]) {
        Long max = Long.MIN_VALUE;
        for (int i = 0; i < a.length; ++i) {
            if (max < a[i]) {
                max = a[i];
            }
        }
        return max;
    }

    public Long getMinFromLongArray(Long a[]) {
        Long min = Long.MAX_VALUE;
        for (int i = 0; i < a.length; ++i) {
            if (min > a[i]) {
                min = a[i];
            }
        }
        return min;
    }

    public Integer getMaxFromIntegerArray(Integer a[]) {
        Integer max = Integer.MIN_VALUE;
        for (int i = 0; i < a.length; ++i) {
            if (max < a[i]) {
                max = a[i];
            }
        }
        return max;
    }

    public Integer getMinFromIntegerArray(Integer a[]) {
        Integer min = Integer.MAX_VALUE;
        for (int i = 0; i < a.length; ++i) {
            if (min > a[i]) {
                min = a[i];
            }
        }
        return min;
    }
}
