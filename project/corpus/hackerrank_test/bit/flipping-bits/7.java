import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Scanner;

public class Solution {

    static class Trie {
        HashSet<Character> asd;

        Trie(String a) {
            asd = new HashSet<>();
            for (int i = 0; i < a.length(); i++) {
                asd.add(a.charAt(i));
            }
        }

        boolean find(String b) {
            for (int i = 0; i < b.length(); i++) {
                if (asd.contains(b.charAt(i)))
                    return true;
            }
            return false;
        }
    }
    public static void main(String argv[]) throws NumberFormatException, IOException {
        Scanner l = new Scanner(new InputStreamReader(System.in));
        int T = l.nextInt();
        long d = (long) Math.pow(2, 32) - 1;
        for (int i = 0; i < T; i++) {
            long m = l.nextLong();
            long n = (~m) & d;
            System.out.println(n);
        }
        l.close();
    }
}
