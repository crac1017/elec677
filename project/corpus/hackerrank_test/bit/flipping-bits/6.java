import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long mask = 0x00000000ffffffff;
            
        for(int i = 0; i < n; i++) {
            long l = sc.nextLong();
            System.out.println(~l&mask + 4294967296l);
        }
    }
}
