import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;


public class Solution {

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter writer = new PrintWriter(System.out);
		
		int cases = Integer.parseInt(br.readLine());
		long a = Integer.MAX_VALUE + 2147483648L;
		for(int t=0;t<cases;t++){
			long n = Long.parseLong(br.readLine());
			writer.println(a - n);
		}
		writer.close();
	}
}
