import java.io.*;

public class Solution {

    public static void solve(Input in, PrintWriter out) throws IOException {
        int tests = in.nextInt();
        for (int test = 0; test < tests; ++test) {
            String s = in.next();
            int ans1 = 0, ans2 = 0;
            for (char c : s.toCharArray()) {
                if (c == (ans1 % 2 == 0 ? 'A' : 'B')) {
                    ++ans1;
                }
                if (c == (ans2 % 2 == 1 ? 'A' : 'B')) {
                    ++ans2;
                }
            }
            out.println(s.length() - Math.max(ans1, ans2));
        }
    }

    public static void main(String[] args) throws IOException {
        PrintWriter out = new PrintWriter(System.out);
        solve(new Input(new BufferedReader(new InputStreamReader(System.in))), out);
        out.close();
    }

    static class Input {
        BufferedReader in;
        StringBuilder sb = new StringBuilder();

        public Input(BufferedReader in) {
            this.in = in;
        }

        public Input(String s) {
            this.in = new BufferedReader(new StringReader(s));
        }

        public String next() throws IOException {
            sb.setLength(0);
            while (true) {
                int c = in.read();
                if (c == -1) {
                    return null;
                }
                if (" \n\r\t".indexOf(c) == -1) {
                    sb.append((char)c);
                    break;
                }
            }
            while (true) {
                int c = in.read();
                if (c == -1 || " \n\r\t".indexOf(c) != -1) {
                    break;
                }
                sb.append((char)c);
            }
            return sb.toString();
        }

        public int nextInt() throws IOException {
            return Integer.parseInt(next());
        }

        public long nextLong() throws IOException {
            return Long.parseLong(next());
        }

        public double nextDouble() throws IOException {
            return Double.parseDouble(next());
        }
    }
}
