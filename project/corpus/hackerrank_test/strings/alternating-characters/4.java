import java.io.*;
import java.util.*;

public class Solution {
  private static BufferedReader in;
  private static PrintWriter out;
  public static void main(String[] args) throws IOException {
    in = new BufferedReader(new InputStreamReader(System.in));
    out = new PrintWriter(System.out, true);
    int T = Integer.parseInt(in.readLine());
    while (T-- > 0) {
      char[] c = in.readLine().toCharArray();
      int N = c.length;
      int mb = 0;
      char r = 'B';
      for (int i = 0; i < N; i++) {
        if (c[i] != r) {
          mb++;
        } else {
          r ^= ('B' ^ 'A');
        }
      }
      int ma = 0;
      r = 'A';
      for (int i = 0; i < N; i++) {
        if (c[i] != r) {
          ma++;
        } else {
          r ^= ('B' ^ 'A');
        }
      }
      out.println (Math.min(mb, ma));
    }
    out.close();
    System.exit(0);
  }
}
