import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static int count(String s){
        char curr=s.charAt(0);
        int count=0;
        for(int i=1;i<s.length();i++){
            if(s.charAt(i)==curr) count++;
            else curr=s.charAt(i);
        }
        return count;
    }
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int t=in.nextInt();
        while(t-->0){
            System.out.println(count(in.next()));
        }
    }
}
