import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
      int count = 0;
      
      Scanner scanner = new Scanner(System.in);
      
      count = scanner.nextInt();
      
      while (count-- > 0) {
        String content = scanner.next();
        int result = 0;
        
        for (int i = 1; i != content.length(); i++) {
          if (content.charAt(i) == content.charAt(i - 1)) {
            result++;    
          }  
        }
        
        System.out.println(result);
      }
    }
}
