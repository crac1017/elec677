import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 */
public class Solution {
	public static void main(String[] args) {
		InputStream inputStream = System.in;
		OutputStream outputStream = System.out;
		InputReader in = new InputReader(inputStream);
		PrintWriter out = new PrintWriter(outputStream);
		AlternatingCharacters solver = new AlternatingCharacters();
		solver.solve(1, in, out);
		out.close();
	}
}

class AlternatingCharacters {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s;
        int test = in.nextInt();
        for (int i = 0; i < test; i++) {
            String inp = in.next();
            int res = 0;
            int d = 0;
            for (int j = 1; j < inp.length(); j++) {
                if (inp.charAt(j) == inp.charAt(j - 1)){
                    d++;
                }
                else {
                    res += d;
                    d = 0;
                }
            }
            res += d;
            out.print(res+"\n");
        }
    }
}

class InputReader {
    public BufferedReader reader;
    public StringTokenizer tokenizer;

    public InputReader(InputStream stream) {
        reader = new BufferedReader(new InputStreamReader(stream));
        tokenizer = null;
    }

    public String next() {
        while (tokenizer == null || !tokenizer.hasMoreTokens()) {
            try {
                tokenizer = new StringTokenizer(reader.readLine());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tokenizer.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt(next());
    }


}

