import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        for (int i = 0; i < n; i ++) {
            String string = input.next().trim();
            int counter = 0;
            for (int j = 1; j < string.length(); j ++) {
                if (string.charAt(j-1) != string.charAt(j)) counter ++;
            }
            System.out.println(string.length() - 1- counter);
        }
    }
}
