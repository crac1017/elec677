import java.io.*;
import java.util.*;

public class Solution {

	static BufferedReader reader;
	static StringTokenizer tokenizer = null;
	static PrintWriter writer;

	static String nextToken() throws IOException {
		while (tokenizer == null || (!tokenizer.hasMoreTokens())) {
			tokenizer = new StringTokenizer(reader.readLine());
		}
		return tokenizer.nextToken();
	}

	static int nextInt() throws NumberFormatException, IOException {
		return Integer.parseInt(nextToken());
	}

	static double nextDouble() throws NumberFormatException, IOException {
		return Double.parseDouble(nextToken());
	}

	static long nextLong() throws NumberFormatException, IOException {
		return Long.parseLong(nextToken());
	}

	public static void main(String[] args) throws IOException {
		reader = new BufferedReader(new InputStreamReader(System.in));
		writer = new PrintWriter(System.out);
		cherry();
		reader.close();
		writer.close();
	}

	static void cherry() throws NumberFormatException, IOException {
		int t = nextInt();
		for (int i = 0; i < t; i++) {
			String s = nextToken();
			int n = s.length();
			int k = 0;
			for (int j = 1; j < n; j++) {
				if (s.charAt(j - 1) == s.charAt(j))
					k++;
			}
			writer.println(k);
		}
	}
}
