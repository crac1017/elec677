

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int T = input.nextInt();
        input.nextLine();
        for (int t=0; t<T; t++) {
            String s = input.nextLine();
            int count = 0;
            char last = s.charAt(0);
            for (int i=1; i<s.length(); i++) {
                char c = s.charAt(i);
                if (c == last) {
                    count++;
                }
                last = c;
            }
            System.out.println(count);
        }
    }
    
}
