import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
    	Solution sol = new Solution();
    	
        Scanner scanner = new Scanner(System.in);
        
         int numStrings = Integer.parseInt(scanner.nextLine());
        
        for (int i=0; i < numStrings; i++) {
        	String strIn = scanner.nextLine();
        	System.out.println(sol.processString(strIn));
        }
    }
    
    public String processString(String str) {
    	String reverse = new StringBuilder(str).reverse().toString();
    	for (int i=1;i < str.length(); i ++) {
    		int strDiff = Math.abs(str.charAt(i) - str.charAt(i - 1));
    		int revDiff = Math.abs( reverse.charAt(i) - reverse.charAt(i - 1));
    		
    		if (strDiff != revDiff) {
    			return "Not Funny";
    		}
    	}    	
    	return "Funny";
    }
}
