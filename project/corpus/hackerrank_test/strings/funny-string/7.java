import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int T = sc.nextInt();
        sc.nextLine();
        for(int i=0;i<T;i++){
            String s = sc.nextLine();
            boolean funny = true;
            int len = s.length();
            for(int j=0;j<len/2;j++)
                if (Math.abs(s.charAt(j)-s.charAt(j+1)) != Math.abs(s.charAt(len-1-j) - s.charAt(len-2-j))){
                    funny = false;
                    break;
                }
            if(funny) System.out.println("Funny");
            else System.out.println("Not Funny");
        }
    }
}
