import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int i = 0; i<t; i++){
        	String word = in.next();
        	char[] array = word.toCharArray();
        	boolean yes = true;
        	for(int j = 1; j<array.length; j++){
        		if((int) Math.abs(array[j]-array[j-1]) != (int) Math.abs(array[array.length-j-1] - array[array.length-j])){
        			yes = false;
        		}
        	}
        	
        	
        	if(yes) System.out.println("Funny");
        	else System.out.println("Not Funny");
        }
    }
}
