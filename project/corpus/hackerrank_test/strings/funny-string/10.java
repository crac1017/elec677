import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < T; i++) {
            String S = sc.nextLine();
            int left = 0;
            int right = S.length() - 1;
            boolean funny = true;
            while (left < right) {
                if (Math.abs(S.charAt(left) - S.charAt(left+1)) !=
                    Math.abs(S.charAt(right) - S.charAt(right-1))) {
                    funny = false;
                    break;
                }
                left++;
                right--;
            }
            if (funny)
                System.out.println("Funny");
            else 
                System.out.println("Not Funny");
        }
    }
}
