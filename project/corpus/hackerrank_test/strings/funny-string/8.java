import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        final Scanner sc = new Scanner(System.in);
        final int TC = Integer.parseInt(sc.nextLine());
        for (int it = 0; it < TC; it++) {
            boolean funny = true;
            final String s = sc.nextLine();
            final int n = s.length();
            for (int i = 1; i < n; i++) {
                if (Math.abs(s.charAt(i)-s.charAt(i-1)) != Math.abs(s.charAt(n-i)-s.charAt(n-i-1))) funny = false;
            }
            System.out.println(funny ? "Funny" : "Not Funny");
        }
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
    }
}
