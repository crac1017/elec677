import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    private BufferedReader rd;

    private Solution() throws Exception {
        rd = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(rd.readLine());
        for(int i=0;i<t;i++) {
            char[] c = rd.readLine().toCharArray();
            int s = c.length;
            boolean funny = true;
            for(int j=0;j<s-1;j++) {
                if(Math.abs(c[j+1]-c[j]) != Math.abs(c[s-1-j]-c[s-2-j])) {
                    funny = false;
                }
            }
            out(funny?"Funny":"Not Funny");
        }
    }

    private void out(Object x) {
        System.out.println(x);
    }

    public static void main(String[] args) throws Exception {
        new Solution();
    }
}
