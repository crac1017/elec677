import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner in = new Scanner(System.in);
        int len = Integer.parseInt(in.nextLine());
        for (int i=0; i < len; i++) {
            String temp = in.nextLine();
            int strlen = temp.length();
            int left, right;
            for (int j=0; j < strlen/2; j++) {
                left = Math.abs(temp.charAt(j)-temp.charAt(j+1));
                right = Math.abs(temp.charAt(strlen-1-j)-temp.charAt(strlen-2-j));
                if (left != right) {
                    System.out.println("Not Funny");
                    break;
                }
                if (j==strlen/2-1)
                    System.out.println("Funny");
            }
        }   
    }
}
