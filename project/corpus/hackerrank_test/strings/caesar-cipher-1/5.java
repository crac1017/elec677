import java.util.*;

public class Solution
    {
    public static void main(String args[])
        {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = sc.next();
        int k = sc.nextInt();
        
        for(int i = 0 ; i < s.length() ; i++)
            {
            char c = s.charAt(i);
            if(c >= 'A' && c <= 'Z')
                {
                c = (char)((((c - 'A')+k)%26)+'A');
            }
            else if(c >= 'a' && c <= 'z')
                {
                c = (char)((((c - 'a')+k)%26)+'a');
            }
            System.out.print(c);
        }
    }
}
