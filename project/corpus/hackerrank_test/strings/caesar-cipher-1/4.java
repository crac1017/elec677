import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = sc.next();
        int k = sc.nextInt();
        char[] lw = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        char[] up = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        
        for(int i=0; i<n; i++){
            char c = s.charAt(i);
            if((int)c >= 97 && (int)c <= 122){
                int x = (((int)c - 'a') + k)%26;
                System.out.print(lw[x]);
            }
            else if((int)c >= 65 && (int)c <= 90){
                int x = (((int)c - 'A') + k)%26;
                System.out.print(up[x]);
            }
            else System.out.print(c);
        }
    }
}
