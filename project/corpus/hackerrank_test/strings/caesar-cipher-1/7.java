import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        in.nextLine();
        String str = in.nextLine();
        int k = in.nextInt();
         int i = 0;
        String result = "";
        int chZ = (int)'z';
        int chZZ = (int)'Z';
        while(i < k){
            
            for(char ch : str.toCharArray()){
                if(!Character.isAlphabetic(ch)){
                    result = result + ch;
                }else if(Character.isLowerCase(ch)){
                    int chI = (int)ch;
                    chI = chI+1;
                    if(chI > chZ){
                        chI = (int)'a';
                    }
                    result = result + (char)chI;
                }else if(Character.isUpperCase(ch)){
                    int chI = (int)ch;
                    chI = chI+1;
                    if(chI > chZZ){
                        chI = (int)'A';
                    }
                    result = result + (char)chI;
                }
            }
            str = result;
            result = "";
            i++;
        }
        System.out.println(str);
    }
}
