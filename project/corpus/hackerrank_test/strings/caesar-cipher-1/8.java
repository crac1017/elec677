import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int minUpperCase = 65;
        int maxUpperCase = 90;
        int minLowerCase = 97;
        int maxLowerCase = 122;

        int length = in.nextInt();
        String text = in.next();
        int rotate = in.nextInt();
        char[] arr = new char[length];
        for(int i=0; i < length; i++){

            char c = text.charAt(i);
            int code = (int)c;


            int shiftedCode;

            if(minUpperCase <= code && code <= maxUpperCase){
                shiftedCode = ((code - minUpperCase + rotate)%(26));
                shiftedCode = minUpperCase + shiftedCode;
            }else if(minLowerCase <= code && code <= maxLowerCase){
                shiftedCode = ((code - minLowerCase + rotate)%(26));
                shiftedCode = minLowerCase + shiftedCode;
            }else{
                shiftedCode = code;
            }
            //System.out.println("char => " + c + ", code => " + code + ", shiftedCode => " + shiftedCode + ", new char => " + (char) shiftedCode );

            arr[i] = (char) shiftedCode;

        }
        System.out.println(arr);
    }
}
