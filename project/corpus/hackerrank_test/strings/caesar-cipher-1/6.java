import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int len=in.nextInt();
        String string=in.next();
        int sol=in.nextInt();
        for(int i=0;i<len;i++)
            {
            char a=string.charAt(i);
            if(a>=97 && a<=122)
                {
                char n=(char)((((a-'a')+sol)%26)+97);
                System.out.print(n);
            }
            else if(a>=65 && a<=90)
                {
                char m=(char)((((a-'A')+sol)%26)+65);
                System.out.print(m);
            }
            else
                {System.out.print(a);}
        }
    }
}
