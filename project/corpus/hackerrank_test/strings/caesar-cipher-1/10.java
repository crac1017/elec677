import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution12 {

	public static Scanner in;
    public static void main(String[] args) {
        in = new Scanner(System.in);
		int wordLen = in.nextInt();
		String word = in.next();
		String newWord="";
		int cipher = in.nextInt();
		String letters = "abcdefghijklmnopqrstuvwxyz";
		String upperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int pos;

		for(int i = 0; i< wordLen; i++){
			if(Character.isLetter(word.charAt(i))){
				if(Character.isUpperCase(word.charAt(i))){
					pos = upperLetters.indexOf(word.substring(i,i+1));
					pos = (pos+cipher)%26;
					newWord += upperLetters.substring(pos, pos+1);
				}
				else{
					pos = letters.indexOf(word.substring(i,i+1));
					pos = (pos+ cipher)%26;
					newWord += letters.substring(pos, pos+1);
				}
			}
			else{
				newWord += word.substring(i, i+1);
			}
		}
		
		System.out.println(newWord);
    }
}
