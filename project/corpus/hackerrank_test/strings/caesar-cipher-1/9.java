import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) throws IOException{
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        int len = Integer.parseInt(br.readLine());
        String line = br.readLine();
        int k = Integer.parseInt(br.readLine())%26;
        
        for(int i=0;i<len;i++){
            int c = line.charAt(i);
            if(c>64 && c<91){
                if(c+k>90){
                    System.out.print((char)(64+(c+k)%90));
                }else{
                    System.out.print((char)(c+k));
                }
            }else if(c>96 && c<123){
               
                if(c+k>122){
                    System.out.print((char)(96+(c+k)%122));
                }else{
                    System.out.print((char)(c+k));
                }
                     
            }else{
                System.out.print(line.charAt(i));
            }
            
        }
        
        
        br.close();
        br = null;
    }
}
