\documentclass[12pt, a4paper]{article}
\usepackage{fullpage}
\usepackage{setspace}
\usepackage{color}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{graphics}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{syntax}
\usepackage{amssymb}
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\fontsize{9}{10}\selectfont\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  % captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  % frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Java,                 % the language of the code
  morekeywords={*,??,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  % numbersep=5pt,                   % how far the line-numbers are from the code
  % numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  % rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  % stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  % stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  % title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}


\begin{document}

\begin{flushleft}
Yanxin Lu \\
Final Project Report \\
Link to code: \verb|https://bitbucket.org/crac1017/elec677| \\
\today \\
\end{flushleft}

% \doublespacing

\section{Introduction}
Automated program synthesis aims to simplify programming by allowing
users to specify their intent as a high level specification. Then a
program synthesizer is used to generate programs which satisfy such
specification. Specifically, our original goal is to synthesize
solution programs to algorithmic programming problems. The following
shows one of the algorithmic problems:

\begin{quote}
  How many different ways can you make change for an amount, given a
  list of coins? In this problem, your code will need to efficiently
  compute the answer.
\end{quote}

The programmer is required to write a program that reads the input and
produces correct output that passes a set of hidden test cases while a
few sample test cases are provided. One important part of the solution
program for the above problem is showed at Figure~\ref{fig:coin-solution}.
\begin{figure}
  \begin{lstlisting}
private static int maxCoins(int n, int[]coins){
  int[] results = new int[n+1];
  results[0] = 1;
  for(int i=0; i<coins.length; i++) {
    for(int j=coins[i]; j<=n; j++) {
      results[j] += results[j - coins[i]];
    }
  }
  
  return results[n];
}
  \end{lstlisting}
  \vspace{-20pt}
\caption{Coin Exchange Solution}
\label{fig:coin-solution}
\end{figure}

People have considered the problem of program synthesis for many
years. From the programming languages community, logic-based methods
including SKETCH\cite{lezama06}, using SMT
solvers\cite{Srivastava2012}, enumerative search\cite{Alur2015} and
types\cite{udupa2013transit, Feser2015} have been quite successful and
gained some popularity. However, the biggest problem with logic-based
method is scalability; these methods cannot generate large amount of
code at one due to the search space being extremely large. In recent
years, statistical methods have gained some popularity with the access
to a large code corpus available in some online repositories such as
GitHub. As a result, statistical methods such as graphical
models~\cite{Raychev2015}, language models~\cite{Raychev2014,
  hindle2012naturalness, nguyen2015graph} and learning from noisy
data~\cite{raychev2016} have been showed to be quite effective in
inferring program properties and code completion. However, Gaunt et
al.~\cite{gaunt2016terpret} have showed that traditional logic-based
method is still better than pure gradient descent method.

People have been trying to combine statistical methods and logic-based
methods together, hoping that statistical methods can reduce the
search space and make logic-based method scale
better. DeepCoder~\cite{balog16} has showed that even a simple shallow
neural network can be extremely helpful in reducing the search space,
even though the domain specific language does not have loops and
conditions. Inspired by their work, we also would like to see whether
a neural network could be helpful in solving algorithmic problems.

\section{Method}
\label{sec:method}
\begin{figure}
  \centering
  \includegraphics[scale=0.3]{method.png}
  \caption{Method}
  \label{fig:method}
\end{figure}
In this project, we propose a method which takes a high level
specification expressed in natural language and then generates some
program features of the program that satisfies the specification.
In our case, the problem description is the high-level specification
and we choose the presence of a data type as our program feature.

We choose to use a recurrent neural network (RNN) as our statistical model
and we would like to learn whether the RNN could learn to predict what
data types are required to solve an algorithmic
problem. Figure~\ref{fig:method} illustrates our method. We first take
the problem description $D$ and use \verb|word2vec| to generate a
real-value matrix, $M$. We then feed $M$ into the RNN and the RNN is
supposed to produce a bit-vector indicating what data types are
present in the solution program. To train the RNN, in addition to the
description, we also harvest the solution program and perform static
analysis to retrieve the bit-vector.

\section{Experiment and Result}
We gather 500 Java programs from
Hackerrank~\footnote{https://www.hackerrank.com} and the programs are
divided into the following categories:
\begin{itemize}
\item bit manipulation
\item dynamic programming
\item graph theory and application
\item sorting
\item string manipulation
\end{itemize}

For each problem, we convert the description into a real-value matrix
using \verb|word2vec| and we use Eclipse JDT to analyze the solution
programs into order to extract the bit-vector. Specifically we record
the following data types into the the bit-vector:
\begin{grammar}
  <type> ::= <prim> | <com>

  <prim> ::= \emph{integer} | \emph{double} | \emph{string} | \emph{boolean}

  <com> ::= 1d-array(<prim>) | 2d-array(<prim>) | 3d-array(<prim>)
  \alt multi-dim-array(<prim>) if dimension is greater than 3
  \alt set(<prim>) | map(<prim>) | queue(<prim>)
\end{grammar}

We tuned the RNN with different types of hidden units including basic
RNN cells and GRU cells. We use L2-norm as the loss function and also
tried different learning rate using Adam as the optimizer. The best
performing model is an RNN with 20 GRU units and a learning rate of
$1e-4$. Figure~\ref{fig:training} shows the loss during the training
process and we can see that the model is capable of learning to
predict the data types. From the plot, we can see that the model needs
around 10000 epoch to be well trained and in the end we achieved
$96\%$ test accuracy in predicting data types.
\begin{figure}
  \centering
  \includegraphics[scale=0.3]{train.png}
  \caption{Training}
  \label{fig:training}
\end{figure}

\section{Conclusion and Future Work}
\label{sec:conc}
From the experiment, we can see that RNN seems to be capable of
predicting data types of the solution program. However, we did not
have time to investigate the reason behind the achievement of high
test accuracy and to produce some examples to see what the RNN will
produce given some unseen problem descriptions. This would be our next
future work. Our initial guess would be the different categories of
problems contain different set of keywords. For example, bit
manipulation problems might contain words such as ``bit'' and ``xor''
and their solution problems tend to only have integer or boolean as
the data type. Graph theory problems might contain works such as
``graph'', ``map'' and ``city'' and the corresponding solution
programs usually have 2d-array of integer type. We believe that these
words provide useful information for the model to associate the some
keywords with the data types.

Additional future work might include designing more program features
in order to help reduce the search space for program
synthesis. Possible program features include the number of variables,
high-level program structures, the presence of some important API
calls. We believe that these high-level features can guide the search
and reduce the search space significantly.


\bibliographystyle{abbrv}
\bibliography{refs} 

\end{document}
