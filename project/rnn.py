import tensorflow as tf 
from tensorflow.python.ops import rnn, rnn_cell
from tensorflow.examples.tutorials.mnist import input_data
import numpy as np 
import csv;
import sys;

TYPE_TOKEN_MAPPING = {
    "INT_TYPE"          : 0,
    "DOUBLE_TYPE"       : 1,
    "STRING_TYPE"       : 2,
    "BOOL_TYPE"         : 3,
    "CHAR_TYPE"         : 4,
    "ARRAY_INT_TYPE"    : 5,
    "ARRAY_DOUBLE_TYPE" : 6,
    "ARRAY_BOOL_TYPE"   : 7,
    "ARRAY_CHAR_TYPE"   : 8
};
TYPE_TOKEN_LEN = 9;

def read_data(file_prefix):
    """
    Read the data file and embedding file
    """
    data_filename = file_prefix + "_desc.txt";
    embed_filename = file_prefix + "_embedding.csv";
    label_filename = file_prefix + "_types.txt";
    embedding_dict = {};

    with open(embed_filename) as infile:
        reader = csv.reader(infile);
        vec_len = None;
        for line in reader:
            word = line[0];
            vec = np.array(line[1:], dtype = np.float32);
            embedding_dict[word] = vec;
            if vec_len is None:
                vec_len = vec.shape;

        embedding_dict["<eos>"] = np.ones(vec_len, dtype = np.float32);
        
    print "Vocabulary size:", len(embedding_dict);

    # read the text data file
    labels = [];
    max_len = 0;
    words_mat = [];
    with open(data_filename) as infile:
        lines = infile.read().split("\n");
        for l in lines:
            if l.strip() == "":
                continue;
            fields = l.split(" ");
            words = fields[:-1];
            words_mat.append(words);

            max_len = max(len(words), max_len);

            # label = float(fields[-1]);
            # labels.append(label);

    with open(label_filename) as infile:
        lines = infile.read().split("\n");
        for l in lines:
            if l.strip() == "":
                continue;
            tokens = l.strip().split(" ");
            final_label = np.zeros(TYPE_TOKEN_LEN);
            for t in tokens:
                pos = TYPE_TOKEN_MAPPING[t];
                final_label[pos] = 1.0;


            labels.append(final_label);

    data = np.zeros((len(labels), max_len, vec_len[0]), dtype = np.float32);
    for i in range(len(labels)):
        words = words_mat[i];
        for j in range(len(words)):
            w = words[j];
            data[i, j] = embedding_dict[w] if w in embedding_dict else embedding_dict["UNK"];
        
    return data, np.array(labels), max_len, vec_len[0];

def split_data(data, label):
    """
    Split the dataset for training and testing by 70/30
    """
    whole_len = len(data);
    split_point = int(float(whole_len) * 0.3);
    train_data = data[split_point:];
    train_label = label[split_point:];

    test_data = data[:split_point];
    test_label = label[:split_point];

    return train_data, train_label, test_data, test_label;

def seq_len(sequence):
    """
    return the length of a sequence
    """
    used = tf.sign(tf.reduce_max(tf.abs(sequence), reduction_indices=2))
    length = tf.reduce_sum(used, reduction_indices=1)
    length = tf.cast(length, tf.int32)
    return length

if len(sys.argv) < 2:
    sys.exit("Usage: %s [Original JSON data filename]" % sys.argv[0]);

filename = sys.argv[1];

data, labels, max_seq_len, embed_size = read_data(filename);
train_data, train_label, test_data, test_label = split_data(data, labels);
# print data.shape, labels.shape, max_seq_len, embed_size;
# print train_data.shape, train_label.shape, test_data.shape, test_label.shape;
# exit();

# mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

batch_start = 0;
def next_batch(batch_size):
    """
    Return the next batch given the batch size and the start position
    in the data array
    """
    global data, labels, batch_start;
    old_start = batch_start;
    end = old_start + batch_size if old_start + batch_size < len(train_data) else len(train_data);
    batch_start = end if end < len(train_data) else 0;
    return (train_data[old_start : end], train_label[old_start : end]);

learningRate = 1e-7;
trainingIters = 1000;
batchSize = 20;
displayStep = 10;

nHidden = 10; #number of neurons for the RNN

x = tf.placeholder('float', [None, max_seq_len, embed_size])
y = tf.placeholder('float', [None])

weights = {
    'out': tf.Variable(tf.random_normal([nHidden, 1]))
}

biases = {
    'out': tf.Variable(tf.random_normal([1]))
}

def last_relevant(output, length):
    batch_size = tf.shape(output)[0]
    max_length = tf.shape(output)[1]
    out_size = int(output.get_shape()[2])
    index = tf.range(0, batch_size) * max_length + (length - 1)
    flat = tf.reshape(output, [-1, out_size])
    relevant = tf.gather(flat, index)
    return relevant

def RNN(x, weights, biases):
    # x = tf.transpose(x, [1, 0, 2])
    # x = tf.reshape(x, [-1, embed_size])
    # x = tf.split(0, max_seq_len, x) 

    # gru_cell = tf.nn.rnn_cell.GRUCell(nHidden); #find which lstm to use in the documentation
    # lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(nHidden); #find which lstm to use in the documentation
    rnn_cell = tf.nn.rnn_cell.BasicRNNCell(nHidden); #find which lstm to use in the documentation

    length = seq_len(x);
    outputs, states = tf.nn.dynamic_rnn(rnn_cell,
                                        x,
                                        dtype = tf.float32,
                                        sequence_length = seq_len(x)) #for the rnn where to get the output and hidden state 

    # print outputs.get_shape();
    last = last_relevant(outputs, length);
    # print last;
    return tf.matmul(last, weights['out'])+ biases['out']

result_dir = "./rnn_result/";
train_summary_writer = tf.train.SummaryWriter(result_dir + "train/");
test_summary_writer = tf.train.SummaryWriter(result_dir + "test/");
pred = RNN(x, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
with tf.name_scope("summaries"):
    cost = tf.reduce_mean(tf.nn.l2_loss(tf.sub(pred, y)));
    tf.scalar_summary("cost", cost);

optimizer = tf.train.AdamOptimizer(learningRate).minimize(cost);

# correctPred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1));

# with tf.name_scope("summaries"):
    # accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32));
    # tf.scalar_summary("accuracy", accuracy);


summary_op = tf.merge_all_summaries();
init = tf.initialize_all_variables()

# testData = mnist.test.images.reshape((-1, nSteps, nInput))
# testLabel = mnist.test.labels

with tf.Session() as sess:
    sess.run(init)
    step = 1

    while step * batchSize < trainingIters:
        # batchX, batchY = mnist.train.next_batch(batchSize); #mnist has a way to get the next batch
        batchX, batchY = next_batch(batchSize);
        # batchX = batchX.reshape((batchSize, nSteps, nInput))
        sess.run(optimizer, feed_dict={x : batchX, y : batchY})

        if step % displayStep == 0:
            # acc = accuracy.eval(feed_dict = {x : batchX, y : batchY});
	    loss = cost.eval(feed_dict = {x : batchX, y : batchY});
	    print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss))

            train_acc_summary = sess.run(summary_op, feed_dict = {x : batchX, y : batchY});
            test_acc_summary = sess.run(summary_op, feed_dict = {x : test_data, y : test_label});

            train_summary_writer.add_summary(train_acc_summary, step);
            test_summary_writer.add_summary(test_acc_summary, step);

            train_summary_writer.flush();
            test_summary_writer.flush();

        step +=1
    print('Optimization finished')
    print("Testing Error:", sess.run(cost, feed_dict={x : test_data, y : test_label}))
