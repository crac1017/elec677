from scipy import misc
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt
import matplotlib as mp
import os;
import time;

# --------------------------------------------------
# setup

def weight_variable(shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    W = tf.Variable(tf.truncated_normal(shape, stddev = 0.1));
    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    b = tf.Variable(tf.constant(0.1, shape = shape));
    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides = [1, 1, 1, 1], padding = "SAME");
    return h_conv;

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x,
                           ksize = [1, 2, 2, 1],
                           strides = [1, 2, 2, 1],
                           padding = "SAME");
    return h_max;

def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.scalar_summary('mean/' + name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
            tf.scalar_summary('sttdev/' + name, stddev)
            tf.scalar_summary('max/' + name, tf.reduce_max(var))
            tf.scalar_summary('min/' + name, tf.reduce_min(var))
            # tf.histogram_summary(name, var)


ntrain = 1000;  # per class
ntest = 100; # per class
nclass = 10; # number of classes
imsize = 28;
nchannels = 1;
batchsize = 600;
learning_rate = 1e-4;

Train = np.zeros((ntrain * nclass, imsize, imsize, nchannels))
Test = np.zeros((ntest * nclass, imsize, imsize, nchannels))
LTrain = np.zeros((ntrain * nclass, nclass))
LTest = np.zeros((ntest * nclass, nclass))

itrain = -1
itest = -1
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = './CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float) / 255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = './CIFAR10/Test/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path); # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

nsample = itrain + 1;
sess = tf.InteractiveSession()

#tf variable for the data, remember shape is [None, width, height, numberOfChannels] 
tf_data = tf.placeholder(tf.float32, shape = [None, imsize, imsize, nchannels]);
tf_labels = tf.placeholder(tf.float32, shape = [None, nclass]); #tf variable for labels

# --------------------------------------------------
# model

# first convolutional layer
with tf.name_scope("weights"):
    W_conv1 = weight_variable([5, 5, 1, 32]);
    variable_summaries(W_conv1, "layer1/weights");
    for i in range(32):
        with tf.name_scope("summaries"):
            tf.image_summary("layer1/filter%d" % i, tf.expand_dims(W_conv1[:, :, :, i], 0), max_images = 1);

with tf.name_scope("bias"):
    b_conv1 = bias_variable([32]);
    variable_summaries(b_conv1, "layer1/bias");

with tf.name_scope("net_input"):
    h_net1 = conv2d(tf_data, W_conv1) + b_conv1;
    variable_summaries(h_net1, "layer1/net_input");

with tf.name_scope("ReLU_activation"):
    h_conv1 = tf.nn.relu(h_net1);
    variable_summaries(h_conv1, "layer1/ReLU_activation");

with tf.name_scope("Max-Pool_activation"):
    h_pool1 = max_pool_2x2(h_conv1);
    variable_summaries(h_pool1, "layer1/Max-Pool_activation");

# second convolutional layer
with tf.name_scope("weights"):
    W_conv2 = weight_variable([5, 5, 32, 64]);
    variable_summaries(W_conv2, "layer2/weights");

with tf.name_scope("bias"):
    b_conv2 = bias_variable([64]);
    variable_summaries(b_conv2, "layer2/bias");

with tf.name_scope("net_input"):
    h_net2 = conv2d(h_pool1, W_conv2) + b_conv2;
    variable_summaries(h_net2, "layer2/net_input");

with tf.name_scope("ReLU_activation"):
    h_conv2 = tf.nn.relu(h_net2);
    variable_summaries(h_net1, "layer2/ReLU_activation");

with tf.name_scope("Max-Pool_activation"):
    h_pool2 = max_pool_2x2(h_conv2);
    variable_summaries(h_pool2, "layer2/Max-Pool_activation");

# densely connected layer
with tf.name_scope("weights"):
    W_fc1 = weight_variable([7 * 7 * 64, 1024]);
    variable_summaries(W_fc1, "fc1/weights");

with tf.name_scope("bias"):
    b_fc1 = bias_variable([1024]);
    variable_summaries(b_fc1, "fc1/bias");

h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64]);

with tf.name_scope("net_input"):
    h_fc_net1 = tf.matmul(h_pool2_flat, W_fc1) + b_fc1;
    variable_summaries(h_fc_net1, "fc1/net_input");

with tf.name_scope("ReLU_activation"):
    h_fc1 = tf.nn.relu(h_fc_net1);
    variable_summaries(h_fc1, "fc1/ReLU_activation");

# dropout
with tf.name_scope("dropout"):
    keep_prob = tf.placeholder(tf.float32);
    tf.scalar_summary("dropout_keep_probability", keep_prob);
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob);

# softmax
with tf.name_scope("weights"):
    W_fc2 = weight_variable([1024, 10]);
    variable_summaries(W_fc2, "fc2/weights");

with tf.name_scope("bias"):
    b_fc2 = bias_variable([10]);
    variable_summaries(b_fc2, "fc2/bias");

with tf.name_scope("net_input"):
    y_net = tf.matmul(h_fc1_drop, W_fc2) + b_fc2;
    variable_summaries(y_net, "fc2/net_input");

with tf.name_scope("Softmax_activation"):
    y_conv = tf.nn.softmax(y_net);
    variable_summaries(y_conv, "fc2/Softmax_activation");

print "Done with building the model."


# --------------------------------------------------
# loss
#set up the loss, optimization, evaluation, and accuracy
# setup training

with tf.name_scope("cross_entropy"):
    diff = tf_labels * tf.log(y_conv);
    with tf.name_scope("total"):
        cross_entropy = tf.reduce_mean(-tf.reduce_sum(diff, reduction_indices=[1]));

    tf.scalar_summary("cross_entropy", cross_entropy);

    # cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y_conv), reduction_indices=[1]));

with tf.name_scope("train"):
    # we choose 1e-3 because 1e-2 sucks and 1e-4 is slower
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)

with tf.name_scope("error"):
    with tf.name_scope("incorrect_prediction"):
        incorrect_prediction = tf.not_equal(tf.argmax(y_conv,1), tf.argmax(tf_labels,1));
    with tf.name_scope("error"):
        loss = tf.reduce_mean(tf.cast(incorrect_prediction, tf.float32));
    tf.scalar_summary("error", loss);

print "Done with setting up error."

# Build the summary operation based on the TF collection of Summaries.
summary_op = tf.merge_all_summaries()

# Add the variable initializer Op.
init = tf.initialize_all_variables()

# Create a saver for writing training checkpoints.
saver = tf.train.Saver()

# Instantiate a SummaryWriter to output summaries and the Graph.
result_dir = "./results/";
# summary_writer = tf.train.SummaryWriter(result_dir, sess.graph, "train/")
summary_writer = tf.train.SummaryWriter(result_dir, sess.graph, "train/")
test_summary_writer = tf.train.SummaryWriter(result_dir + "test/");
# validation_summary_writer = tf.train.SummaryWriter(result_dir + "validation/");

# --------------------------------------------------
# optimization
print "Training..."
sess.run(init);

#setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_xs = np.zeros([batchsize, imsize, imsize, nchannels]);
#setup as [batchsize, the how many classes] 
batch_ys = np.zeros([batchsize, nclass])

max_step = 10000

# try a small iteration size once it works then continue
start_time = time.time();
for i in range(max_step):
    perm = np.arange(nsample)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]

    if i % 60 == 0 or i == max_step - 1:
        entropy = cross_entropy.eval(feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 1.0})
        #calculate train accuracy and print it
        train_loss = loss.eval(feed_dict={tf_data:batch_xs, tf_labels:batch_ys, keep_prob: 1.0})
        test_loss = loss.eval(feed_dict={tf_data:Test, tf_labels:LTest, keep_prob: 1.0})
        print("step %d, entropy %.6f, training loss %.6f, testing loss %.6f, used %s seconds."%(i, entropy, train_loss, test_loss, time.time() - start_time))


        # Update the events file which is used to monitor the training (in this case,
        # only the training loss is monitored
        summary_str = sess.run(summary_op, feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5})
        summary_test_str = sess.run(summary_op, feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 0.5})

        summary_writer.add_summary(summary_str, i)
        test_summary_writer.add_summary(summary_test_str, i);

        summary_writer.flush();
        test_summary_writer.flush();

        checkpoint_file = os.path.join(result_dir, "checkpoint");
        saver.save(sess, checkpoint_file, global_step = i);


    optimizer.run(feed_dict={tf_data : batch_xs, tf_labels : batch_ys, keep_prob : 0.5}) # dropout only during training


# --------------------------------------------------
# test

print("test loss %g"%loss.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0}))



sess.close()
