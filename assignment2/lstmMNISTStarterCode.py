import tensorflow as tf 
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np 

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

learningRate = 1e-4;
trainingIters = 1000000;
batchSize = 130;
displayStep = 10;

nInput = 28 #we want the input to take the 28 pixels
nSteps = 28; #every 28
nHidden = 150; #number of neurons for the RNN
nClasses = 10; #this is MNIST so you know

x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])

weights = {
    'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}

biases = {
    'out': tf.Variable(tf.random_normal([nClasses]))
}

def RNN(x, weights, biases):
    print(x.get_shape());
    x = tf.transpose(x, [1, 0, 2])
    print(x.get_shape());
    x = tf.reshape(x, [-1, nInput])
    print(x.get_shape());
    x = tf.split(0, nSteps, x) #configuring so you can get it as needed for the 28 pixels
    print(len(x), x[0].get_shape())
    exit()

    # gru_cell = tf.nn.rnn_cell.GRUCell(nHidden); #find which lstm to use in the documentation
    # lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(nHidden); #find which lstm to use in the documentation
    rnn_cell = tf.nn.rnn_cell.BasicRNNCell(nHidden); #find which lstm to use in the documentation

    outputs, states = tf.nn.rnn(rnn_cell, x, dtype = tf.float32) #for the rnn where to get the output and hidden state 

    return tf.matmul(outputs[-1], weights['out'])+ biases['out']

result_dir = "./rnn_result/";
train_summary_writer = tf.train.SummaryWriter(result_dir + "train/");
test_summary_writer = tf.train.SummaryWriter(result_dir + "test/");
pred = RNN(x, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
with tf.name_scope("summaries"):
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y));
    tf.scalar_summary("cost", cost);

optimizer = tf.train.AdamOptimizer(learningRate).minimize(cost);

correctPred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1));

with tf.name_scope("summaries"):
    accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32));
    tf.scalar_summary("accuracy", accuracy);


summary_op = tf.merge_all_summaries();
init = tf.initialize_all_variables()

testData = mnist.test.images.reshape((-1, nSteps, nInput))
testLabel = mnist.test.labels

with tf.Session() as sess:
    sess.run(init)
    step = 1

    while step * batchSize < trainingIters:
        batchX, batchY = mnist.train.next_batch(batchSize); #mnist has a way to get the next batch
        batchX = batchX.reshape((batchSize, nSteps, nInput))
        sess.run(optimizer, feed_dict={x : batchX, y : batchY})

        if step % displayStep == 0:
            acc = accuracy.eval(feed_dict = {x : batchX, y : batchY});
	    loss = cost.eval(feed_dict = {x : batchX, y : batchY});
	    print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss) + ", Training Accuracy= " + \
                  "{:.5f}".format(acc))

            train_acc_summary = sess.run(summary_op, feed_dict = {x : batchX, y : batchY});
            test_acc_summary = sess.run(summary_op, feed_dict = {x : testData, y : testLabel});

            train_summary_writer.add_summary(train_acc_summary, step);
            test_summary_writer.add_summary(test_acc_summary, step);

            train_summary_writer.flush();
            test_summary_writer.flush();

        step +=1
    print('Optimization finished')
    print("Testing Accuracy:", sess.run(accuracy, feed_dict={x : testData, y : testLabel}))
