#!/bin/bash
docker run --rm -v $(pwd):/root -p 8888:8888 -it crac1017/elec677 /bin/bash -c "/opt/conda/bin/jupyter notebook --notebook-dir=/root --ip='*' --port=8888 --no-browser"
# docker run --rm -v $(pwd):/root -p 8888:8888 -it crac1017/tensorflow:cpu /bin/bash -c "jupyter notebook --notebook-dir=/root --ip='*' --port=8888 --no-browser"
# docker run --rm -p 6006:6006 -v $(pwd):/root -it crac1017/tensorflow:cpu /bin/bash
# nvidia-docker run --rm -p 6006:6006 -v $(pwd):/root -it crac1017/tensorflow:gpu /bin/bash
# docker run --rm -p 6006:6006 -v $(pwd):/root -it crac1017/tensorflow:cpu /bin/bash
# nvidia-docker run --rm -it -p 8888:8888 crac1017/tensorflow:gpu
